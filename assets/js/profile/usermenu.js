
var profile_name = document.getElementById("profile_name");
var detail = document.getElementById("detail");
var menuShown = false;

if (profile_name !== null) 
{
    profile_name.onmousedown = showProfileMenu;
}

function showProfileMenu(evt)
{
    if(!menuShown) 
    {
        menuShown = true;
        var profile_name = document.getElementById("profile_name");
        var menu = document.createElement("div");
        var mouseX = 0, mouseY = 0;
        menu.id="menu_prof";
        menu.className="blue_panel";
        menu.style.width="115px";
        menu.style.height="65px";
        menu.style.position = "absolute";
        menu.style.top = "80px";
        menu.innerHTML = '<ul id="menu_profile">\
                            <li><a href="/mi_perfil" title="ir a mi perfil">Ir a mi perfil</a></li>\
                            <li><a href="/salir" title="Cerrar sesion">Cerrar sesi&oacute;n</a></li>\
                        </ul>';

        detail.appendChild(menu);
        profile_name.onmousedown = hideProfileMenu;
    }

}

function hideProfileMenu(evt)
{
    if(menuShown) 
    {
        menuShown = false;
        var menu_prof = document.getElementById("menu_prof");
        var profile_name = document.getElementById("profile_name");

        menu_prof.parentNode.removeChild(menu_prof);
        profile_name.onmousedown = showProfileMenu;
    }

}

function load()
{
    var link_exit = document.getElementById("close_session");

    if (link_exit !== null) 
    {
        link_exit.parentNode.removeChild(link_exit);
    }
}