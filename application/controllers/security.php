<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Security extends CI_Controller 
{

    public function index()
    {
        $logged_in = $this->session->userdata('logged_in');

        if ($logged_in) 
        {
            // tener en cuenta para un futuro usar uri_string() para 
            // redirigir tras login
            redirect("panel", 'refresh'); 
        }
        else
        {
            $this->load->view('security/login');
        }
    }

    public function login()
    {
        $this->load->library('form_validation');
        $this->load->library('authorization');

        $this->form_validation->set_rules('_username', 'Usuario', 'trim|required');
        $this->form_validation->set_rules('_password', 'Contrase&ntilde;a', 'trim|required|md5');
        $this->form_validation->set_rules('_remember_me', 'Mantener sesi&oacute;n', 'trim');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('security/login');
        }
        else
        {
            $username = $this->input->post('_username');
            $password = $this->input->post('_password');
            $remember_me = $this->input->post('_remember_me');

            // check if user exists
            $user = $this->authorization->check_login($username, $password);
            if ($user != false)
            {
                if ($user->isActive())
                {
                    $this->authorization->create_session($user);
                    $this->load->helper('cookie');
                    if ($remember_me)
                    {
                        $cookie = get_cookie("mu_session");
                        if (!empty($cookie))
                        {
                            setcookie("mu_session", $cookie, strtotime("next month"));
                        }
                        $cookie = get_cookie("mumita_cookie");
                        if (!empty($cookie))
                        {
                            setcookie("mumita_cookie", $cookie,  strtotime("next month"));
                        }
                    }
                    else 
                    {
                        $cookie = get_cookie("mu_session");
                        if (!empty($cookie))
                        {
                            setcookie("mu_session", $cookie);
                        }
                        $cookie = get_cookie("mumita_cookie");
                        if (!empty($cookie))
                        {
                            setcookie("mumita_cookie", $cookie);
                        }
                    }
                    redirect('/panel', 'refresh');
                }
                else
                {
                    // message
                    $data['login_error'] = 'Usuario no activo, por favor comprueba tu correo.';
                    $this->load->view('security/login', $data);
                }
            }
            else
            {
                // message
                $data['login_error'] = 'Usuario o contrase&ntilde;a incorrectos.';
                $this->load->view('security/login', $data);
            }
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        $this->load->helper('cookie');
        $cookie = get_cookie("mu_session");
        
        if (!empty($cookie))
        {
            setcookie("mu_session", $cookie, -1);
        }
        
        $cookie = get_cookie("mumita_cookie");
        
        if (!empty($cookie))
        {
            setcookie("mumita_cookie", $cookie, -1);
        }
        
        redirect('/', 'refresh');
    }
    
    public function activate($email, $code)
    {
        $this->load->library('authorization');
        $this->load->library('user');
        $email = urldecode($email);
        
        $code = $this->authorization->activate($email, $code);
        
        switch($code)
        {
            case 1:  // activated
                $users = $this->user->find_by("email", $email, TRUE);
                $user = $users[0];
                $data['name'] = $user->getName();
                $data['email'] = $user->getEmail();
                $view = 'activate';
                break;
            case 2:  // email not exists
                $data['email'] = $email;
                $view = 'email_not_exists';
                break;
            default:  // activation failed
                $data['email'] = $email;
                $view = 'activation_failed';
                break;
        }
        
        $this->load->view("security/activation/$view", $data);
    }
    
    function forgot_password() 
    {		
        $this->load->library('form_validation');
        $this->load->library('user');

        $this->form_validation->set_rules('email', 'Correo electr&oacute;nico', 'trim|required|max_length[254]|valid_email');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('security/forgot_password/enter_email');
        }
        else
        {
            $email = $this->input->post('email');
            $users = $this->user->find_by('email', $email, TRUE);
            
            if (count($users)==1)
            {
                $user = $users[0];
             
                if(!$user->getActive()) 
                {
                    $data['forgot_error'] = 'Usuario no activo, por favor comprueba tu correo.';
                    $this->load->view('security/forgot_password/enter_email', $data);
                }
                else
                {
                    $sent = $this->user->reset_password($user);
                    if (!$sent)
                    {
                        $data['forgot_error'] = 'No se pudo enviar la contrase&ntilde;a, por favor vuelve a probar pasados unos minutos y si persiste ponte en contacto con nosotros y te ayudaremos a solucionar tu problema :).';
                        $this->load->view('security/forgot_password/enter_email', $data);
                    }
                    else
                    {
                        $this->session->set_userdata('email_sent',true);
                        redirect('/correo_enviado', 'refresh');
                    }
                }
            }
        }
    }
    
    function forgot_sent() 
    {
        $email_sent = $this->session->userdata('email_sent');
        
        if ($email_sent)
        {
            $this->load->view('security/forgot_password/email_sent');
            $this->session->unset_userdata('email_sent');
        }
        else
        {
            echo "eerrrorrrr 404444";
        }
    }
}

/* End of file security.php */
/* Location: ./application/controllers/security.php */