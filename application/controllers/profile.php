<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller 
{

    public function __construct()
    {
        parent::__construct();

        $logged_in = $this->session->userdata('logged_in');

        if (!$logged_in) 
        {
            // tener en cuenta para un futuro usar uri_string() para 
            // redirigir tras login
            redirect("acceder", 'refresh'); 
        }
    }

    public function index()
    {
        $this->load->library('user');
        $id = $this->session->userdata('id');
        $users = $this->user->find_by('id', $id, TRUE);
        
        $this->load->view("privates/user/private_profile_1", array("user"=>$users[0]));
    }

    public function page_1($edit = false)
    {
        $this->load->library('user');
        $id = $this->session->userdata('id');
        $users = $this->user->find_by('id', $id, TRUE);
        
        if ($edit == "edit")
        {
            $this->load->view("privates/user/private_profile_1_edit", array("user"=>$users[0]));
        }
        else
        {
            $this->load->view("privates/user/private_profile_1", array("user"=>$users[0]));
        }
    }
    
    public function save_1()
    {
        $this->load->library('form_validation');
        $this->load->library('authorization');
        $this->load->library('user');

        $id = $this->session->userdata('id');
        $this->form_validation->set_rules('name', 'Nombre', 'trim|required|max_length[50]|xss_clean');
        $this->form_validation->set_rules('lastname', 'Apellidos', 'trim|required|max_length[75]|xss_clean');
        $this->form_validation->set_rules('email', 'e-mail', 'trim|required|max_length[254]|valid_email');
        $this->form_validation->set_rules('password', 'Contrase&ntilde;a', 'trim|max_length[255]|matches[repeat_password]|md5');
        $this->form_validation->set_rules('repeat_password', 'Repetir contrase&ntilde;a', 'trim');
        $this->form_validation->set_rules('sex', 'Sexo', 'trim|xss_clean');
        $this->form_validation->set_rules('bio', 'Bio', 'trim|max_length[400]|xss_clean');
        $this->form_validation->set_rules('street', 'Calle', 'trim|max_length[200]|xss_clean');
        $this->form_validation->set_rules('city', 'Ciudad', 'trim|max_length[200]|xss_clean');
        $this->form_validation->set_rules('province', 'Provincia', 'trim|max_length[200]|xss_clean');
        $this->form_validation->set_rules('postal_code', 'Código postal', 'trim|max_length[10]|xss_clean');
        $users = $this->user->find_by('id', $id, TRUE);
        
        if (empty($users))
        {
            $data['message'] = "No se ha puesto en la URL el identificador de usuario o es incorrecto.";
            $data['user'] = new User_entity();
        }
        else
        {
            $data['user'] = $users[0];
        }
        
        if(!is_dir("./assets/images/tmp/"))
        {
            mkdir("./assets/images/tmp/", 0777, true);
        }
        $config['upload_path'] = './assets/images/tmp/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']	= '300';
        $config['max_width']  = '1024';
        $config['max_height']  = '768';
        $config['overwrite']  = TRUE;

        $this->load->library('upload', $config);
        
        $do_upload = $this->upload->do_upload();
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('privates/user/private_profile_1_edit', $data);
        }
        else
        {
            $user_result = $this->user->find_by('id', $id, TRUE);
            
            if (!empty($user_result))
            {
                // <user data>
                $user_data = $user_result[0];
                $user_data->setName($this->input->post('name'));
                $user_data->setLastname($this->input->post('lastname'));
                $user_data->setEmail($this->input->post('email'));
                $password = $this->input->post('password');
                if (!empty($password))
                {
                    $user_data->setPassword($password);
                }
                $user_data->setSex($this->input->post('sex'));
                $user_data->setBio($this->input->post('bio'));
                // </user data>
                
                // <address>
                $address = $user_data->getShippingAddress();
                $address->setStreet($this->input->post('street'));
                $address->setCity($this->input->post('city'));
                $address->setProvince($this->input->post('province'));
                $address->setCountry("España");
                $address->setPostalCode($this->input->post('postal_code'));
                $user_data->setShippingAddress($address);
                // </address>
                
                $updated = $this->user->update($user_data);

                if($do_upload)
                {
                    $file = $this->upload->data();
                    if($file['is_image'] == 1)
                    { 
                        $user_dir = $file['file_path'].'../users/'.md5($data['user']->getId().$data['user']->getSalt());
                        $this->user->create_avatars($file, $user_dir);
                    }
                }

                if($updated)
                {
                    $this->session->set_flashdata('message', 'Usuario actualizado correctamente.');
                    redirect("mi_perfil/");
                }
                else
                {
                    $data['error'] = "El email ya existe.";
                    $upload_error = $this->upload->display_errors();

                    if(!empty($upload_error))
                    {
                        $data['error'] .= "<br />$upload_error";
                    }
                }
            }
            else
            {
                $data['error'] = "El usuario es incorrecto.";
            }
            
            if(!empty($data['error']))
            {
                $this->load->view('privates/user/private_profile_1_edit', $data);
            }
        }
    }
    
}

/* End of file profile.php */
/* Location: ./application/controllers/profile.php */