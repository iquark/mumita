<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Community extends CI_Controller 
{
    const PER_PAGE = 15;

    public function __construct()
    {
        parent::__construct();

        $logged_in = $this->session->userdata('logged_in');

        if (!$logged_in) 
        {
            // tener en cuenta para un futuro usar uri_string() para 
            // redirigir tras login
            redirect("acceder", 'refresh'); 
        }
    }
	
    public function index()
    {
        $this->load->library('user');
        
        $users = $this->user->get_all();
        
        $this->load->library('pagination');
        $config['base_url'] = base_url().'comunidad/pag/';
        $config['total_rows'] = count($users);
        $config['per_page'] = self::PER_PAGE;
        $config['full_tag_open'] = '<p>';
        $config['full_tag_close'] = '</p>';
        $config['first_link'] = 'Primera';
        $config['last_link'] = '&Uacute;ltima';
        $this->pagination->initialize($config);
        $data['users'] = array_slice($users, $this->uri->segment(3), $config['per_page']);
        
	$data['total_rows'] = $config['total_rows'];
        
        $this->load->view('privates/community', $data);
    }
	
    public function search()
    {
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $data['users'] = array();
        
        $this->form_validation->set_rules('search', 'Campo de búsqueda', 'trim|max_length[75]|xss_clean');
        $this->form_validation->set_rules('sex', 'Sexo', 'trim|xss_clean');
        $this->form_validation->set_rules('size', 'Talla', 'trim|xss_clean');
        $this->form_validation->set_rules('season', 'Temporada', 'trim|xss_clean');
        $continue = true;
        
        if ($this->form_validation->run() != FALSE)
        {
            $search = $this->input->post("search");
            $sex = $this->input->post("sex");
            $size = $this->input->post("size");
            $season = $this->input->post("season");
        }
        else
        {
            $search_parameters = unserialize($this->session->userdata('community_search'));
            if (!empty($search_parameters))
            {
                $search = $search_parameters["search"];
                $sex = $search_parameters["sex"];
                $size = $search_parameters["size"];
                $season = $search_parameters["season"];
            }
            else
            {
                $continue = false;
                $data['total_rows'] = 0;
                $data['users'] = array();
            }
        }
        
        if ($continue)
        {
            // TODO filter by the boxes that users are selling
            $this->load->library('user');
            $users = $this->user->search($search, $sex, $size, $season);
            
            $config['base_url'] = base_url().'comunidad/buscar/';
            $config['total_rows'] = count($users);
            $config['per_page'] = self::PER_PAGE;
            $config['full_tag_open'] = '<p>';
            $config['full_tag_close'] = '</p>';
            $config['first_link'] = 'Primera';
            $config['last_link'] = '&Uacute;ltima';
            $this->pagination->initialize($config);
            $data['users'] = array_slice($users, $this->uri->segment(3), $config['per_page']);

            $data['total_rows'] = $config['total_rows'];
            $search_parameters = array(
                "search" => $search, "sex"=> $sex, "size" => $size, "season" => $season
            );
            
            $this->session->set_userdata('community_search', serialize($search_parameters));
        }

        $this->load->view('privates/community', $data);
    }
    
    public function profile($user_id = null)
    {
        $this->load->library('user');
        $users = $this->user->find_by('id', $user_id);
        
        if (!empty($users))
        {
            $data['user'] = $users[0];
        }
        else
        {
            $data['user'] = new User_entity();
            $data['error'] = "El usuario que buscas no existe.";
        }
        $this->load->view('privates/profile', $data);
    }
    
    /**
     * The actual user will follow the given user
     * @param integer $user_id 
     */
    public function follow($user_id = null)
    {
        $this->load->library('user');
        $our_id = $this->session->userdata('id');
        
        $following = $this->user->follow($our_id, $user_id);
        
        redirect("perfil/$user_id", 'refresh'); 
    }
    
    /**
     * The actual user will unfollow the given user
     * @param integer $user_id 
     */
    public function unfollow($user_id = null)
    {
        $this->load->library('user');
        $our_id = $this->session->userdata('id');
        
        $unfollowing = $this->user->unfollow($our_id, $user_id);
        
        redirect("perfil/$user_id", 'refresh'); 
    }
}

/* End of file community.php */
/* Location: ./application/controllers/community.php */