<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Publics extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();

        $logged_in = $this->session->userdata('logged_in');

        if (!$logged_in) 
        {
            // tener en cuenta para un futuro usar uri_string() para 
            // redirigir tras login
        //    redirect("acceder", 'refresh'); 
        }
    }
    
    public function index()
    {
        $session_id = $this->session->userdata('session_id');
        $this->load->view('publics/index');
    }

    public function register()
    {
        $this->load->library('form_validation');
        $this->load->library('authorization');
        $this->load->library('user');
        
        $year = date("Y", strtotime("now"));
        $this->form_validation->set_rules('name', 'Nombre', 'trim|required|max_length[50]|xss_clean');
        $this->form_validation->set_rules('lastname', 'Apellidos', 'trim|required|max_length[75]|xss_clean');
        $this->form_validation->set_rules('email', 'e-mail', 'trim|required|max_length[254]|valid_email|is_unique[user.email]');
        $this->form_validation->set_rules('sex', 'Sexo', 'trim|required|xss_clean');
        $this->form_validation->set_rules('day', 'D&iacute;a', 'trim|required|less_than[32]|greater_than[0]|xss_clean');
        $this->form_validation->set_rules('month', 'Mes', 'trim|required|less_than[13]|greater_than[0]|xss_clean');
        $this->form_validation->set_rules('year', 'A&ntilde;o', "trim|required|xss_clean|callback_year_check");
        $this->form_validation->set_rules('password', 'Contrase&ntilde;a', 'trim|required|max_length[255]|matches[repeat_password]|md5');
        $this->form_validation->set_rules('repeat_password', 'Repetir contrase&ntilde;a', 'trim|required');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('publics/register');
        }
        else
        {
            $name = $this->input->post('name');
            $lastname = $this->input->post('lastname');
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $sex = $this->input->post('sex');
            $day = $this->input->post('day');
            $month = $this->input->post('month');
            $year = $this->input->post('year');
            $birthday = date( 'Y-m-d', strtotime("$day.$month.$year") );
            $bio = null;

            $id_user = $this->user->register($name, $lastname, $email, $password, $sex, $bio, $birthday);

            if(!empty($id_user))
            {
                $this->user->send_email($id_user);
                $this->session->set_flashdata('register_success', 'Registro completado, ahora puedes '.anchor(base_url().'/panel','acceder a tu espacio privado','Accede a tu espacio privado de Mumita').".");
                redirect('registro_completado');
            }
            else
            {
                $data['register_error'] = "Ha ocurrido un error, vuelva a intentarlo pasados unos minutos y si persiste, por favor, comuniquelo a trav&eacute;s del formulario de contacto.";
                $this->load->view('publics/register', $data);
            }
        }
    }
    
    function year_check()
    {
        $this->load->library('form_validation');
        $day = $this->input->post('day');
        $month = $this->input->post('month');
        $year = $this->input->post('year');
        
        $d = date('d', strtotime("$day.$month.$year"));
        $m = date('m', strtotime("$day.$month.$year"));
        $y = date('Y', strtotime("$day.$month.$year"));
        if ($y != $year || $m != $month || $d != $day)
        {
            $this->form_validation->set_message('year_check', 'La fecha no es v&aacute;lida.');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    public function register_success()
    {
        $message = $this->session->flashdata('register_success');

        if(empty($message))
        {
            redirect('/');
        }
        else 
        {
            $this->load->view('publics/register_success',array("message"=>$message));
        }
    }
    
}

/* End of file publics.php */
/* Location: ./application/controllers/publics.php */