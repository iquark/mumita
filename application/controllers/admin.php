<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller 
{

    public function __construct()
    {
        parent::__construct();

        $logged_in = $this->session->userdata('logged_in');
        $role_level = $this->session->userdata('role_level');

        if (!$logged_in || $role_level==0) 
        {
            // tener en cuenta para un futuro usar uri_string() para 
            // redirigir tras login
            redirect("acceder", 'refresh'); 
        }
    }
    
    public function index()
    {
      $this->load->view('admin/index');
    }
    
}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */