<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_users extends CI_Controller 
{

    public function __construct()
    {
        parent::__construct();

        $logged_in = $this->session->userdata('logged_in');
        $role_level = $this->session->userdata('role_level');

        if (!$logged_in || $role_level==0) 
        {
            // tener en cuenta para un futuro usar uri_string() para 
            // redirigir tras login
            redirect("acceder", 'refresh'); 
        }
    }
    
    public function index()
    {
        $this->load->library('user');
        $message = $this->session->flashdata('message');
        
        if (!empty($message))
        {
            $data['message'] = $message;
        }
        
        $data['users'] =  $this->user->get_all(TRUE);
        
        $this->load->view('admin/users/list', $data);
    }
    
    public function create()
    {
        $this->load->library('form_validation');
        $this->load->library('authorization');
        $this->load->library('user');
        $this->load->model('User_model');

        $this->form_validation->set_rules('name', 'Nombre', 'trim|required|max_length[50]|xss_clean');
        $this->form_validation->set_rules('lastname', 'Apellidos', 'trim|required|max_length[75]|xss_clean');
        $this->form_validation->set_rules('email', 'e-mail', 'trim|required|max_length[254]|valid_email|is_unique[user.email]');
        $this->form_validation->set_rules('password', 'Contrase&ntilde;a', 'trim|required|max_length[255]|matches[repeat_password]|md5');
        $this->form_validation->set_rules('repeat_password', 'Repetir contrase&ntilde;a', 'trim|required');
        $this->form_validation->set_rules('sex', 'Sexo', 'trim|xss_clean');
        $this->form_validation->set_rules('day', 'D&iacute;a', 'trim|required|less_than[32]|greater_than[0]|xss_clean');
        $this->form_validation->set_rules('month', 'Mes', 'trim|required|less_than[13]|greater_than[0]|xss_clean');
        $this->form_validation->set_rules('year', 'A&ntilde;o', "trim|required|xss_clean|callback_year_check");
        $this->form_validation->set_rules('active', 'Activo', 'trim|xss_clean');
        $this->form_validation->set_rules('role_level', 'Rol', 'trim|xss_clean');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('admin/users/user_form');
        }
        else
        {
            $name = $this->input->post('name');
            $lastname = $this->input->post('lastname');
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $sex = $this->input->post('sex');
            $active = $this->input->post('active');
            $role_level = $this->input->post('role_level');
            $day = $this->input->post('day');
            $month = $this->input->post('month');
            $year = $this->input->post('year');
            $birthday = date( 'Y-m-d', strtotime("$day.$month.$year") );
            $bio = null;

            $id_user = $this->user->register($name, $lastname, $email, $password, $sex, $bio, $birthday, $active, $role_level);

            if(!empty($id_user))
            {
                $this->session->set_flashdata('message', 'Nuevo usuario creado.');
                redirect('admin/users');
            }
            else
            {
                $data = array();
                $data['message'] = "Ha habido un error interno, no se ha podido crear el usuario.";
                $this->load->view('admin/users/user_form', $data);
            }
        }
    }
    
    public function edit($id = null)
    {
        $this->load->library('form_validation');
        $this->load->library('authorization');
        $this->load->library('user');
       
        $data['id'] = $id;
        $this->form_validation->set_rules('name', 'Nombre', 'trim|required|max_length[50]|xss_clean');
        $this->form_validation->set_rules('lastname', 'Apellidos', 'trim|required|max_length[75]|xss_clean');
        $this->form_validation->set_rules('email', 'e-mail', 'trim|required|max_length[254]|valid_email');
        $this->form_validation->set_rules('password', 'Contrase&ntilde;a', 'trim|max_length[255]|matches[repeat_password]|md5');
        $this->form_validation->set_rules('repeat_password', 'Repetir contrase&ntilde;a', 'trim');
        $this->form_validation->set_rules('sex', 'Sexo', 'trim|xss_clean');
        $this->form_validation->set_rules('day', 'D&iacute;a', 'trim|required|less_than[32]|greater_than[0]|xss_clean');
        $this->form_validation->set_rules('month', 'Mes', 'trim|required|less_than[13]|greater_than[0]|xss_clean');
        $this->form_validation->set_rules('year', 'A&ntilde;o', "trim|required|xss_clean|callback_year_check");
        $this->form_validation->set_rules('active', 'Activo', 'trim|xss_clean');
        $this->form_validation->set_rules('role_level', 'Rol', 'trim|xss_clean');
        $users = $this->user->find_by('id', $id, TRUE);
        
        if (empty($users))
        {
            $data['message'] = "No se ha puesto en la URL el identificador de usuario o es incorrecto.";
            $user = new User_entity();
        }
        else
        {
            $user = $users[0];
        }
        
        if ($this->form_validation->run() == FALSE)
        {
            $data['user'] = $user;
            $this->load->view('admin/users/user_form', $data);
        }
        else
        {
            $id = $this->input->post('id');
            $name = $this->input->post('name');
            $lastname = $this->input->post('lastname');
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $password = (!empty($password))?$password:null;
            $sex = $this->input->post('sex');
            $active = $this->input->post('active');
            $role_level = $this->input->post('role_level');
            $day = $this->input->post('day');
            $month = $this->input->post('month');
            $year = $this->input->post('year');
            $birthday = date( 'Y-m-d', strtotime("$day.$month.$year") );
            $bio= null;
            
            $user_update = new User_entity($id, $name, $lastname, $email, $password, $sex, $role_level, $bio, null, $active, $birthday);
            $updated = $this->user->update($user_update);
            
            if($updated)
            {
                $this->session->set_flashdata('message', 'Usuario actualizado correctamente.');
                redirect('admin/users');
            }
            else
            {
                $data['user'] = $user;
                $data['error'] = "El email ya existe.";
                $this->load->view('admin/users/user_form', $data);
            }
        }
    }
    
    public function delete($id = null)
    {
        $this->load->library('user');
        $deleted = $this->user->delete($id);
        
        if ($deleted)
        {
           $this->session->set_flashdata('message', "Eliminado usuario $id.");
        }
        else
        {
           $this->session->set_flashdata('message', "No se pudo eliminar el usuario $id.");
        }
        
        redirect('admin/users');
    }
    
    
    public function year_check()
    {
        $this->load->library('form_validation');
        $day = $this->input->post('day');
        $month = $this->input->post('month');
        $year = $this->input->post('year');
        
        $d = date('d', strtotime("$day.$month.$year"));
        $m = date('m', strtotime("$day.$month.$year"));
        $y = date('Y', strtotime("$day.$month.$year"));
        if ($y != $year || $m != $month || $d != $day)
        {
            $classname = get_class();
            $this->form_validation->set_message($classname.'->year_check', 'La fecha no es v&aacute;lida.');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    
}

/* End of file user.php */
/* Location: ./application/controllers/user.php */