<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(dirname(__FILE__) . '/../libraries/entities/User_entity.php');

class User_boxes extends CI_Controller 
{
    const PER_PAGE = 8;
    
    public function __construct()
    {
        parent::__construct();

        $logged_in = $this->session->userdata('logged_in');

        if (!$logged_in) 
        {
            // tener en cuenta para un futuro usar uri_string() para 
            // redirigir tras login
            redirect("acceder", 'refresh'); 
        }
    }
    
    public function index()
    {
        $this->load->library('box');
        $message = $this->session->flashdata('message');
        $id_user = $this->session->userdata('id');
        
        if (!empty($message))
        {
            $data['message'] = $message;
        }
        
        $boxes =  $this->box->find_by("id_user", $id_user);
        
        $this->load->library('pagination');
        $config['base_url'] = base_url().'cajas/pag/';
        $config['total_rows'] = count($boxes);
        $config['per_page'] = self::PER_PAGE;
        $config['full_tag_open'] = '<p>';
        $config['full_tag_close'] = '</p>';
        $config['first_link'] = 'Primera';
        $config['last_link'] = '&Uacute;ltima';
        $this->pagination->initialize($config);
        $data['boxes'] = array_slice($boxes, $this->uri->segment(3), $config['per_page']);
        
	$data['total_rows'] = $config['total_rows'];
        
        
        $this->load->view('privates/boxes/list', $data);
    }
    
    public function create_cloth()
    {
        $this->load->library('form_validation');
        $this->load->library('authorization');
        $this->load->library('box');
        $this->load->model('Box_model');
        
        $config = array();

        $this->form_validation->set_rules('sex', 'Sexo', 'trim|required|xss_clean');
        $this->form_validation->set_rules('abstract', 'Resumen', 'trim|required|max_length[100]|xss_clean');
        $this->form_validation->set_rules('description', 'Descripci&oacute;n', 'trim|required|max_length[500]|xss_clean');
        $this->form_validation->set_rules('size', 'Talla', 'trim|required|xss_clean');
        $this->form_validation->set_rules('colour1', 'Color 1', 'trim|required|xss_clean');
        $this->form_validation->set_rules('colour2', 'Color 2', 'trim|required|xss_clean');
        $this->form_validation->set_rules('colour3', 'Color 3', 'trim|required|xss_clean');
        $this->form_validation->set_rules('season', 'Estacion', 'trim|required|xss_clean');

        if(!is_dir("./assets/images/tmp/"))
        {
            mkdir("./assets/images/tmp/", 0777, true);
        }
        $config['upload_path'] = './assets/images/tmp/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']	= '300';
        $config['max_width']  = '1024';
        $config['max_height']  = '768';
        $config['overwrite']  = TRUE;

        $this->load->library('upload', $config);
        
        $do_upload = $this->upload->do_upload();
        
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('privates/boxes/cloth_form');
        }
        else
        {
            $age = 1;
            $sex = $this->input->post('sex');
            $abstract = $this->input->post('abstract');
            $description = $this->input->post('description');
            $size = $this->input->post('size');
            $colour1 = $this->input->post('colour1');
            $colour2 = $this->input->post('colour2');
            $colour3 = $this->input->post('colour3');
            $season = $this->input->post('season');
            $user_id = $this->session->userdata('id');
            
            $box = new Cloth_entity(null, $abstract, 0, $description,
                                    $age, $sex, $user_id, null, Box::STATUS_PUBLISHED, 1,
                                    $size, $colour1,
                                    $colour2, $colour3, $season);

            $id_box = $this->box->create($box);
            if($do_upload)
            {
                $file = $this->upload->data();
                if($file['is_image'] == 1)
                { 
                    $box_dir = $file['file_path'].'../boxes/'.md5($id_box);
                    $this->box->create_avatars($file, $box_dir);
                }
            }
            if(!empty($id_box))
            {
                $this->session->set_flashdata('message', 'Nueva caja creada.');
                redirect('cajas');
            }
            else
            {
                $data = array();
                $data['message'] = "Ha habido un error interno, no se ha podido crear la caja.";
                $this->load->view('privates/boxes/cloth_form', $data);
            }
        }
    }
    
    public function edit_cloth($id = null)
    {
        $this->load->library('form_validation');
        $this->load->library('authorization');
        $this->load->library('box');
        $user_data = unserialize($this->session->userdata('user_data'));
       
        $data['id'] = $id;
        $this->form_validation->set_rules('sex', 'Sexo', 'trim|required|xss_clean');
        $this->form_validation->set_rules('abstract', 'Resumen', 'trim|required|max_length[100]|xss_clean');
        $this->form_validation->set_rules('description', 'Descripci&oacute;n', 'trim|required|max_length[500]|xss_clean');
        $this->form_validation->set_rules('size', 'Talla', 'trim|required|xss_clean');
        $this->form_validation->set_rules('colour1', 'Color 1', 'trim|required|xss_clean');
        $this->form_validation->set_rules('colour2', 'Color 2', 'trim|required|xss_clean');
        $this->form_validation->set_rules('colour3', 'Color 3', 'trim|required|xss_clean');
        $this->form_validation->set_rules('season', 'Estacion', 'trim|required|xss_clean');
        $boxes = $this->box->find_by('id', $id);
        $wrong_box = false;
        
        if (empty($boxes))
        {
            $wrong_box = true;
        }
        else
        {
            $box = $boxes[0];
            $user = $box->getUser();

            if ((is_object($user) && $user->getId() != $user_data->getId()) || $user_data->isAdmin())
            {
                $wrong_box = true;
            }
        }
        
        if(!is_dir("./assets/images/tmp/"))
        {
            mkdir("./assets/images/tmp/", 0777, true);
        }
        $config['upload_path'] = './assets/images/tmp/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']	= '500';
        $config['max_width']  = '1024';
        $config['max_height']  = '768';
        $config['overwrite']  = TRUE;

        $this->load->library('upload', $config);
        
        $do_upload = $this->upload->do_upload();
        
        if ($this->form_validation->run() == FALSE || $wrong_box)
        {
            if($wrong_box)
            {
                $data['error'] = "Caja incorrecta.";
                $data['box'] = new Cloth_entity();
            }
            else
            {
                $data['box'] = $box;
            }
            $this->load->view('privates/boxes/cloth_form', $data);
        }
        else
        {
            $age = 1;
            $sex = $this->input->post('sex');
            $abstract = $this->input->post('abstract');
            $description = $this->input->post('description');
            $size = $this->input->post('size');
            $colour1 = $this->input->post('colour1');
            $colour2 = $this->input->post('colour2');
            $colour3 = $this->input->post('colour3');
            $season = $this->input->post('season');
            $user_id = $this->session->userdata('id');
            
            $box_update = new Cloth_entity($id, $abstract, 0, $description,
                                    $age, $sex, $user_id, null, Box::STATUS_DRAFT, 1, 
                                    $size, $colour1,
                                    $colour2, $colour3, $season);
            $updated = $this->box->update($box_update);
            
            $uploaded = false;
            if($do_upload)
            {
                $file = $this->upload->data();
                if($file['is_image'] == 1)
                { 
                    $box_dir = $file['file_path'].'../boxes/'.md5($id);
                    $created = $this->box->create_avatars($file, $box_dir);
                    $updated = $this->box->update($box_update);
                    $uploaded = true;
                }
            }
            else
            {
                $upload = $this->upload->data();
                if ($upload['file_type']!=null)
                {
                    $data['error'] = $this->upload->display_errors();
                }
                else 
                {
                    $uploaded = true;
                    $updated = $this->box->update($box_update);
                }
            }
            if($updated && $uploaded)
            {
                $this->session->set_flashdata('message', 'Caja actualizada correctamente.');
                redirect('cajas');
            }
            else
            {
                $data['box'] = $box;
                if (!$updated)
                {
                    $data['error'] .= ((!empty($data['error']))?"\n":"")."El email ya existe.";
                }
                $this->load->view('privates/boxes/cloth_form', $data);
            }
        }
    }
    
    public function view_cloth($id_box = null)
    {
        $data['box'] = new Cloth_entity();
        $data['user_id'] = $this->session->userdata("id");
        if (!empty($id_box))
        {
            $this->load->library('box');
            $boxes = $this->box->find_by('id', $id_box);
            
            if (!empty($boxes))
            {
                $data['box'] = $boxes[0];
            }
            else
            {
                $data['error'] = "No existe la caja seleccionada.";
            }
        }
        else
        {
            $data['error'] = "No se ha seleccionado ninguna caja.";
        }
        
        $this->load->view('privates/boxes/cloth_view', $data);
    }
    
    public function delete($id = null)
    {
        $this->load->library('box');
        $boxes = $this->box->find_by('id', $id);
        $user_data = unserialize($this->session->userdata('user_data'));
        $wrong_box = false;
        
        if (empty($boxes))
        {
            $wrong_box = true;
        }
        else
        {
            $box = $boxes[0];
            $user = $box->getUser();

            if ((is_object($user) && $user->getId() != $user_data->getId()) || $user_data->isAdmin())
            {
                $wrong_box = true;
            }
        }
        
        if($wrong_box)
        {
            $this->session->set_flashdata('message', "Caja incorrecta.");
        }
        else
        {
            $deleted = $this->box->delete($id);
            if ($deleted)
            {
                $this->session->set_flashdata('message', "Eliminada caja $id.");
            }
            else
            {
                $this->session->set_flashdata('message', "No se pudo eliminar la caja $id.");
            }
        }
        
        redirect('cajas');
    }

    public function search()
    {
        $this->load->library('form_validation');
        $this->load->library('box');
        $this->load->model('Box_model');
        $this->load->library('pagination');
        
        // if flashdata donÂ´t use form
        $search = $this->session->flashdata('search');
        
        if(!empty($search))
        {
            $continue = true;
            $sex = null;
            $size = null;
            $season = null;
        }
        else
        {
            $this->form_validation->set_rules('search', 'Buscar', 'trim|xss_clean');
            $this->form_validation->set_rules('sex', 'Sexo', 'trim|xss_clean');
            $this->form_validation->set_rules('size', 'Talla', 'trim|xss_clean');
            $this->form_validation->set_rules('season', 'Temporada', 'trim|xss_clean');
            $continue = true;

            if ($this->form_validation->run() != FALSE)
            {
                $search = $this->input->post("search");
                $sex = $this->input->post("sex");
                $size = $this->input->post("size");
                $season = $this->input->post("season");
            }
            else
            {
                $search_parameters = unserialize($this->session->userdata('boxes_search'));
                if (!empty($search_parameters))
                {
                    $search = $search_parameters["search"];
                    $sex = $search_parameters["sex"];
                    $size = $search_parameters["size"];
                    $season = $search_parameters["season"];
                }
                else
                {
                    $continue = false;
                    $data['total_rows'] = 0;
                    $data['boxes'] = array();
                }
            }
        }
        
        if ($continue)
        {
            $boxes = $this->box->search_cloth($search, $sex, $size, $season);
            
            $config['base_url'] = base_url().'cajas/buscar/';
            $config['total_rows'] = count($boxes);
            $config['per_page'] = self::PER_PAGE;
            $config['full_tag_open'] = '<p>';
            $config['full_tag_close'] = '</p>';
            $config['first_link'] = 'Primera';
            $config['last_link'] = '&Uacute;ltima';
            $this->pagination->initialize($config);
            $data['boxes'] = array_slice($boxes, $this->uri->segment(3), $config['per_page']);

            $data['total_rows'] = $config['total_rows'];
            $search_parameters = array(
                "search" => $search, "sex"=> $sex, "size" => $size, "season" => $season
            );
            
            $this->session->set_userdata('boxes_search', serialize($search_parameters));
        }
        
        $this->load->view('privates/boxes/search', $data);
    }
    
    public function comment($id_box)
    {
        $this->load->library('form_validation');
        $this->load->library('box');
        $id_user = $this->session->userdata('id');
        
        $this->form_validation->set_rules('comment', 'de texto del comentario', 'trim|required|xss_clean');

        if ($this->form_validation->run() != FALSE && !empty($id_box) && !empty($id_user))
        {
            $comment = $this->input->post("comment");
            
            $num_comment = $this->box->comment($id_box, $id_user, $comment);
            
            redirect("/caja/$id_box#$num_comment");
        }
        else
        {
            if (empty($id_user))
            {
                redirect("/salir");
            }
            if (empty($id_box))
            {
                redirect("/panel");
            }
            
            $data['box'] = new Box_entity();
            
            $this->load->library('box');
            $boxes = $this->box->find_by('id', $id_box);

            if (!empty($boxes))
            {
                $data['box'] = $boxes[0];

            }
            else
            {
                $data['error'] = "No existe la caja seleccionada.";
            }
            
            $this->load->view("privates/boxes/cloth_view", $data);
        }
    }
    
    public function valorate($box_id)
    {
        $this->load->library('form_validation');
        $this->load->library('box');
        $user_id = $this->session->userdata('id');
        
        $this->form_validation->set_rules('valoration', 'valoracion', 'trim|required|xss_clean');

        if ($this->form_validation->run() != FALSE && !empty($box_id) && !empty($user_id))
        {
            $valoration = $this->input->post("valoration");
            
            $valoration = $this->box->add_valoration($box_id, $user_id, $valoration);
         
            redirect("/caja/$box_id");
        }
        else
        {
            if (empty($user_id))
            {
                redirect("/salir");
            }
            if (empty($box_id))
            {
                redirect("/panel");
            }
            
            $data['box'] = new Box_entity();
            
            $this->load->library('box');
            $boxes = $this->box->find_by('id', $box_id);

            if (!empty($boxes))
            {
                $data['box'] = $boxes[0];

            }
            else
            {
                $data['error'] = "No existe la caja seleccionada.";
            }
            
            $this->load->view("privates/boxes/cloth_view", $data);
        }
    }
    
    public function by_user($user_id)
    {
        $this->session->set_flashdata('search', "user:$user_id");
        redirect("cajas/buscar");
    }
    
    public function year_check()
    {
        $this->load->library('form_validation');
        $day = $this->input->post('day');
        $month = $this->input->post('month');
        $year = $this->input->post('year');
        
        $d = date('d', strtotime("$day.$month.$year"));
        $m = date('m', strtotime("$day.$month.$year"));
        $y = date('Y', strtotime("$day.$month.$year"));
        if ($y != $year || $m != $month || $d != $day)
        {
            $classname = get_class();
            $this->form_validation->set_message($classname.'->year_check', 'La fecha no es v&aacute;lida.');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    
    public function send()
    {
        $box_id = $this->input->post("box_id");
        echo "ccc $box_id";   
        
        if (!empty($box_id))
        {
            $this->load->library("mrw/transmenvio");
            $this->transmenvio->runOperation($box_id);
        }
        else
        {
            echo "pos no";
        }
    }
    
}

/* End of file box.php */
/* Location: ./application/controllers/user_boxes.php */