<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Privates extends CI_Controller 
{

    public function __construct()
    {
        parent::__construct();

        $logged_in = $this->session->userdata('logged_in');

        if (!$logged_in) 
        {
            // tener en cuenta para un futuro usar uri_string() para 
            // redirigir tras login
            redirect("acceder", 'refresh'); 
        }
    }

    public function index()
    {
        redirect("panel", 'refresh'); 
    }

    public function dashboard()
    {
        $data['your_info'] = array_slice($this->timeline->get_your_info(), 0, 10);
        $data['friends_info'] = array_slice($this->timeline->get_friends_info(), 0, 10);
        $data['suggested_boxes'] = $this->timeline->get_suggested_boxes();
        $data['suggested_people'] = $this->timeline->get_suggested_people();
        
        $this->load->view('privates/dashboard', $data);
    }
    
    public function profile($page=1)
    {
        $this->load->library('user');
        $id = $this->session->userdata('id');
        $users = $this->user->find_by('id', $id, TRUE);

        $this->load->view("privates/user/setup_$page", array("user"=>$users[0]));
    }
    
}

/* End of file privates.php */
/* Location: ./application/controllers/privates.php */