<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_boxes extends CI_Controller 
{

    public function __construct()
    {
        parent::__construct();

        $logged_in = $this->session->userdata('logged_in');
        $role_level = $this->session->userdata('role_level');

        if (!$logged_in || $role_level==0) 
        {
            // tener en cuenta para un futuro usar uri_string() para 
            // redirigir tras login
            redirect("acceder", 'refresh'); 
        }
    }
    
    public function index()
    {
        $this->load->library('box');
        $message = $this->session->flashdata('message');
        
        if (!empty($message))
        {
            $data['message'] = $message;
        }
        
        $data['boxes'] =  $this->box->get_all();
        
        $this->load->view('admin/boxes/list', $data);
    }
    
    public function create_cloth()
    {
        $this->load->library('form_validation');
        $this->load->library('authorization');
        $this->load->library('box');
        $this->load->model('Box_model');

        $this->form_validation->set_rules('age', 'Edad', 'trim|required|xss_clean');
        $this->form_validation->set_rules('sex', 'Sexo', 'trim|required|xss_clean');
        $this->form_validation->set_rules('abstract', 'Resumen', 'trim|required|max_length[100]|xss_clean');
        $this->form_validation->set_rules('description', 'Descripci&oacute;n', 'trim|required|max_length[500]|xss_clean');
        $this->form_validation->set_rules('size', 'Talla', 'trim|required|xss_clean');
        $this->form_validation->set_rules('colour1', 'Color 1', 'trim|required|xss_clean');
        $this->form_validation->set_rules('colour2', 'Color 2', 'trim|required|xss_clean');
        $this->form_validation->set_rules('colour3', 'Color 3', 'trim|required|xss_clean');
        $this->form_validation->set_rules('season', 'Estacion', 'trim|required|xss_clean');
        
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('admin/boxes/cloth_form');
        }
        else
        {
            $age = $this->input->post('age');
            $sex = $this->input->post('sex');
            $abstract = $this->input->post('abstract');
            $description = $this->input->post('description');
            $size = $this->input->post('size');
            $colour1 = $this->input->post('colour1');
            $colour2 = $this->input->post('colour2');
            $colour3 = $this->input->post('colour3');
            $season = $this->input->post('season');
            $user_id = $this->session->userdata('id');
            
            $box = new Cloth_entity(null, $abstract, 0, $description,
                                    $age, $sex, $user_id, null, Box::STATUS_DRAFT, 1, 
                                    $size, $colour1,
                                    $colour2, $colour3, $season);
            
            $id_box = $this->box->create($box);

            if(!empty($id_box))
            {
                $this->session->set_flashdata('message', 'Nueva caja creada.');
                redirect('admin/boxes');
            }
            else
            {
                $data = array();
                $data['message'] = "Ha habido un error interno, no se ha podido crear la caja.";
                $this->load->view('admin/boxes/cloth_form', $data);
            }
        }
    }
    
    public function edit_cloth($id = null)
    {
        $this->load->library('form_validation');
        $this->load->library('authorization');
        $this->load->library('box');
       
        $data['id'] = $id;
        $this->form_validation->set_rules('age', 'Edad', 'trim|required|xss_clean');
        $this->form_validation->set_rules('sex', 'Sexo', 'trim|required|xss_clean');
        $this->form_validation->set_rules('abstract', 'Resumen', 'trim|required|max_length[100]|xss_clean');
        $this->form_validation->set_rules('description', 'Descripci&oacute;n', 'trim|required|max_length[500]|xss_clean');
        $this->form_validation->set_rules('size', 'Talla', 'trim|required|xss_clean');
        $this->form_validation->set_rules('colour1', 'Color 1', 'trim|required|xss_clean');
        $this->form_validation->set_rules('colour2', 'Color 2', 'trim|required|xss_clean');
        $this->form_validation->set_rules('colour3', 'Color 3', 'trim|required|xss_clean');
        $this->form_validation->set_rules('season', 'Estacion', 'trim|required|xss_clean');
        $boxes = $this->box->find_by('id', $id, TRUE);
        
        if (empty($boxes))
        {
            $data['message'] = "No se ha puesto en la URL el identificador de caja o es incorrecta.";
            $box = new Cloth_entity();
        }
        else
        {
            $box = $boxes[0];
        }
        
        if ($this->form_validation->run() == FALSE)
        {
            $data['box'] = $box;
            $this->load->view('admin/boxes/cloth_form', $data);
        }
        else
        {
            $age = $this->input->post('age');
            $sex = $this->input->post('sex');
            $abstract = $this->input->post('abstract');
            $description = $this->input->post('description');
            $size = $this->input->post('size');
            $colour1 = $this->input->post('colour1');
            $colour2 = $this->input->post('colour2');
            $colour3 = $this->input->post('colour3');
            $season = $this->input->post('season');
            $user_id = $this->session->userdata('id');
            
            $box_update = new Cloth_entity($id, $abstract, 0, $description,
                                    $age, $sex, $user_id, null, Box::STATUS_DRAFT, 1, 
                                    $size, $colour1,
                                    $colour2, $colour3, $season);
            $updated = $this->box->update($box_update);
            
            if($updated)
            {
                $this->session->set_flashdata('message', 'Caja actualizada correctamente.');
                redirect('admin/boxes');
            }
            else
            {
                $data['box'] = $box;
                $data['error'] = "El email ya existe.";
                $this->load->view('admin/boxes/box_form', $data);
            }
        }
    }
    
    public function delete($id = null)
    {
        $this->load->library('box');
        $deleted = $this->box->delete($id);
        
        if ($deleted)
        {
           $this->session->set_flashdata('message', "Eliminada caja $id.");
        }
        else
        {
           $this->session->set_flashdata('message', "No se pudo eliminar la caja $id.");
        }
        
        redirect('admin/boxes');
    }
    
    
    public function year_check()
    {
        $this->load->library('form_validation');
        $day = $this->input->post('day');
        $month = $this->input->post('month');
        $year = $this->input->post('year');
        
        $d = date('d', strtotime("$day.$month.$year"));
        $m = date('m', strtotime("$day.$month.$year"));
        $y = date('Y', strtotime("$day.$month.$year"));
        if ($y != $year || $m != $month || $d != $day)
        {
            $classname = get_class();
            $this->form_validation->set_message($classname.'->year_check', 'La fecha no es v&aacute;lida.');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    
}

/* End of file box.php */
/* Location: ./application/controllers/box.php */