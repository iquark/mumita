<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Splash extends CI_Controller 
{
    public function index()
    {
        $session_id = $this->session->userdata('session_id');
        $this->load->view('splash/index');
    }
    
    public function save()
    {
        $this->load->helper(array('form', 'url'));
        $this->load->library('customer');

        $this->load->library('form_validation');

        $this->form_validation->set_rules('email', 'Correo electr&oacute;nico', 'trim|required|valid_email');
        $this->form_validation->set_rules('name', 'Nombre', 'trim|required|xss_clean');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('splash/index');
        }
        else
        {
            // TODO store into the db
            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $ip = $this->customer->get_ip_address();
            
            $id_customer = $this->customer->store($name, $email, $ip);
            
            if(!empty($id_customer))
            {
                $data['register_success'] = 1;
            }
            else
            {
                $data['register_error'] = "Ha ocurrido un error, vuelva a intentarlo pasados unos minutos y si persiste, por favor, comuniquelo a trav&eacute;s del formulario de contacto.";
            }
            $this->load->view('splash/index', $data);
        }
    }
    
    public function privacy()
    {
        $this->load->view('splash/privacy');
    }
}

/* End of file splash.php */
/* Location: ./application/controllers/splash.php */