<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test extends CI_Controller 
{

    public function __construct()
    {
        parent::__construct();

        $logged_in = $this->session->userdata('logged_in');

        if (!$logged_in) 
        {
            // tener en cuenta para un futuro usar uri_string() para 
            // redirigir tras login
            redirect("acceder", 'refresh'); 
        }
    }

    public function index()
    {
        $this->load->view('test/index');
    }
    
    public function mrw($operation=null)
    {
        $this->load->view("test/mrw/$operation");
    }
    
}

/* End of file test.php */
/* Location: ./application/controllers/test.php */