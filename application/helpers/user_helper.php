<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

if ( ! function_exists('member_tiny'))
{
	function member_tiny($user)
	{ 
?>
            <div class="member">
                <?php echo photo_tiny($user);?>
                <div class="data">
                    <span><?php echo anchor('perfil/'.$user->getId(),$user->getFullName(),'title="Visita el perfil de '.$user->getFullName().'"'); ?></span>
                    <span><?php show_valoration($user->getValoration()); ?></span>
                    <?php follow_button_big($user); ?>
                </div>
                <div class="selling">
                    <?php 
                        $abstract_sell = user_abstract_sell($user->getBoxes()); 
                        if (!empty($abstract_sell))
                        {
                            echo "Tiene a la venta: $abstract_sell";
                        }
                        else
                        {
                            echo "No tiene cajas a la venta.";
                        }
                    ?>
                </div>
            </div>
<?php	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('member_admin'))
{
	function member_admin($user)
	{ 
            $CI =& get_instance();
            $logged_in = $CI->session->userdata('logged_in');
            $role_level = $CI->session->userdata('role_level');

            if ($logged_in && $role_level>0) 
            {
?>
            <div class="member long<?php echo ($user->getRoleLevel()>0)?" admin":""; ?>">
                <div class="photo"><?php echo photo_tiny($user);?>&nbsp;</div>
                <div class="data">
                    <span class="name member-<?php echo $user->getId(); ?>"><?php echo $user->getFullName(); ?></span>
                    <span class="address member-<?php echo $user->getId(); ?>"><?php echo $user->getShippingAddress()->getBrief(); ?></span>
                    <span class="valoration member-<?php echo $user->getId(); ?>">* * * * </span>
                </div>
                <div class="data">
                    <span class="sex member-<?php echo $user->getId(); ?>"><?php echo ($user->getSex()==0)?"Hombre":"Mujer"; ?></span>
                    <span class="active member-<?php echo $user->getId(); ?>"><?php echo ($user->getActive()==0)?"Inactivo":"Activo"; ?></span>
                    <span class="created_at"><?php echo $user->getCreatedAt(); ?></span>
                </div>
                
                <div class="dataend">
                    <span class="edit member-<?php echo $user->getId(); ?>"><?php echo anchor(base_url('/admin/users/edit/'.$user->getId()),'Editar','id="edit-'.$user->getId().'"'); ?></span>
                    <span class="delete member-<?php echo $user->getId(); ?>"><?php echo anchor(base_url('/admin/users/delete/'.$user->getId()),'Borrar','id="delete-'.$user->getId().'"'); ?></span>
                </div>
            </div>
<?php       }
        }
}

// ------------------------------------------------------------------------

if ( ! function_exists('photo_tiny'))
{
    function photo_tiny($user)
    { 
        if (!empty($user) && file_exists(dirname(__FILE__)."/../../assets/images/users/".md5($user->getId().$user->getSalt())."/tiny.png")) {
          $photo = "<img src=".base_url("assets/images/users/".md5($user->getId().$user->getSalt())."/tiny.png")." class=\"photo_user\" />";
        } else {
          $photo = "<img src=". base_url("assets/images/users/tiny.png")." class=\"photo_user\" />";
        }
        
        return $photo;
    }
}

// ------------------------------------------------------------------------

if ( ! function_exists('photo_normal'))
{
    function photo_normal($user)
    {
        if (!empty($user) && file_exists(dirname(__FILE__)."/../../assets/images/users/".md5($user->getId().$user->getSalt())."/normal.png")) {
          $photo = "<img src=".base_url("assets/images/users/".md5($user->getId().$user->getSalt())."/normal.png")." class=\"photo_user\" />";
        } else {
          $photo = "<img src=". base_url("assets/images/users/normal.png")." class=\"photo_user\" />";
        }
        
        return $photo;
    }
}

// ------------------------------------------------------------------------

if ( ! function_exists('medals'))
{
    function medals($user)
    {  
        echo "<ul>";
        
        for ($idx=0; $idx<3; $idx++) 
        {
            echo "<li>&copy;</li>";
        }
        
        echo "</ul>";
    }
}

// ------------------------------------------------------------------------

if ( ! function_exists('follow_button_big'))
{
    function follow_button_big($user)
    {
        $CI =& get_instance();
    	
        $our_id = $CI->session->userdata("id");
        if ($user->getId() != $our_id): ?>
            <div class="member_follow">
<?php       // check if the actual user is following him
            if ($user->getFollowing())
            {
                $text_button = "Le est&aacute;s siguiendo";
                $class_button = "following";
                $url = "unfollow";
            }
            else
            {
                $text_button = "Seguir";
                $class_button = "";
                $url = "follow";
            }

            echo form_open(base_url('perfil/'.$user->getId().'/'.$url)); ?>
                    <input type="submit" id="follow" name="follow" class="<?php echo $class_button; ?>" value="<?php echo $text_button; ?>" />
                </form>
            </div>
<?php   else: ?>
            <div class="member_follow">Eres t&uacute;</div>
<?php
        endif;
    }
}

// ------------------------------------------------------------------------

if ( ! function_exists('profile_header'))
{
    function profile_header($user)
    {
?>
            <div class="profile">
                <img src="" title="Esta es tu foto!" alt="Tu foto" />
            </div>
<?php
    }
}

// ------------------------------------------------------------------------

if ( ! function_exists('profile_link'))
{
    function profile_link($user)
    {
        $link = anchor("perfil/".$user->getId(), 
                       $user->getFullName(),
                       "title='ver el perfil de ".$user->getFullName()."'");
        
        return $link;
    }
}

// ------------------------------------------------------------------------

/* End of file user_helper.php */
/* Location: ./application/helpers/user_helper.php */
