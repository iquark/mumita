<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

if ( ! function_exists('your_info'))
{
    function your_info($timeline)
    {   
        $CI =& get_instance();
    	
        $our_id = $CI->session->userdata('id');
     
        $output = "";
        
        switch($timeline->getIdAction())
        {
            case Timeline::BOX_BOUGHT:
                $bought = true;
            case Timeline::BOX_SENT:
                $sent = true;
            case Timeline::BOX_RECEIVED:
                if ($timeline->getIdUser1() == $our_id)
                {
                    $output = "Tu caja ".box_link($timeline->getBox())." ha sido <strong> ";

                    if(isSet($bought) && $bought) 
                    {
                        $output .= "comprada</strong> por ";
                    } 
                    else
                    {
                        if(isSet($sent) && $sent)
                        {
                            $output .= "enviada</strong> a ";
                        }
                        else
                        {
                            $output .= "recibida</strong> por ";
                        }
                    }
                    $output .= profile_link($timeline->getUser2());
                }
                else
                {
                    if($bought) 
                    {
                        $output .= "Has comprado";
                    } 
                    else
                    {
                        if($sent)
                        {
                            $output .= "Se te ha enviado";
                        }
                        else
                        {
                            $output .= "Has recibido";
                        }
                    }
                    
                    $output .= " la caja ".box_link($timeline->getBox())." de ";
                    $output .= profile_link($timeline->getUser1());
                }
                
                break;
            case Timeline::BOX_COMMENTED:
                if ($timeline->getIdUser1() == $our_id)
                {
                    $output = "Has ";
                }
                else
                {
                    $output = profile_link($timeline->getUser1())." ha ";
                }
                $output .= "comentado la caja ".box_link($timeline->getBox());
                $output .= " ".box_comment_link($timeline).".";
                break;
            case Timeline::BOX_DELETED:
                $deleted = true;
            case Timeline::BOX_CREATED:
                $created = true;
            case Timeline::BOX_UPDATED:
                if ($timeline->getIdUser1() == $our_id)
                {
                    $output = "Has ";
                }
                else
                {
                    $output = profile_link($timeline->getUser1())." te ha ";
                }
                
                if (isSet($created) && !$created && isSet($deleted) &&  $deleted)
                {
                    $output .= "eliminado";
                    $link = "<em>".box_short_name($timeline->getBox())."</em>";
                }
                else
                {
                    $link = box_link($timeline->getBox());
                    if (isSet($deleted) && $deleted)
                    {
                        $output .= "eliminado";
                        
                    }
                    else
                    {
                        $output .= "actualizado";
                    }
                }
                $output .= " la caja ".$link.".";
                break;
            case Timeline::USER_FOLLOW:
                if ($timeline->getIdUser1() == $our_id)
                {
                    $output = "Estas siguiendo a ".profile_link($timeline->getUser2()).".";
                }
                else
                {
                    $output = profile_link($timeline->getUser1())." te est&aacute; siguiendo.";
                }
                break;
            case Timeline::USER_UNFOLLOW:
                if ($timeline->getIdUser1() == $our_id)
                {
                    $output = "Has dejado de seguir a ".profile_link($timeline->getUser2()).".";
                }
                else
                {
                    $output = profile_link($timeline->getUser1())." te ha dejado de seguir.";
                }
                break;
            case Timeline::USER_COMMENTED:
                if ($timeline->getIdUser1() == $our_id)
                {
                    $output = "Has comentado al usuario ".profile_link($timeline->getUser2()).".";
                }
                else
                {
                    $output = profile_link($timeline->getUser1())." te ha comentado.";
                }
                break;
            
        }
        
        $output = "<div class='timeline'><span class='date'>".$timeline->getDate()."</span>: ".$output."</div>";
        
	return $output;
    }
}

// ------------------------------------------------------------------------

if ( ! function_exists('friends_info'))
{
    function friends_info($timeline)
    {   
        $CI =& get_instance();
    	
        $our_id = $CI->session->userdata('id');
     
        $output = "";
        
        switch($timeline->getIdAction())
        {
            case Timeline::BOX_BOUGHT:
                $output = profile_link($timeline->getUser1())." ha vendido ";
                $output .= "la caja ".box_link($timeline->getBox());
                
                break;
            case Timeline::BOX_COMMENTED:
                $output = profile_link($timeline->getUser1())." ha comentado ";
                $output .= "la caja ".box_link($timeline->getBox())." ".box_comment_link($timeline).".";
                
                break;
            case Timeline::BOX_CREATED:
                $output = profile_link($timeline->getUser1())." ha creado ";
                $output .= "la caja ".box_link($timeline->getBox());
                
                break;
            case Timeline::BOX_UPDATED:
                $output = profile_link($timeline->getUser1())." ha actualizado ";
                $output .= "la caja ".box_link($timeline->getBox());
                
                break;
            case Timeline::USER_FOLLOW:
                if ($timeline->getIdUser2() == $our_id)
                {
                    $output = profile_link($timeline->getUser1()). " te est&aacute; siguiendo.";
                }
                else
                {
                    $output = profile_link($timeline->getUser1())." est&aacute; siguiendo a ".profile_link($timeline->getUser2()).".";
                }
                break;
            case Timeline::USER_UNFOLLOW:
                if ($timeline->getIdUser2() == $our_id)
                {
                    $output = profile_link($timeline->getUser1())." te ha dejado de seguir.";
                }
                else
                {
                    $output = "Has dejado de seguir a ".profile_link($timeline->getUser2()).".";
                }
                break;
            case Timeline::USER_COMMENTED:
                if ($timeline->getIdUser2() == $our_id)
                {
                    $output = profile_link($timeline->getUser1())." te ha comentado.";
                }
                else
                {
                    $output = profile_link($timeline->getUser1())." ha comentado a ";
                    $output .= profile_link($timeline->getUser2()).".";
                }
                break;
            
        }
        
        $output = "<div class='timeline'><span class='date'>".$timeline->getDate()."</span>: ".$output."</div>";
        
	return $output;
    }
}

// ------------------------------------------------------------------------

if ( ! function_exists('box_comment_link'))
{
    function box_comment_link($timeline)
    {
        $link =  anchor("caja/".$timeline->getIdBox()."#".$timeline->getIdComment(),
                "(Ver comentario)","title='Ver comentario'");
        return $link;
    }
}



/* End of file timeline_helper.php */
/* Location: ./application/helpers/timeline_helper.php */
