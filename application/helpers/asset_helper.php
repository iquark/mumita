<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

if ( ! function_exists('asset_url'))
{
    function asset_url($url)
    {
        return base_url("assets/$url");
	
    }
}

// ------------------------------------------------------------------------

if ( ! function_exists('load_image'))
{
    function load_image($url, $title="")
    {
        return '<img src="'.base_url("assets/images/$url").'" alt="'.$title.'" title="'.$title.'" />';
    }
}

// ------------------------------------------------------------------------

if ( ! function_exists('load_component'))
{
    function load_component($url, $title="")
    {
        return load_image("components/$url", $title);
    }
}
// ------------------------------------------------------------------------

if ( ! function_exists('load_jquery'))
{
    function load_jquery()
    { 
?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
<?php	
    }
}

// ------------------------------------------------------------------------

if ( ! function_exists('load_js_third_party'))
{
    function load_js_third_party($name)
    { 
?>
            <script src="<?php echo base_url("assets/js/third_party/$name/$name.js"); ?>" type="text/javascript"></script>
<?php	
    }
}

// ------------------------------------------------------------------------

if ( ! function_exists('load_css_third_party'))
{
	function load_css_third_party($name)
    { 
?>
        <link rel="stylesheet" href="<?php echo base_url("assets/css/third_party/$name.css"); ?>" type="text/css" media="screen, projection" />
<?php	
    }
}

// ------------------------------------------------------------------------

if ( ! function_exists('load_js'))
{
    function load_js($file)
    { 
?>
        <script src="<?php echo base_url("assets/js/$file.js"); ?>" type="text/javascript"></script>
<?php	
    }
}

// ------------------------------------------------------------------------

if ( ! function_exists('load_css'))
{
    function load_css($file)
    { 
?>
        <link rel="stylesheet" href="<?php echo base_url("assets/css/$file.css"); ?>" type="text/css" media="screen, projection" />
<?php	
    }
}

// ------------------------------------------------------------------------

if ( ! function_exists('show_valoration'))
{
    function show_valoration($value)
    { 
        $valoration = intval($value);
        $i = 0;
        // yellow stars
        for($i=0; $i<$valoration; $i++) {
            echo load_image("components/star_yes.png");
        }
        // white stars
        for(; $i<5; $i++) {
            echo load_image("components/star_no.png");
        }
    }
}

// ------------------------------------------------------------------------


/* End of file asset_helper.php */
/* Location: ./application/helpers/asset_helper.php */
