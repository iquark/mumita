<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(dirname(__FILE__) . '/../libraries/entities/Cloth_entity.php');

// ------------------------------------------------------------------------

if ( ! function_exists('box_tiny'))
{
	function box_tiny($box)
	{ 
?>
            <div class="box">
                <?php echo photo_box_tiny($box);?>
                <div class="data">
                    <span><?php echo anchor('perfil/'.$box->getUser()->getFullName(),'title="Visita el perfil de '.$box->getUser()->getFullName().'"'); ?></span>
                    <span>Valencia</span>
                    <span>* * * * </span>
                </div>
            </div>
<?php	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('box_admin'))
{
	function box_admin($box)
	{ 
            $CI =& get_instance();
            $logged_in = $CI->session->userdata('logged_in');
            $role_level = $CI->session->userdata('role_level');

            if ($logged_in && $role_level>0): ?>
            <div class="box long" style="height:30px;clear:both">
                <div class="data">
                    <span class="colours box-<?php echo $box->getId(); ?>" style="margin: auto 20px; float: left">
                        <?php 
                            $colours = $box->getColours(); 
                            foreach($colours as $colour)
                            {
                                echo "<div class=\"colour\" style=\"background-color:$colour;\"></div>";
                            }
                          ?>
                    </span>
                    <div class="abstract box-<?php echo $box->getId(); ?>" style="width: 300px; float:left; background: lightblue "><?php echo $box->getAbstract(); ?></div>
                   <div class="abstract box-<?php echo $box->getId(); ?>" style="width: 100px; float:left; background: white; text-align: center">
                    <?php 
                        $valoration = intval($box->getValoration());
                        $i = 0;
                        // yellow stars
                        for($i=0; $i<$valoration; $i++) {
                            echo load_image("components/star_yes.jpg");
                        }
                        // white stars
                        for(; $i<5; $i++) {
                            echo load_image("components/star_no.jpg");
                        }
                    ?>
                    </div>
                    <div class="abstract box-<?php echo $box->getId(); ?>" style="width: 350px; float:left; background: lightpink">
                        <?php echo box_season($box->getSeason()); ?>
                        <?php echo box_sex($box->getSex()); ?>
                    </div>
                </div>
                
                <div class="dataend">
                    <span class="edit box-<?php echo $box->getId(); ?>">
                        <?php echo anchor(base_url('/admin/boxes/edit/'.$box->getId()),'Editar','id="edit-'.$box->getId().'"'); ?>
                    </span>
                    <span class="delete box-<?php echo $box->getId(); ?>">
                        <?php echo anchor(base_url('/admin/boxes/delete/'.$box->getId()),'Borrar','id="delete-'.$box->getId().'"'); ?>
                    </span>
                </div>
            </div>
<?php       endif;
        }
}

// ------------------------------------------------------------------------

if ( ! function_exists('box_user'))
{
    function box_user($box, $last_row=false)
    { 
        $CI =& get_instance();
        $logged_in = $CI->session->userdata('logged_in');
        $id = $CI->session->userdata('id');
        $box = (empty($box))?new Cloth_entity():$box;
        
        if ($logged_in): ?>
        <div class="box-list<?php echo ($last_row)?" last-box-row":""; ?>">
            <div class="head">
                <?php echo box_size($box); ?>
                <?php echo box_season($box->getSeason()); ?>
                <?php echo box_sex($box->getSex()); ?>
                <span class="colours">
                    <?php 
                        $colours = $box->getColours(); 
                        foreach($colours as $colour)
                        {
                            echo "<div class=\"colour\" style=\"background-color:#$colour;\"></div>";
                        }
                        ?>
                </span>
            </div>
            
            <div class="photo">
                <?php echo photo_box_normal($box); ?>
            </div>
            <div class="abstract">
                <?php echo substr($box->getAbstract(), 0, 120).((strlen($box->getAbstract())>120)?"...":""); ?>
            </div>
            <div class="metadata">
                <span class="left">
                    <?php 
                        $user = $box->getUser();
                        $link = ($id==$user->getId())?"mi_perfil":"/perfil/".$user->getId();
                        echo anchor(base_url("/$link"), $user->getFullName()); ?>
                    <div class="valoration"><?php show_valoration($box->getValoration()); ?>
                    <?php echo anchor(base_url('/caja/'.$box->getId()),'('.count($box->getComments()).' comentarios)'); ?>
                    </div>
                </span>
                <span class="plusinfo">
                    <?php echo anchor("/caja/".$box->getId(), load_component("plusinfo.png")); ?>
                </span>
            </div>
                
            <div class="actions">
                <?php echo anchor(base_url('/caja/editar/'.$box->getId()),'Editar','id="edit-'.$box->getId().'" class="edit"'); ?>
                <?php echo anchor(base_url('/caja/borrar/'.$box->getId()),'Borrar','id="delete-'.$box->getId().'" class="delete"'); ?>
            </div>
        </div>
<?php   endif;
    }
}
// ------------------------------------------------------------------------

if( ! function_exists('box_slim'))
{
    function box_slim($box)
    {
?>
        <div class="box tiny">
            <div class="box-top">
                <?php echo box_size($box); ?>
                <?php echo box_season($box->getSeason()); ?>
                <?php echo box_sex($box->getSex()); ?>
                <span class="colours" style="float: right; margin-top: 5px">
                    <div style="border-radius:5px;width:10px; height:10px; background-color:#ccc; float: left; margin-right:5px"></div>
                    <div style="border-radius:5px;width:10px; height:10px; background-color:#cdcdcd; float: left; margin-right:5px"></div>
                    <div style="border-radius:5px;width:10px; height:10px; background-color:#434343; float: left; margin-right:5px"></div>
                </span>
            </div>
            <div class="photo">
                <?php echo photo_box_tiny($box); ?>
            </div>
            <div class="abstract">
                <?php echo $box->getAbstract(); ?>
            </div>  
            <div class="bottom">
                <?php 
                    $plusinfo = load_component("masinfo.gif", "Mas informacion");
                    echo anchor("/caja/".$box->getId(),$plusinfo);
                ?>
                <div class="right">
                    <?php show_valoration($box->getValoration()); ?>
                </div>
            </div>
        </div>
<?php
    }
    
}

// ------------------------------------------------------------------------

if ( ! function_exists('box_user_2'))
{
	function box_user_2($box)
	{ 
            $CI =& get_instance();
            $logged_in = $CI->session->userdata('logged_in');

            if ($logged_in): ?>
            <div class="box long" style="height:30px;clear:both">
                <div class="data">
                    <span class="colours box-<?php echo $box->getId(); ?>" style="margin: auto 20px; float: left">
                        <?php 
                            $colours = $box->getColours(); 
                            foreach($colours as $colour)
                            {
                                echo "<div style=\"border-radius:5px;width:10px; height:10px; background-color:#$colour; float: left; margin-right:10px\"></div>";
                            }
                          ?>
                    </span>
                    <div class="abstract box-<?php echo $box->getId(); ?>" style="width: 300px; float:left; background: lightblue "><?php echo $box->getAbstract(); ?></div>
                    <div class="abstract box-<?php echo $box->getId(); ?>" style="width: 100px; float:left; background: white; text-align: center">
                    <?php 
                        $valoration = intval($box->getValoration());
                        $i = 0;
                        // yellow stars
                        for($i=0; $i<$valoration; $i++) {
                            echo load_image("components/star_yes.jpg");
                        }
                        // white stars
                        for(; $i<5; $i++) {
                            echo load_image("components/star_no.jpg");
                        }
                    ?>
                    </div>
                    <div class="abstract box-<?php echo $box->getId(); ?>" style="width: 350px; float:left; background: lightpink">
                        <?php echo box_season($box->getSeason()); ?>
                        <?php echo box_sex($box->getSex()); ?>
                    </div>
                </div>
                
                <div class="dataend">
                    <span class="edit box-<?php echo $box->getId(); ?>">
                        <?php echo anchor(base_url('/caja/editar/'.$box->getId()),'Editar','id="edit-'.$box->getId().'"'); ?>
                    </span>
                    <span class="delete box-<?php echo $box->getId(); ?>">
                        <?php echo anchor(base_url('/caja/borrar/'.$box->getId()),'Borrar','id="delete-'.$box->getId().'"'); ?>
                    </span>
                </div>
            </div>
<?php       endif;
        }
}

// ------------------------------------------------------------------------

if ( ! function_exists('photo_box_tiny'))
{
    function photo_box_tiny($box)
    {
        if (file_exists(dirname(__FILE__)."/../../assets/images/boxes/".md5($box->getId())."/tiny.png")) {
          $photo = "<img src=".base_url("assets/images/boxes/".md5($box->getId())."/tiny.png")." />";
        } else {
          $photo = "<img src=". base_url("assets/images/boxes/tiny.png")." />";
        }
        
        return $photo;
    }
}

// ------------------------------------------------------------------------

if ( ! function_exists('photo_box_normal'))
{
    function photo_box_normal($box)
    {
        if (!empty($box) && file_exists(dirname(__FILE__)."/../../assets/images/boxes/".md5($box->getId())."/normal.png")) {
          $photo = "<img src=".base_url("assets/images/boxes/".md5($box->getId())."/normal.png")." />";
        } else {
          $photo = "<img src=". base_url("assets/images/boxes/normal.png")." />";
        }
        
        return $photo;
    }
}

// ------------------------------------------------------------------------

if ( ! function_exists('photo_box_big'))
{
    function photo_box_big($box)
    {
        if (!empty($box) && file_exists(dirname(__FILE__)."/../../assets/images/boxes/".md5($box->getId())."/big.png")) {
          $photo = "<img src=".base_url("assets/images/boxes/".md5($box->getId())."/big.png")." />";
        } else {
          $photo = "<img src=". base_url("assets/images/boxes/big.png")." />";
        }
        
        return $photo;
    }
}

// ------------------------------------------------------------------------

if ( ! function_exists('box_season'))
{
    function box_season($box_season)
    {
        $season = null;
        $component = null;
        
        if (!empty($box_season) || $box_season == 0) {
            $title = "";
            switch ($box_season) {
                case 0: // summer
                    $season = "summer";
                    $title = "Verano";
                    break;
                case 1: // autumn
                    $season = "autumn";
                    $title = "Oto&ntilde;o";
                    break;
                case 2: // winter
                    $season = "winter";
                    $title = "Invierno";
                    break;
                case 3: // spring
                    $season = "spring";
                    $title = "Primavera";
                    break;
                default:
                    $season_lower = strtolower($box_season);
                    if (preg_match("/(summer|winter|autumn|spring)/", $season_lower)) {
                        $season = $season_lower;
                    }
                    $title = $season;
                    break;
            }
            
            if (!empty($season)) {
                $component = load_component("$season.png", $title);
            }
        }
        
        return $component;
    }
}

// ------------------------------------------------------------------------

if ( ! function_exists('box_sex'))
{
    function box_sex($box_sex)
    {
        $sex = null;
        $component = null;
        
        switch($box_sex)
        {
            case 1: // girl
                $sex = "girl";
                $title = "Ni&ntilde;a";
                break;
            case 2: // kid
                $sex = "boy";
                $title = "Ni&ntilde;o";
                break;
            default: // unisex
                $sex = "unisex";
                $title = "Unisex";
                break;
        }
        
        if (!empty($sex)) {
            $component = load_component("$sex.png", $title);
        }
        
        return $component;
    }
}

// ------------------------------------------------------------------------

if ( ! function_exists('box_size'))
{
    function box_size($box)
    {
        $option = null;
        
        if (!empty($box))
        {
            $options = array(
                '1'  => '56 cm',
                '2'  => '62 cm',
                '3'  => '68 cm',
                '4'  => '74 cm',
                '5'  => '80 cm',
                '6'  => '86 cm',
                '7'  => '92 cm',
                '8'  => '104 cm',
                '9'  => '116 cm',
                '10'  => '128 cm'
                );
            $option = $options[$box->getIdSize()];
        }
        
        return $option;
    }
}

// ------------------------------------------------------------------------

if (!function_exists('user_abstract_sell'))
{
    function user_abstract_sell($boxes) {
        $seasons = array();
        $sex = array();
        $output = "";
        
        foreach($boxes as $box) {
            if($box->getStatus() == Box::STATUS_PUBLISHED)
            {
                array_push($seasons, $box->getSeason());
                array_push($sex, $box->getSex());
            }
        }
        
        $seasons = array_flip($seasons);
        $sex = array_flip($sex);
        $seasons = array_flip($seasons);
        $sex = array_flip($sex);

        foreach($seasons as $s) {
            $output .= box_season($s);
        }
        
        foreach($sex as $s) {
            $output .= box_sex($s);
        }
        
        return $output;
    }
}

// ------------------------------------------------------------------------

if ( ! function_exists('box_link'))
{
    function box_link($box)
    {
        $link = "";
        if (!empty($box))
        {
            $link =  anchor("caja/".$box->getId(), 
                        box_short_name($box, 75), 
                        "title='".$box->getAbstract()."'");
        }
        return $link;
    }
}

// ------------------------------------------------------------------------

if ( ! function_exists('box_short_name'))
{
    function box_short_name($box, $limit = 10)
    {
        $name = "";
        if (!empty($box))
        {
            $name = substr($box->getAbstract(),0, $limit);
            if (strlen($box->getAbstract())>$limit) {
                $name .= "...";
            }
        }
        
        return $name;
    }
}

// ------------------------------------------------------------------------

/* End of file box_helper.php */
/* Location: ./application/helpers/box_helper.php */
