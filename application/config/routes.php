<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "splash";
$route['404_override'] = 'errors/error404';

$route['enviar'] = 'splash/save';
$route['privacidad'] = 'splash/privacy';

$route['home'] = 'publics';
//$route['acceder/(:any)'] = 'security/login/$1';
$route['acceder'] = 'security/index';
$route['salir'] = 'security/logout';
$route['comprobar'] = 'security/login';
$route['activar/(:any)/(:any)'] = 'security/activate/$1/$2';
$route['contrasena_olvidada'] = 'security/forgot_password';
$route['correo_enviado'] = 'security/forgot_sent';

$route['panel'] = 'privates/dashboard';
$route['registro'] = 'publics/register';
$route['registro_completado'] = 'publics/register_success';
$route['mi_perfil'] = 'profile/page_1';
$route['mi_perfil/1'] = 'profile/page_1';
$route['mi_perfil/editar/1'] = 'profile/page_1/edit';
$route['mi_perfil/guardar/1'] = 'profile/save_1';
$route['mi_perfil/2'] = 'profile/page_2';
$route['mi_perfil/guardar/2'] = 'profile/save_2';

// YOUR BOXES
$route['cajas'] = 'user_boxes/index';
$route['cajas/usuario/(:num)'] = 'user_boxes/by_user/$1';
$route['cajas/buscar'] = 'user_boxes/search';
$route['cajas/buscar/(:num)'] = 'user_boxes/search/$1';
$route['cajas/pag'] = 'user_boxes/index';
$route['cajas/pag/(:num)'] = 'user_boxes/index';
$route['caja/nueva'] = 'user_boxes/create_cloth';
$route['caja/(:num)'] = 'user_boxes/view_cloth/$1';
$route['caja/editar/(:num)'] = 'user_boxes/edit_cloth/$1';
$route['caja/borrar/(:num)'] = 'user_boxes/delete/$1';
$route['caja/comentar/(:num)'] = 'user_boxes/comment/$1';
$route['caja/valorar/(:num)'] = 'user_boxes/valorate/$1';

// COMMUNITY
$route['comunidad'] = 'community/index';
$route['comunidad/pag'] = 'community/index';
$route['comunidad/pag/(:num)'] = 'community/index';
$route['comunidad/buscar'] = 'community/search';
$route['comunidad/buscar/(:num)'] = 'community/search';
$route['perfil/(:num)'] = 'community/profile/$1';
$route['perfil/(:num)/follow'] = 'community/follow/$1';
$route['perfil/(:num)/unfollow'] = 'community/unfollow/$1';

// ADMIN
$route['admin'] = 'admin/index';
$route['admin/users'] = 'admin_users';
$route['admin/users/new'] = 'admin_users/create';
$route['admin/users/edit/(:num)'] = 'admin_users/edit/$1';
$route['admin/users/delete/(:num)'] = 'admin_users/delete/$1';
$route['admin/boxes'] = 'admin_boxes';
$route['admin/boxes/new_cloth'] = 'admin_boxes/create_cloth';
$route['admin/boxes/edit/(:num)'] = 'admin_boxes/edit_cloth/$1';
$route['admin/boxes/delete/(:num)'] = 'admin_boxes/delete/$1';

// ORDERS
$route['pedido/crear'] = 'orders/create';

// TEST
$route['test'] = 'test/index';
$route['test/mrw'] = 'test/mrw/index';
$route['test/mrw/transmEnvio'] = 'test/mrw/transmenvio';

/* End of file routes.php */
/* Location: ./application/config/routes.php */