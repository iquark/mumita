<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(dirname(__FILE__) . '/mrw/TransmEnvio.php');


/***
 * Interface for all MRW's operations
 * 
 */
class Mrw
{
    public function transmEnvio()
    {
        $CI=& get_instance();
        $transmEnvio = new TransmEnvio();
        
        $transmEnvio->run();
        
        return true;
    }
  
}

/* End of file Mrw.php */