<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(dirname(__FILE__) . '/entities/Customer_entity.php');

class Customer
{
    public function get_all()
    {
        $CI=& get_instance();
        $CI->load->model('Customer_model');
        
        $customers = $CI->Customer_model->get_all();
        
        return $customers;
    }
    
    public function find_by($field, $value)
    {
        $CI=& get_instance();
        $CI->load->model('Customer_model');
        
        $customers = $CI->Customer_model->find_by($field, $value);
        
        return $customers;
    }
    
    public function delete($id = null)
    {
        $CI =& get_instance();
    	$CI->load->model('Customer_model');
    	$deleted = false;
        
        $customers = $CI->Customer_model->find_by('id', $id);
        if (!empty($customers))
        {
            $deleted = $CI->Customer_model->delete($customers[0]);
        }
        
        return $deleted;
    }
    
    public function update($customer=null)
    {
        $CI=& get_instance();
        $CI->load->model('Customer_model');
        $updated = false;
        
        if (!empty($customer))
        {
            $updated = $CI->Customer_model->update($customer);
        }
        
        return $updated;
    }
    
    function send_email($id)
    {
        $CI=& get_instance();
        $CI->load->library('email');
    	$CI->load->model('Customer_model');
        
        $customers = $CI->Customer_model->find_by('id', $id);
        if (!empty($customers))
        {
            $customer = $customers[0];
            
            $CI->email->from("noresponder@mumita.com", 'Equipo de Mumita');
            $CI->email->to($customer->getEmail()); 

            $CI->email->subject('Mumita: hola');
            $CI->email->message('Hola '.$customer->getName().'!');	

            $CI->email->send();
            echo $CI->email->print_debugger();
        }
    }
    
    public function store($name, $email, $ip)
    {
    	$CI =& get_instance();
        $CI->load->model('Customer_model');
    	
        $customer = new Customer_entity(null, $name, $email, $ip, null);
        $id_customer = $CI->Customer_model->store($customer->getToInsert());
    	
    	return $id_customer;
    }
    
    function get_ip_address(){
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
            if (array_key_exists($key, $_SERVER) === true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe

                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        return $ip;
                    }
                }
            }
        }
    }

}

/* End of file Customer.php */