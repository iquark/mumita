<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(dirname(__FILE__) . '/entities/Box_entity.php');

class Box
{
    // box statuses
    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;
    const STATUS_BUYING = 2;
    const STATUS_WAITING_SEND = 3; 
    const STATUS_SENT = 4;
    const STATUS_DELIVERED = 5;
    const STATUS_SOLD = 6;
   
    public function get_all($num=null, $offset=null)
    {
        $CI=& get_instance();
        $CI->load->model('Box_model');
        
        $boxes = $CI->Box_model->get_all($num, $offset);
        
        return $boxes;
    }
    
    public function find_by($field, $value)
    {
        $CI=& get_instance();
        $CI->load->model('Box_model');
        
        $boxes = $CI->Box_model->find_by($field, $value);
        
        return $boxes;
    }
    
    public function create($box)
    {
    	$CI =& get_instance();
        
        switch($box->getCategory())
        {
            case 0:
                $CI->load->model('Cloth_model');
                $box_id = $CI->Cloth_model->create($box->getToInsert());
                break;
            default:
                $CI->load->model('Box_model');
                $box_id = $CI->Box_model->create($box->getToInsert());
                break;
        }
        $box->setId($box_id);
        $CI->timeline->box_action(Timeline::BOX_CREATED, $box->getId());
    	
    	return $box_id;
    }
    
    public function search_cloth($string, $sex, $size, $season)
    {
    	$CI =& get_instance();
    	$CI->load->model('Cloth_model');
        
        $string_array = explode(" ", $string);
        $user_id = null;
        $string = "";
        
        foreach($string_array as $s)
        {
            if(preg_match("/^user:[0-9]+$/", $s)) 
            {
                $user_id = explode(':', $s);
                $user_id = $user_id[1];
            }
            else
            {
                $string.= "$s ";
            }
        }
        
    	$boxes = $CI->Cloth_model->search($string, $sex, $size, $season, $user_id);
    	
    	// TODO filter by boxes that users are selling
    	
    	return $boxes;
    }
    
    public function delete($box_id = null)
    {
        $CI =& get_instance();
    	$CI->load->model('Box_model');
    	$deleted = false;
        $our_id = $CI->session->userdata('id');
        $role_level = $CI->session->userdata('role_level');
        
        $boxes = $CI->Box_model->find_by('id', $box_id);
        if(!empty($boxes))
        {
            $box = $boxes[0];
            $box_id = $box->getId();
            $owner_id = $box->getUser();

            $CI->timeline->box_action(Timeline::BOX_DELETED, $box->getId(), $box->getAbstract());
            // we only can delete our own boxes, at least we are an admin
            if (!empty($box_id) && ($owner_id==$our_id || $role_level>0))
            {
                switch($box->getCategory())
                {
                    case 0:
                        $CI->load->model('Cloth_model');
                        $deleted = $CI->Cloth_model->delete($box_id);
                        break;
                    default:
                        $deleted = $CI->Box_model->delete($box_id);
                        break;
                }
            }
        }
        
        return $deleted;
    }
    
    public function update($box=null)
    {
        $CI=& get_instance();
        $updated = false;
        
        if (!empty($box))
        {
            switch($box->getCategory())
            {
                case 0:
                    $CI->load->model('Cloth_model');
                    $updated = $CI->Cloth_model->update($box);
                    break;
                default:
                    $CI->load->model('Box_model');
                    $updated = $CI->Box_model->update($box);
                    break;
            }
            
            $CI->timeline->box_action(Timeline::BOX_UPDATED, $box->getId(), $box->getAbstract());
        }
        
        return $updated;
    }
    
    function send_email($id)
    {
        $CI=& get_instance();
        $CI->load->library('email');
    	$CI->load->model('Box_model');
        
        $boxes = $CI->Box_model->find_by('id', $id);
        if (!empty($boxes))
        {
            $box = $boxes[0];
            
            $CI->email->from("noresponder@mumita.com", 'Equipo de Mumita');
            $CI->email->to($box->getEmail()); 

            $CI->email->subject('Mumita: Activa tu cuenta');
            $CI->email->message('Hola '.$box->getFullName().'! Para activar tu cuenta debes entrar aquí­: '.anchor('activar/'.urlencode($box->getEmail()).'/'.$box->getActivationCode()).'.');	

            $CI->email->send();
            echo $CI->email->print_debugger();
        }
    }
    
    public function comment($box_id, $user_id, $comment)
    {
        $CI=& get_instance();
        
        if (!empty($box_id) && !empty($user_id) && !empty($comment))
        {
            $CI->load->model('Box_model');
            $id_comment = $CI->Box_model->comment($box_id, $user_id, $comment);
            
            $CI->timeline->box_action(Timeline::BOX_COMMENTED, $box_id, $comment, $id_comment);
        }
        
        return $id_comment;
    }
    
    public function get_comments($box_id)
    {
        $CI=& get_instance();
        $comments = array();
        
        if (!empty($box_id))
        {
            $CI->load->model('Box_model');
            $comments = $CI->Box_model->get_comments($box_id);
        }
        
        return $comments;
    }
    
    public function add_valoration($box_id, $user_id, $valoration)
    {
        $CI=& get_instance();
        $success = false;
        
        if (!empty($box_id) && !empty($user_id) && $valoration>=1 && $valoration<=5)
        {
            $CI->load->model("Box_model");
            $success = $CI->Box_model->add_valoration($box_id, $user_id, $valoration);
        }
        
        return $success;
    }
    
       
    public function create_avatars($file, $box_dir)
    {
        $return = false;
        $finfo = finfo_open(FILEINFO_MIME_TYPE); // devuelve el tipo mime de su extensión
        $mime = finfo_file($finfo, $file['full_path']);
        
        if(!is_dir($box_dir."/"))
        {
            mkdir($box_dir."/", 0777, true);
        }
        
        switch($mime)
        {
            case "image/jpeg":
                $tmp_image=imagecreatefromjpeg($file['full_path']);
                break;
            case "image/png":
                $tmp_image=imagecreatefrompng($file['full_path']);
                break;
            case "image/gif":
                $tmp_image=imagecreatefromgif($file['full_path']);
                break;
            default:
                $tmp_image = null;
        }
        
        if(!empty($tmp_image))
        {
            list($width, $height) = getimagesize($file['full_path']);
            $central = array('x'=>$width/2, 'y'=>$height/2);
            $new_width = $width;
            $new_height = $height;
            $big = imagecreatetruecolor($new_width, $new_height);
            imagecopyresized($big, $tmp_image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
            imagepng($big, "$box_dir/original.png");

            $sizes = array('big'=>array('w'=>269, 'h'=>269),
                           'normal'=>array('w'=>220, 'h'=>159),
                           'tiny'=>array('w'=>165, 'h'=>119));

            foreach($sizes as $name=>$size) {
                // size big
                $new_width = $size['w'];
                $new_height = $size['h'];
                $img = imagecreatetruecolor($new_width, $new_height);
                if ($height >= $width) {
                    $h = ($height*$new_width)/$width;
                    $w = $new_width;
                } else {
                    $h = $new_height;
                    $w = ($width*$new_height)/$height;
                }
                $central_aux = array('x'=>$w/2, 'y'=>$h/2);
                $y = $central_aux['y']-$new_height/2;
                $x = $central_aux['x']-$new_width/2;
                imagecopyresampled ($img, $tmp_image, 0, 0, $x, $y, $w, $h, $width, $height);
                imagepng($img, "$box_dir/$name.png");
            }
            $return = true;
            unlink($file['full_path']);
        }
        
        return $return;
    }
    
}

/* End of file Box.php */