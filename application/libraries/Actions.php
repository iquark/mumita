<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Actions
{
    // actions
    const BOX_CREATED = 1;
    const BOX_UPDATED = 2;
    const BOX_DELETED = 3;
    const BOX_COMMENTED = 4;
    const BOX_BOUGHT = 5;
    const BOX_SENT = 6;
    const BOX_RECEIVED = 7;
    const USER_FOLLOW = 11;
    const USER_UNFOLLOW = 12;
    const USER_COMMENTED = 13;
}

/* End of file Actions.php */