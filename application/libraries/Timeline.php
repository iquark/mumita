<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(dirname(__FILE__) . '/entities/Timeline_entity.php');

class Timeline
{
    // actions
    const BOX_CREATED = 1;
    const BOX_UPDATED = 2;
    const BOX_DELETED = 3;
    const BOX_COMMENTED = 4;
    const BOX_BOUGHT = 5;  // id_box:id_user1=>id_seller:id_user2=>id_buyer
    const BOX_SENT = 6;
    const BOX_RECEIVED = 7;
    const USER_FOLLOW = 11;
    const USER_UNFOLLOW = 12;
    const USER_COMMENTED = 13;
    
    public function get_all()
    {
        $CI=& get_instance();
        $CI->load->model('Timeline_model');
        
        $timelines = $CI->Timeline_model->get_all();
        
        return $timelines;
    }
    
    public function find_by($field, $value)
    {
        $CI=& get_instance();
        $CI->load->model('Timeline_model');
        
        $timelines = $CI->Timeline_model->find_by($field, $value);

        return $timelines;
    }

    public function create($timeline)
    {
    	$CI =& get_instance();
        
        $CI->load->model('Timeline_model');
        $timeline_id = $CI->Timeline_model->insert($timeline->getToInsert());
        
    	return $timeline_id;
    }
    
    public function box_action($action, $box_id, $value=null, $comment_id=null) 
    {
    	$CI =& get_instance();
        
        $user_id = $CI->session->userdata('id');
        $timeline = new Timeline_entity(null, $action, $user_id, null, 
                                        $box_id, $value, null, $comment_id);
        
        $this->create($timeline);
        
    }
    
    public function user_action($action, $user1_id, $user2_id=null, $value=null, $comment_id=null)
    {
    	$CI =& get_instance();
        
        $timeline = new Timeline_entity(null, $action, $user1_id, $user2_id, 
                                        null, $value, null, $comment_id);
        
        $this->create($timeline);
        
    }
    
    public function delete($comment_id = null)
    {
        $CI =& get_instance();
    	$CI->load->model('Timeline_model');
    	$deleted = false;
        
        $timelines = $CI->Timeline_model->find_by('id', $id);
        if (!empty($timelines))
        {
            $deleted = $CI->Timeline_model->delete($timelines[0]);            
        }
        
        return $deleted;
    }
    
    public function update($timeline=null)
    {
        $CI=& get_instance();
        $updated = false;
        
        if (!empty($timeline))
        {
            $CI->load->model('Timeline_model');
            $updated = $CI->Timeline_model->update($timeline);
        }
        
        return $updated;
    }
    
    public function get_your_info()
    {
        $CI = &get_instance();
        $user_id = $CI->session->userdata('id');
        $actions = array();
        
        if(!empty($user_id))
        {
            $CI->load->model('Timeline_model');
            $actions = $CI->timeline_model->get_your_info($user_id);
        }
        
        return $actions;
    }
    
    public function get_friends_info()
    {
        $CI = &get_instance();
        $user_id = $CI->session->userdata('id');
        $actions = array();
        
        if(!empty($user_id))
        {
            $CI->load->model('Timeline_model');
            $actions = $CI->timeline_model->get_friends_info($user_id);
        }
        
        return $actions;
    }
    
    public function get_suggested_boxes()
    {
        
    }

    public function get_suggested_people()
    {
        
    }
}

/* End of file Timeline.php */