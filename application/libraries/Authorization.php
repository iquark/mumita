<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Authorization 
{

    public function check_login($username, $password)
    {
    	$CI =& get_instance();
    	
    	$CI->load->model('User_model');
    	$return = $CI->User_model->check_user($username, $password);
    	
    	return $return;
    }
    
    public function create_session($user)
    {
    	$CI =& get_instance();
    	
    	$newdata = array(
                'id'  => $user->getId(),
                'username' => $user->getEmail(),
                'name' => $user->getName(),
                'lastname' => $user->getLastname(),
                'role_level' => $user->getRoleLevel(),
                'user_data' => serialize($user),
                'logged_in' => TRUE
    	);
    	
    	$CI->session->set_userdata($newdata);
    	
    }
    
    function activate($email, $code)
    {
    	$CI =& get_instance();
    	$CI->load->model('User_model');
        
    	$return = $CI->User_model->activate($email, $code);
        
        return $return;
    }
    
}

/* End of file Authorization.php */