<?php
/***
 * Base clase for the operations with MRW
 */
class MRWOperation
{
    protected $wsdl;
    protected $client;
    protected $parameters;
    protected $action;
    protected $last_response;
    
    public function __construct() 
    {
        $this->wsdl = "http://sagec-test.mrw.es/MRWEnvio.asmx?wsdl";
        echo "wsdl: ".$this->wsdl;
        ini_set($this->wsdl, "0"); // disabling WSDL cache
        $this->parameters = null;
    }
    
    protected function createClient()
    {
        $this->client = new SoapClient($this->wsdl,array('encoding'=>'utf-8',
                                            'soap_version'=> SOAP_1_2, 
                                            'SOAPAction'=> "http://www.mrw.es/".$this->action));
    }
    
    protected function createHeader()
    {
        $headerbody = array('CodigoFranquicia' => "04318",
                            'CodigoAbonado' => "005521",
                            'UserName'=>"04318SAGECFUT",
                            'Password'=>"04318SAGECFUT",
                            'CodigoDepartamento' => "");

        $namespace = 'http://www.mrw.es/'; 
        
        $header = new SoapHeader($namespace, 'AuthInfo', $headerbody);  
        $this->client->__setSoapHeaders($header); 
    }
    
    protected function createParameters()
    {
        
    }
    
    protected function runOperation()
    {
        $return = false;
        
        if(!empty($this->parameters))
        {
            $this->createClient();
            $this->createHeader();
            $return = true;
        }
        
        return $return;
    }
    
    protected function showClient()
    {
        var_dump($this->client);
    }
    
    protected function showLastResponse()
    {
        var_dump($this->last_response);
    }
}
?>
