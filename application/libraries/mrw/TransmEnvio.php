<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(dirname(__FILE__) . '/MRWOperation.php');
require_once(dirname(__FILE__) . '/../entities/Order_entity.php');

class TransmEnvio extends MRWOperation
{

    public function __construct() 
    {
        parent::__construct();
        $this->action = "TransmEnvio";
    }
    
    protected function createParameters() {
        $this->parameters = array(
            'request' => array (
                'DatosEntrega' => array (
                    'Direccion' => array (
                        'CodigoTipoVia' => "CL",
                        'Via' => "PRUEBA",
                        'Numero' => "1",
                        'Resto' => "PRUEBA",
                        'CodigoPostal' =>"28004",
                        'Poblacion' =>"MADRID"
                    ),
                    'Nif' => "12345678Z",
                    'Nombre' => "PRUEBA INTEGRACION",
                    'Telefono' => "947001001",
                    'Contacto' => "Prueba",
                    'ALaAtencionDe' => "SERGIO PRUEBA INTEGRACION",
                    'Observaciones' => "PRUEBA DPTO FIELD SUPPORT"
                ),
                'DatosServicio' => array (
                    'Fecha' =>"28/09/2013",
                    'Referencia' => "PRUEBA INTEGRACION",
                    'CodigoServicio' =>"0800",
                    'NumeroSobre' => 1,
                    'Bultos' => array(
                        'Bulto' => array(
                            'Alto' =>10,
                            'Largo' =>20,
                            'Ancho' =>30,
                            'Dimension' =>40,
                            'Referencia' =>"AAA",
                            'Peso' =>500
                        ),
                    ),
                    'NumeroBultos' =>1,
                    'Peso' =>500,
                    'EntregaPartirDe' =>"",
                    'Gestion' =>"",
                    'Retorno'=>"",
                    'ConfirmacionInmediata' =>"",
                    'EntregaSabado' =>"",
                    'Entrega830' =>"",
                    'Reembolso' =>"O",
                    'ImporteReembolso' =>"13,40",
                    'TramoHorario' =>2
                ),
            )
        );
    }

    public function runOperation($id_box)
    {
    	$CI =& get_instance();
        $return = false;
        $this->createParameters();
        
        if(parent::runOperation()) 
        {
            try
            {
                $this->last_response = $this->client->TransmEnvio($this->parameters);
                $return = $this->last_response;
                $this->showLastResponse();
                
                // get the data to store
                $status = $this->last_response->TransmEnvioResult->Estado;
                $message =  $this->last_response->TransmEnvioResult->Mensaje;
                $request_number =  $this->last_response->TransmEnvioResult->NumeroSolicitud;
                $delivery_number =  $this->last_response->TransmEnvioResult->NumeroEnvio;
                $URL =  $this->last_response->TransmEnvioResult->Url;
                $id_user = $CI->session->userdata('id');
                
                // create the order
                $CI->load->library("order");
                $order = new Order_entity(null, $id_box, $id_user, $date=time(), $request_number, 
                                          $delivery_number, $URL, $message, $status);
                $CI->order->create($order);
            }
            catch(SoapFault $fault)
            {
                log_message('error', 'TransmEnvio: '.$fault->getMessage().' \nRequest: '.$client->__getLastRequest());
            } 
        }
        
        return $return;
    }
}

/* End of file TransmEnvio.php */