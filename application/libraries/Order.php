<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order
{
    public function create ($order) 
    {
    	$CI =& get_instance();

        $CI->load->model('Order_model');
        $order_id = $CI->Order_model->create($order->getToInsert());
        //$CI->timeline->order_action(Timeline::ORDER_CREATED, $order_id);
    	
    	return $order_id;
    }
}

/* End of file Order.php */