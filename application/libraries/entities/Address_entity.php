<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Address_entity
{
    private $id;
    private $street;
    private $city;
    private $province;
    private $country;
    private $postal_code;

    public function __construct($id=null, $street=null, $city=null, $province=null, $country=null, $postal_code=null) 
    {
        $this->id = $id;
        $this->street = $street;
        $this->city = $city;
        $this->province = $province;
        $this->country = $country;
        $this->postal_code = $postal_code;
    }

    public function getId() 
    {
        return $this->id;
    }

    public function setId($id) 
    {
        $this->id = $id;
    }

    public function getStreet() 
    {
        return $this->street;
    }

    public function setStreet($street) 
    {
        $this->street = $street;
    }

    public function getCity() 
    {
        return $this->city;
    }

    public function setCity($city) 
    {
        $this->city = $city;
    }

    public function getProvince() 
    {
        return $this->province;
    }

    public function setProvince($province) 
    {
        $this->province = $province;
    }

    public function getCountry() 
    {
        return $this->country;
    }

    public function setCountry($country) 
    {
        $this->country = $country;
    }

    public function getPostalCode() 
    {
        return $this->postal_code;
    }

    public function setPostalCode($postal_code) 
    {
        $this->postal_code = $postal_code;
    }
    
    public function getBrief()
    {
        return $this->province;
    }

    /**
     * It returns an address array without de ID field
     * 
     */
    public function getToInsert()
    {
        return array("street"=>$this->street,
                    "city" => $this->city,
                    "province" => $this->province,
                    "country" => $this->country,
                    "postal_code" => $this->postal_code);	
    }

    /**
     * It returns an address array without de ID field
     * 
     */
    public function getToUpdate()
    {
        return array("street"=>$this->street,
                    "city" => $this->city,
                    "province" => $this->province,
                    "country" => $this->country,
                    "postal_code" => $this->postal_code);	
    }
}

/* End of file Address_entity.php */