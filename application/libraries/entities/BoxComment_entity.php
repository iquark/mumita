<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BoxComment_entity
{
    private $id;
    private $id_box;
    private $id_user;
    private $user;
    private $comment;
    private $created_at;

    public function __construct($id=null, $id_box="", $id_user="", $comment="", $created_at=null) 
    {
        $this->id = $id;
        $this->id_box = $id_box;
        $this->comment = $comment;
        $this->created_at = $created_at;
        
        $this->setUser($id_user);
    }

    public function getId() 
    {
        return $this->id;
    }

    public function setId($id) 
    {
        $this->id = $id;
    }

    public function getIdBox() 
    {
        return $this->id_box;
    }

    public function setIdBox($id_box) 
    {
        $this->id_box = $id_box;
    }

    public function getUser()
    {
        return $this->user;
    }
     
    public function setUser($user_id)
    {
    	$CI =& get_instance();
        $CI->load->library('user');
        // TODO be carefull because you are also looking for admins
        $users = $CI->user->find_by('id', $user_id, TRUE);
        
        if(!empty($users))
        {
            $this->user = $users[0];
        }
        else
        {
            $this->user = null;
        }
    }
    
    public function getComment() 
    {
        return $this->comment;
    }
    
    public function setComment($comment) 
    {
        $this->comment = $comment;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }
    
    /**
        * It returns an customer array without de ID field
        * 
        */
    public function getToInsert()
    {
        $array = array("id_box"=>$this->id_box,
                       "id_user" => $this->id_user,
                       "comment" => $this->comment);
        
        return $array;	
    }

    /**
    * It returns an customer array without de ID field
    * 
    */
    public function getToUpdate()
    {
        $array = array("id_box"=>$this->id_box,
                       "id_user" => $this->id_user,
                       "comment" => $this->comment);
        
        return $array;
    }
    
}

/* End of file BoxComment_entity.php */