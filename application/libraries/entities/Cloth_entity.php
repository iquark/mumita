<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('Box_entity.php');

class Cloth_entity extends Box_entity
{
    private $id_size;
    private $colours;
    private $season;

    public function __construct($id=null, $abstract=null, $category=null, $description=null,
                                $id_age=null, $sex=null, $user_id=null, $created_at=null, 
                                $status = Box::STATUS_DRAFT, $valoration = 1, $id_size=null, 
                                $colour1=null, $colour2=null, $colour3=null, $season=null) 
    {
        parent::__construct($id, $abstract, $category, $description, $id_age, $sex, $user_id, $created_at, $status, $valoration);
        
        $this->id_size = $id_size;
        $this->colours = array();
        array_push($this->colours, $colour1);
        array_push($this->colours, $colour2);
        array_push($this->colours, $colour3);
        $this->season = $season;
    }

    public function getIdSize() 
    {
        return $this->id_size;
    }

    public function setIdSize($id_size) 
    {
        $this->id_size = $id_size;
    }

    public function getColours() 
    {
        return $this->colours;
    }

    public function addColour($colour) 
    {
        array_push($this->colours, $colour);
    }

    public function getSeason()
    {
        return $this->season;
    }

    public function setSeason($season) 
    {
        $this->season = $season;
    }

    /**
        * It returns an user array without de ID field
        * 
        */
    public function getToInsert()
    {
        $array = array();
        $array['box'] = parent::getToInsert();
        $array['id_size'] = $this->id_size;
        
        for($idx=0; $idx<count($this->colours); $idx++) {
            $array["colour".($idx+1)] = $this->colours[$idx];    
        }
        
        $array['season'] = $this->season;
        
        return $array;	
    }

    /**
    * It returns a box array without de ID field
    * 
    */
    public function getToUpdate()
    {
        $array = array();
        
        $array['box'] = parent::getToUpdate();
        $array['id_size'] = $this->id_size;
        
        for($idx=0; $idx<count($this->colours); $idx++) {
            $array["colour".($idx+1)] = $this->colours[$idx];    
        }
        
        $array['season'] = $this->season;
        
        return $array;
    }
  
}

/* End of file Cloth_entity.php */