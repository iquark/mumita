<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer_entity
{
    private $id;
    private $name;
    private $email;
    private $ip;
    private $created_at;

    public function __construct($id=null, $name="", $email="", $ip="", $created_at=null) 
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->ip = $ip;
        $this->created_at = $created_at;
    }

    public function getId() 
    {
        return $this->id;
    }

    public function setId($id) 
    {
        $this->id = $id;
    }

    public function getName() 
    {
        return $this->name;
    }

    public function setName($name) 
    {
        $this->name = $name;
    }
    
    public function getEmail() 
    {
        return $this->email;
    }

    public function setIp($ip) 
    {
        $this->ip = $ip;
    }

    public function getIp() 
    {
        return $this->ip;
    }
    
    public function getCreatedAt()
    {
        return $this->created_at;
    }
    
    /**
        * It returns an customer array without de ID field
        * 
        */
    public function getToInsert()
    {
        $array = array("name"=>$this->name,
                       "email" => $this->email,
                       "ip" => $this->ip);
        
        return $array;	
    }

    /**
    * It returns an customer array without de ID field
    * 
    */
    public function getToUpdate()
    {
        $array = array("name"=>$this->name,
                       "email" => $this->email,
                       "ip" => $this->ip);
        
        return $array;
    }
    
}

/* End of file Customer_entity.php */