<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Box_entity
{
    private $id;
    private $abstract;
    private $category;
    private $description;
    private $id_age;
    private $sex;
    private $created_at;
    private $user;
    private $status;
    private $comments;
    private $valoration;

    public function __construct($id=null, $abstract=null, $category=null, $description=null,
                                $id_age=null, $sex=null, $user_id=null, $created_at=null, 
                                $status = Box::STATUS_DRAFT, $valoration = 1) 
    {
        $this->id = $id;
        $this->abstract = $abstract;
        $this->category = $category;
        $this->description = $description;
        $this->id_age = $id_age;
        $this->sex = $sex;
        $this->created_at = $created_at;
        $this->status = $status;
        $this->valoration = $valoration;
        
        $this->setUser($user_id);
    }

    public function getId() 
    {
        return $this->id;
    }

    public function setId($id) 
    {
        $this->id = $id;
    }

    public function getAbstract() 
    {
        return $this->abstract;
    }

    public function setAbstract($abstract) 
    {
        $this->abstract = $abstract;
    }

    public function getCategory() 
    {
        return $this->category;
    }

    public function setCategory($category) 
    {
        $this->category = $category;
    }
    
    public function getDescription() 
    {
        return $this->description;
    }

    public function setDescription($description) 
    {
        $this->description = $description;
    }

    public function getAge() 
    {
        return $this->id_age;
    }

    public function setAge($id_age) 
    {
        $this->id_age = $id_age;
    }

    public function getSex() 
    {
        return $this->sex;
    }

    public function setSex($sex) 
    {
        $this->sex = $sex;
    }

    public function getCreatedAt() 
    {
        return $this->created_at;
    }

    public function setCreatedAt($created_at) 
    {
        $this->created_at = $created_at;
    }
    
    /**
        * It returns an user array without de ID field
        * 
        */
    public function getToInsert()
    {
        $array = array(
                    "abstract"=>$this->abstract,
                    "description" => $this->description,
                    "id_age" => $this->id_age,
                    "category" => $this->category,
                    "sex" => $this->sex,
                    "id_user" => (!empty($this->user))?$this->user->getId():null);
        
        return $array;	
    }
    
    /**
    * It returns an user array without de ID field
    * 
    */
    public function getToUpdate()
    {
        $array = array(
                    "abstract"=>$this->abstract,
                    "description" => $this->description,
                    "id_age" => $this->id_age,
                    "category" => $this->category,
                    "sex" => $this->sex);
        
        return $array;
    }
    
    public function getUser()
    {
        return $this->user;
    }
     
    public function setUser($user_id)
    {
    	$CI =& get_instance();
        $CI->load->library('user');
        // TODO be carefull because you are also looking for admins
        $users = $CI->user->find_by('id', $user_id, TRUE);
        
        if(!empty($users))
        {
            $this->user = $users[0];
        }
        else
        {
            $this->user = null;
        }
    }
    
    public function getValoration()
    {
        return $this->valoration;
    }
    
    public function addValoration($user, $valoration)
    {
        $CI =& get_instance();
        $CI->load->library("box");
        
        $this->valoration = $CI->box->add_valoration($this->id, $user->getId(), $valoration);
    }
    
    public function getParentCopy()
    {
        return new Box_entity($this->id, $this->abstract, $this->category, $this->description,
                                $this->id_age, $this->sex, $this->user->getId(), $this->created_at);
    }
   
    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
    	$this->status = $status;
    }
    
    public function getComments()
    {
        $CI =& get_instance();
        $CI->load->library("boxComment");
        
        $this->comments = $CI->boxcomment->find_by("id_box", $this->id);
        
        return $this->comments;
    }
}

/* End of file Box_entity.php */