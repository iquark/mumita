<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Timeline_entity
{
    private $id;
    private $id_action;
    private $id_user1;
    private $user1;
    private $id_user2;
    private $user2;
    private $id_box;
    private $box;
    private $date;
    private $value;
    private $id_comment;

    public function __construct($id=null, $id_action=null, $id_user1=null, $id_user2=null, 
                                $id_box=null, $value=null, $date=null, $id_comment=null) 
    {
        $this->id = $id;
        $this->id_action = $id_action;
        $this->id_user1 = $id_user1;
        $this->id_user2 = $id_user2;
        $this->id_box = $id_box;
        $this->date = $date;
        $this->value = $value;
        $this->box = null;
        $this->user1 = null;
        $this->user2 = null;
        $this->id_comment = $id_comment;
    }

    public function getId() 
    {
        return $this->id;
    }

    public function setId($id) 
    {
        $this->id = $id;
    }

    public function getIdAction() 
    {
        return $this->id_action;
    }

    public function setIdAction($id_action) 
    {
        $this->id_action = $id_action;
    }

    public function getIdUser1() 
    {
        return $this->id_user1;
    }

    public function setIdUser1($id_user1) 
    {
        $this->id_user1 = $id_user1;
    }

    public function getUser1() 
    {
        if (empty($this->user1) && !empty($this->id_user1))
        {
            $users = $this->user->find_by('id', $this->id_user1);
            if (!empty($users))
            {
                $this->user1 = $users[0];
            }
        }
        
        return $this->user1;
    }

    public function setUser1($user) 
    {
        $this->user1 = $user;
    }
    
    public function getIdUser2() 
    {
        return $this->id_user2;
    }

    public function setIdUser2($id_user2) 
    {
        $this->id_user2 = $id_user2;
    }

    public function getUser2() 
    {
        $CI =& get_instance();
    	
        if (empty($this->user2) && !empty($this->id_user2))
        {
            $users = $CI->user->find_by('id', $this->id_user2);
            if (!empty($users))
            {
                $this->user2 = $users[0];
            }
        }
        
        return $this->user2;
    }

    public function setUser2($user) 
    {
        $this->user2 = $user;
    }
    
    public function getIdBox() 
    {
        return $this->id_box;
    }

    public function setIdBox($id_box) 
    {
        $this->id_box = $id_box;
    }

    public function getBox() 
    {
        $CI =& get_instance();
    	
        if (empty($this->box) && !empty($this->id_box))
        {
            $boxes = $CI->box->find_by('id', $this->id_box);
            if (!empty($boxes))
            {
                $this->box = $boxes[0];
            }
        }
        return $this->box;
    }

    public function setBox($box) 
    {
        $this->box = $box;
    }
    
    public function getDate() 
    {
        return $this->date;
    }

    public function setDate($date) 
    {
        $this->date = $date;
    }

    public function getValue() 
    {
        return $this->value;
    }

    public function setValue($value) 
    {
        $this->value = $value;
    }

    public function getIdComment() 
    {
        return $this->id_comment;
    }

    public function setIdComment($id_comment) 
    {
        $this->id_comment = $id_comment;
    }
    /**
     * It returns an address array without de ID field
     * 
     */
    public function getToInsert()
    {
        return array("id_action"=>$this->id_action,
                    "id_user1" => $this->id_user1,
                    "id_user2" => $this->id_user2,
                    "id_box" => $this->id_box,
                    "value" => $this->value,
                    "id_comment" => $this->id_comment);	
    }

    /**
     * It returns an address array without de ID field
     * 
     */
    public function getToUpdate()
    {
        return array("id_action"=>$this->id_action,
                    "id_user1" => $this->id_user1,
                    "id_user2" => $this->id_user2,
                    "id_box" => $this->id_box,
                    "date" => $this->date,
                    "value" => $this->value,
                    "id_comment" => $this->id_comment);	
    }
}

/* End of file Timeline_entity.php */