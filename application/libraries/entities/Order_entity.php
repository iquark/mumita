<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_entity
{
    private $id;
    private $id_box;
    private $id_user;
    private $date;
    private $request_number;
    private $delivery_number;
    private $URL;
    private $message;
    private $status;
    
    public function __construct($id=null, $id_box=null, $id_user=null, $date=null, 
                                $request_number=null, $delivery_number=null, $URL=null,
                                $message=null, $status=null) 
    {
        $this->id = $id;
        $this->id_box = $id_box;
        $this->id_user = $id_user;
        $this->date = $date;
        $this->request_number = $request_number;
        $this->delivery_number = $delivery_number;
        $this->URL = $URL;
        $this->message = $message;
        $this->status = $status;
    }

    public function getId() 
    {
        return $this->id;
    }

    public function setId($id) 
    {
        $this->id = $id;
    }

    public function getIdBox() 
    {
        return $this->id_box;
    }

    public function setIdBox($id_box) 
    {
        $this->id_box = $id_box;
    }

    public function getIdUser() 
    {
        return $this->id_user;
    }

    public function setIdUser($id_user) 
    {
        $this->id_user = $id_user;
    }

    public function getDate() 
    {
        return $this->date;
    }

    public function setDate($date) 
    {
        $this->date = $date;
    }

    public function getRequestNumber() 
    {
        return $this->request_number;
    }

    public function setRequestNumber($request_number) 
    {
        $this->request_number = $request_number;
    }

    public function getDeliveryNumber() 
    {
        return $this->delivery_number;
    }

    public function setDeliveryNumber($delivery_number) 
    {
        $this->delivery_number = $delivery_number;
    }
    
    public function getURL() 
    {
        return $this->URL;
    }

    public function setURL($URL) 
    {
        $this->URL = $URL;
    }
    
    public function getMessage() 
    {
        return $this->message;
    }

    public function setMessage($message) 
    {
        $this->message = $message;
    }
    
    public function getStatus() 
    {
        return $this->status;
    }

    public function setStatus($status) 
    {
        $this->status = $status;
    }
    
    /**
     * It returns an order array without de ID field
     * 
     */
    public function getToInsert()
    {
        return array(
                "id_box" => $this->id_box,
                "id_user" => $this->id_user,
                "request_number" => $this->request_number,
                "delivery_number" => $this->delivery_number,
                "URL" => $this->URL,
                "message" => $this->message,
                "status" => $this->status
        );	
    }
    /**
     * It returns an address array without de ID field
     * 
     */
    public function getToUpdate()
    {
        return array(
                "id_box" => $this->id_box,
                "id_user" => $this->id_user,
                "request_number" => $this->request_number,
                "delivery_number" => $this->delivery_number,
                "date" => $this->date,
                "URL" => $this->URL,
                "message" => $this->message,
                "status" => $this->status
        );	
    }
}

/* End of file Order_entity.php */