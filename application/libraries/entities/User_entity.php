<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('Box_entity.php');

class User_entity
{
    private $id;
    private $name;
    private $lastname;
    private $email;
    private $password;
    private $sex;
    private $role_level;
    private $bio;
    private $salt;
    private $shipping_address_id;
    private $pickup_address_id;
    private $billing_address_id;
    private $created_at;
    private $active;
    private $activation_code;
    private $birthday;
    private $following;
    private $boxes;
    private $orders;
    
    public function __construct($id=null, $name="", $lastname="", $email="", $password="", $sex=0, $role_level=0, $bio=null, $salt=null, 
                                $active=0, $birthday=null, $shipping_address_id=null, $pickup_address_id=null, $billing_address_id=null, 
                                $created_at=null, $activation_code = null, $following = null) 
    {
        $this->id = $id;
        $this->name = $name;
        $this->lastname = $lastname;
        $this->email = $email;
        $this->password = $password;
        $this->sex = $sex;
        $this->role_level = $role_level;
        $this->bio = $bio;
        $this->salt = $salt;
        $this->shipping_address_id = $shipping_address_id;
        $this->pickup_address_id = $pickup_address_id;
        $this->billing_address_id = $billing_address_id;
        $this->created_at = $created_at;
        $this->active = $active;
        $this->activation_code = $activation_code;
        $this->birthday = $birthday;
        $this->following = $following;
    }

    public function getId() 
    {
        return $this->id;
    }

    public function setId($id) 
    {
        $this->id = $id;
    }

    public function getName() 
    {
        return $this->name;
    }

    public function setName($name) 
    {
        $this->name = $name;
    }

    public function getLastname() 
    {
        return $this->lastname;
    }

    public function setLastname($lastname) 
    {
        $this->lastname = $lastname;
    }

    public function getFullName()
    {
        return $this->name." ".$this->lastname;
    }

    public function getEmail() 
    {
        return $this->email;
    }

    public function setEmail($email) 
    {
        $this->email = $email;
    }

    public function getPassword() 
    {
        return $this->password;
    }

    public function setPassword($password) 
    {
        $this->password = $password;
    }

    public function getSex() 
    {
        return $this->sex;
    }

    public function setSex($sex) 
    {
        $this->sex = $sex;
    }

    public function getRoleLevel() 
    {
        return $this->role_level;
    }

    public function setRoleLevel($role_level) 
    {
        $this->role_level = $role_level;
    }

    public function getBio() 
    {
        return $this->bio;
    }

    public function setBio($bio) 
    {
        $this->bio = $bio;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    public function getShippingAddressId()
    {
        return $this->shipping_address_id;
    }

    public function setShippingAddressId($shipping_address_id)
    {
        $this->shipping_address_id = $shipping_address_id;
    }

    public function getShippingAddress()
    {
        return $this->getAddress($this->shipping_address_id);
    }

    public function setShippingAddress($address)
    {
        $CI =& get_instance();
    	
        $CI->load->library("user");
        
        $CI->user->set_shipping_address($this, $address);
        
        if (empty($this->shipping_address_id))
        {
            $this->address->delete($this->shipping_address_id);
            $this->shipping_address_id = $address->getId();
        }
        else
        {
            if (!empty($address) && $this->shipping_address_id!=$address->getId())
            {
                $this->shipping_address_id = $address->getId();
            }
        }
    }

    public function getPickupAddressId()
    {
        return $this->pickup_address_id;
    }

    public function setPickupAddressId($pickup_address_id)
    {
        $this->pickup_address_id = $pickup_address_id;
    }

    public function getPickupAddress()
    {
        return $this->getAddress($this->pickup_address_id);
    }

    public function setPickupAddress()
    {
        // TODO set Pick up Address
    }

    public function getBillingAddressId()
    {
        return $this->billing_address_id;
    }

    public function setBillingAddressId($billing_address_id)
    {
        $this->billing_address_id = $billing_address_id;
    }

    public function getBillingAddress()
    {
        return $this->getAddress($this->billing_address_id);
    }

    public function setBillingAddress()
    {
        // TODO set Billing Address
    }
    
    public function getCreatedAt()
    {
        return $this->created_at;
    }
    
    public function isActive()
    {
        return ($this->active==1);
    }
    
    public function getActive()
    {
        return $this->active;
    }
    
    public function setActive($active)
    {
        $this->active = $active;
    }
    
    public function getActivationCode()
    {
        return $this->activation_code;
    }
    
    public function setActivationCode($activation_code)
    {
        $this->activation_code = $activation_code;
    }
    
    public function getBirthday()
    {
        return $this->birthday;
    }
    
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }
    
    public function getFollowing()
    {
        return $this->following;
    }
    
    public function setFollowing($following)
    {
        $this->following = $following;
    }
    
    /**
        * It returns an user array without de ID field
        * 
        */
    public function getToInsert()
    {
        $array = array("name"=>$this->name,
                    "lastname" => $this->lastname,
                    "email" => $this->email,
                    "password" => $this->password,
                    "sex" => $this->sex,
                    "role_level" => $this->role_level,
                    "bio" => $this->bio,
                    "salt" => $this->salt,
                    "birthday" => $this->birthday,
                    "active" => $this->active,
                    "activation_code" => $this->activation_code);
        
        if (!empty($this->shipping_address_id))
        {
            $array['shipping_address_id'] = $this->shipping_address_id;
        }
        if (!empty($this->pickup_address_id))
        {
            $array['pickup_address_id'] = $this->pickup_address_id;
        }
        if (!empty($this->billing_address_id))
        {
            $array['billing_address_id'] = $this->billing_address_id;
        }
        
        return $array;	
    }

    /**
    * It returns an user array without de ID field
    * 
    */
    public function getToUpdate()
    {
        $array = array("name"=>$this->name,
                    "lastname" => $this->lastname,
                    "email" => $this->email,
                    "sex" => $this->sex,
                    "birthday" => $this->birthday,
                    "role_level" => $this->role_level,
                    "bio" => $this->bio,
                    "active" => $this->active);
        if (!empty($this->password))
        {
            $array['password'] = $this->password;
        }
        if (!empty($this->shipping_address_id))
        {
            $array['shipping_address_id'] = $this->shipping_address_id;
        }
        if (!empty($this->pickup_address_id))
        {
            $array['pickup_address_id'] = $this->pickup_address_id;
        }
        if (!empty($this->billing_address_id))
        {
            $array['billing_address_id'] = $this->billing_address_id;
        }
        
        return $array;
    }
    
    private function getAddress($address_id)
    {
        $CI =& get_instance();
    	
        $address = null;
        $CI->load->model("address_model");

        $addresses = $CI->address_model->findBy("id", $address_id);

        if(!empty($addresses))
        {
            $address = $addresses[0];
        }
        else
        {
            $address = new Address_entity();
        }
        
        return $address;
    }
    
    public function followedBy($user_id)
    {
        $CI =& get_instance();
    	
        $CI->load->library("user");
        $following = $CI->user->check_following($user_id, $this->getId());
        
        return $following;
    }
    
    public  function following($user_id)
    {
        $CI =& get_instance();
    	
        $CI->load->library("user");
        $following = $CI->user->check_following($this->getId(), $user_id);
        
        return $following;
    }
    
    public function isAdmin()
    {
        return $this->role_level == 0;
    }
    
    public function setBoxes($boxes) {
        $this->boxes = $boxes;
    }
    
    public function getBoxes($status=null) {
        if(empty($this->boxes)) {
            $CI =& get_instance();
    	
            $CI->load->library("box");
            $this->boxes = $CI->box->find_by("id_user", $this->getId());
        }
        
        $boxes = array();
        
        if (!empty($status))
        {
            foreach($this->boxes as $box) {
                if ($box->getStatus() == $status) {
                    array_push($boxes, $box);
                }    
            }
        }
        
        return $this->boxes;
    }
    
    /***
     *  @todo filter boxes by send
     */
    public function getSentBoxes() {
        return $this->boxes;
    }
    
    /***
     *  @todo orders
     */
    public function getOrders() {
        return $this->orders;
    }
    
    public function setOrders($orders) {
        $this->orders = $orders;
    }
    
    /***
     *  @todo filter order by delivered
     */
    public function getDeliveredOrders() {
        return $this->orders;
    }
    
    public function getReceivedOrders() {
        return $this->orders;
    }
    
    /***
     * @todo Balance
     */
    public function getBalance() {
        return "+1";
    }
    
    /***
     * @todo Valoration
     */
    public function getValoration() {
        return 3;
    }
    
    /***
     * @todo short address
     */
    public function getShortAddress() 
    {
        $address = $this->getShippingAddress()->getBrief();
        if (empty($address))
        {
            $address = "<i>(Direcci&oacute;n no p&uacute;blica)</i>";
        }
        
        return $address;
    }
}

/* End of file User_entity.php */