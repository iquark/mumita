<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(dirname(__FILE__) . '/entities/User_entity.php');

class User
{
    public function get_all($also_admins = FALSE, $num=null, $offset=null)
    {
        $CI=& get_instance();
        $CI->load->model('User_model');
        
        $users = $CI->User_model->get_all($also_admins, $num, $offset);
        
        return $users;
    }
    
    public function find_by($field, $value, $also_admins = FALSE)
    {
        $CI=& get_instance();
        $CI->load->model('User_model');
        
        $users = $CI->User_model->find_by($field, $value, $also_admins);
        
        return $users;
    }
    
    public function register($name, $lastname, $email, $password, $sex, $bio, $birthday=null, $active=0, $role_level=0)
    {
    	$CI =& get_instance();
        $CI->load->model('User_model');
    	
        $salt = sha1($name.$lastname.$password.rand(0, rand(1000, 1000000)));
        $user = new User_entity(null, $name, $lastname, $email, $password, $sex, $role_level, $bio, $salt, $active, $birthday);
        $id_user = $CI->User_model->register($user->getToInsert());
    	
    	return $id_user;
    }
    
    public function create_session($user)
    {
    	$CI =& get_instance();
    	
    	$newdata = array(
                'id'  => $user->id,
                'email'     => $user->username,
                'name' => $user->name,
                'lastname' => $user->lastname,
                'role_level' => $user->role_level,
                'logged_in' => TRUE
    	);
    	
    	$CI->session->set_userdata($newdata);
    	
    }
    
    public function search($search, $sex, $size, $season) 
    {
    	$CI =& get_instance();
    	$CI->load->model('User_model');
        
    	$users = $CI->User_model->search($search, $sex, $size, $season);
    	
    	// TODO filter by the boxes that users are selling
    	
    	return $users;
    }
    
    public function delete($id = null)
    {
        $CI =& get_instance();
    	$CI->load->model('User_model');
    	$deleted = false;
        
        $users = $CI->User_model->find_by('id', $id);
        if (!empty($users))
        {
            $deleted = $CI->User_model->delete($users[0]);
        }
        
        return $deleted;
    }
    
    public function update($user=null)
    {
        $CI=& get_instance();
        $CI->load->model('User_model');
        $updated = false;
        
        if (!empty($user))
        {
            $updated = $CI->User_model->update($user);
        }
        
        return $updated;
    }
    
    public function follow($user_follower_id, $user_followed_id)
    {
        $CI=& get_instance();
        $CI->load->model('User_model');
        
        $following = $CI->User_model->follow($user_follower_id, $user_followed_id);
        
        $CI->timeline->user_action(Timeline::USER_FOLLOW, $user_follower_id, $user_followed_id);
        
        return $following;
    }
        
    public function unfollow($user_follower_id, $user_followed_id)
    {
        $CI=& get_instance();
        $CI->load->model('User_model');
        
        $following = $CI->User_model->unfollow($user_follower_id, $user_followed_id);
        
        $CI->timeline->user_action(Timeline::USER_UNFOLLOW, $user_follower_id, $user_followed_id);
        
        return $following;
    }
    
    public function check_following($user_follower_id, $user_followed_id)
    {
        $CI=& get_instance();
        $CI->load->model('User_model');
        
        $following = $CI->User_model->check_following($user_follower_id, $user_followed_id);
        
        return $following;
    }
    
    public function create_avatars($file, $user_dir)
    {
        $return = false;
        $finfo = finfo_open(FILEINFO_MIME_TYPE); // devuelve el tipo mime de su extensión
        $mime = finfo_file($finfo, $file['full_path']);
        
        if(!is_dir($user_dir."/"))
        {
            mkdir($user_dir."/", 0777, true);
        }
        //move_uploaded_file($filename, "$user_dir/$filename");
        switch($mime)
        {
            case "image/jpeg":
                $tmp_image=imagecreatefromjpeg($file['full_path']);
                break;
            case "image/png":
                $tmp_image=imagecreatefrompng($file['full_path']);
                break;
            case "image/gif":
                $tmp_image=imagecreatefromgif($file['full_path']);
                break;
            default:
                $tmp_image = null;
        }
        
        if(!empty($tmp_image))
        {
            list($width, $height) = getimagesize($file['full_path']);
            $new_width = 120;
            $new_height = 120;
            $central_aux = array('x'=>$new_width/2, 'y'=>$new_height/2);    
            $normal = imagecreatetruecolor($new_width, $new_height);
            if ($height >= $width) {
                $h = ($height*$new_width)/$width;
                $w = $new_width;
            } else {
                $h = $new_height;
                $w = ($width*$new_height)/$height;
            }
            $central_aux = array('x'=>$w/2, 'y'=>$h/2);
            $y = $central_aux['y']-$new_height/2;
            $x = $central_aux['x']-$new_width/2;
            imagecopyresampled($normal, $tmp_image, 0, 0, $x, $y, $w, $h, $width, $height);
            imagepng($normal, "$user_dir/normal.png");
            $new_width = 60;
            $new_height = 60;
            $central_aux = array('x'=>$new_width/2, 'y'=>$new_height/2);  
            $tiny = imagecreatetruecolor($new_width, $new_height);
            if ($height >= $width) {
                $h = ($height*$new_width)/$width;
                $w = $new_width;
            } else {
                $h = $new_height;
                $w = ($width*$new_height)/$height;
            }
            $central_aux = array('x'=>$w/2, 'y'=>$h/2);
            $y = $central_aux['y']-$new_height/2;
            $x = $central_aux['x']-$new_width/2;
            imagecopyresampled($tiny, $tmp_image, 0, 0,  $x, $y, $w, $h, $width, $height);
            imagepng($tiny,"$user_dir/tiny.png");
            $return = true;
            unlink($file['full_path']);
        }
        
        return $return;
    }
    
    function send_email($user_id)
    {
        $CI=& get_instance();
        $CI->load->library('email');
    	$CI->load->model('User_model');
        
        $users = $CI->User_model->find_by('id', $user_id);
        if (!empty($users))
        {
            $user = $users[0];
            
            $CI->email->from("noresponder@mumita.com", 'Equipo de Mumita');
            $CI->email->to($user->getEmail()); 

            $CI->email->subject('Mumita: Activa tu cuenta');
            $CI->email->message('Hola '.$user->getFullName().'! Para activar tu cuenta debes entrar aquí: '.anchor('activar/'.urlencode($user->getEmail()).'/'.$user->getActivationCode()).'.');	

            $CI->email->send();
            echo $CI->email->print_debugger();
        }
    }
    
    public function reset_password($user)
    {
        $new_password = $this->generate_password();
        $user->setPassword(md5($new_password));
        $success = false;
        
        if($this->update($user))
        {
            $this->forgot_email($user, $new_password);
            $success = true;
        }
        
        return $success;
    }
    
    private function forgot_email($user, $new_password)
    {
        $CI=& get_instance();
        $CI->load->library('email');
        
        if (!empty($user))
        {
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = TRUE;
            $config['mailtype'] = 'html';
            $CI->email->initialize($config);
            $CI->email->from("noresponder@mumita.com", 'Equipo de Mumita');
            $CI->email->to($user->getEmail()); 

            $CI->email->subject('Mumita: Cambio de contraseña');
            $CI->email->message('Hola '.$user->getFullName().'! <p>Tu nueva contrase&ntilde;a ahora es: <strong>'.$new_password.'</strong>.</p> <p>Puedes ir a tu panel de usuario y cambiarla, recuerda que no debes dársela a nadie bajo ningún concepto.</p><p>Atentamente,<br />El equipo de Mumita.</p>');

            $CI->email->send();
        }
    }
    
    /**
     * Generate a random password. 
     * 
     * get_random_password() will return a random password with length 6-8 of lowercase letters only.
     *
     * @author  bjornbjorn (http://codeigniter.com/wiki/Random_Password_Generator)
     * @access   public
     * @param    $chars_min the minimum length of password (optional, default 6)
     * @param    $chars_max the maximum length of password (optional, default 8)
     * @param    $use_upper_case boolean use upper case for letters, means stronger password (optional, default false)
     * @param    $include_numbers boolean include numbers, means stronger password (optional, default false)
     * @param    $include_special_chars include special characters, means stronger password (optional, default false)
     *
     * @return    string containing a random password 
     */    
    private function generate_password()
    {
        $chars_min=6; 
        $chars_max=13; 
        $use_upper_case=false;
        
        $length = rand($chars_min, $chars_max);
        $selection = 'aeuoyibcdfghjklmnpqrstvwxz1234567890';
                                
        $password = "";
        for($i=0; $i<$length; $i++) {
            $current_letter = $use_upper_case ? (rand(0,1) ? strtoupper($selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))];            
            $password .=  $current_letter;
        }                
        
        return $password;
    }
    
    public function set_shipping_address($user, $address)
    {
        $CI=& get_instance();
        if(!empty($user) && !empty($address))
        {
            $CI->load->model('address_model');
            $current_address_id = $user->getShippingAddressId();
            
            if (!empty($current_address_id))
            {
                $CI->address_model->update($address);
            }
            else
            {
                $address_id = $CI->address_model->insert($address);
                $user->setShippingAddressId($address_id);
                $CI->load->model('user_model');
                $CI->user_model->update($user);
            }
        }
    }
    
    public function get_following($user_id)
    {
        $CI=& get_instance();
        $users = array();
        
        if (!empty($user_id))
        {
            $CI->load->model("User_model");
            $users = $CI->User_model->get_following($user_id);
        }
        
        return $users;
    }
}

/* End of file User.php */