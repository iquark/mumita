<?php $this->load->view('common/head_open'); ?>
   <title>Administraci&oacute;n de usuarios</title>   
   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/user.css" type="text/css" media="screen, projection" />
<?php $this->load->view('common/head_close', array("context" => "privates")); ?>
   
   <?php 
        $edit = isSet($user);
        $uri_string = explode("/",uri_string());
        $edit_string = ($uri_string[2] == "edit");
        if ($edit || $edit_string) {
            $url_destiny = '/admin/users/edit/'.$id;
            $title = "Editando a ".$user->getFullName();
        } else {
            $url_destiny = '/admin/users/new';
            $title = "Crear nuevo usuario";
        }
   ?>
   
   <h1><?php echo $title; ?></h1>
   
   <?php if (isSet($message)): ?>
       <div class="info"><?php echo $message; ?></div>
   <?php endif; ?>
   
   <?php if (isSet($error)): ?>
       <div class="error"><?php echo $error; ?></div>
   <?php endif; ?>
       
   <?php echo validation_errors(); ?>
   
   <?php echo form_open($url_destiny); ?>
      <?php if ($edit || $edit_string): ?>
       <input type="hidden" id="id" name="id" value="<?php echo $id; ?>" />
      <?php endif; ?>
       
        <div class="field">
            <label for="name">Nombre:</label>
            <input type="text" id="name" name="name" value="<?php echo set_value('name',($edit)?$user->getName():""); ?>" maxlength="50" />
        </div>
        <div class="field">
            <label for="lastname">Apellidos:</label>
            <input type="text" id="lastname" name="lastname" value="<?php echo set_value('lastname', ($edit)?$user->getLastname():""); ?>" maxlength="75" />
        </div>
        <div class="field">
            <label for="sex">Sexo:</label>
            <?php
                $options = array(
                        '0'  => 'Hombre',
                        '1'    => 'Mujer'
                        );
                $default = ($edit)?$user->getSex():array();

                echo form_dropdown('sex', $options, $default, "id='sex'"); 
            ?>
        </div>
       <div class="field">
            <label for="birthday">Fecha de nacimiento:</label>
            <?php
                $options = array();
                for($day=1; $day<=31; $day++) {
                   $options["$day"] = $day;
                }
                
                $year_end = (int)date("Y", strtotime("now"));
                $date = array('mday'=>array(), 'mon'=>array(), 'year'=>$year_end);
                
                if ($edit)
                {
                    $date = getdate(strtotime($user->getBirthday()));
                }
                
                $default = set_value('day', $date['mday']);
                echo form_dropdown('day', $options, $default, "id='day'"); 
                
                $options = array();
                for($month=1; $month<=12; $month++) {
                   $options["$month"] = $month;
                }
                $default = set_value('month', $date['mon']);
                echo form_dropdown('month', $options, $default, "id='month'"); 
                
                $options = array();
                for($year=1910; $year<=$year_end; $year++) {
                   $options[$year] = $year;
                }
                $default = set_value('year', $date['year']);

                echo form_dropdown('year', $options, $default, "id='year'"); 
            ?>
        </div>
        <div class="field">
            <label for="email">Correo electr&oacute;nico:</label>
            <input type="text" id="email" name="email" value="<?php echo set_value('email', ($edit)?$user->getEmail():""); ?>" maxlength="254" />
        </div>
        <div class="field">
    <?php if (!$edit && !$edit_string): ?>
            <label for="password">Contrase&ntilde;a:</label>
            <input type="password" id="password" name="password" maxlength="255" />
        </div>
        <div class="field">
            <label for="repeat_password">Por favor, repite la contrase&ntilde;a:</label>
            <input type="password" id="repeat_password" name="repeat_password" maxlength="75" />
    <?php else: ?>
            <input type="button" name="change_password" id="change_password" value="Cambiar contrase&ntilde;a" />
    <?php endif; ?>
        </div>
        <div class="field">
            <label for="active">Activo:</label>
            <?php
                $checkbox = array(
                        'name'        => 'active',
                        'id'          => 'active',
                        'value'       => '1',
                        'checked'     => ($edit)?(($user->getActive())?TRUE:FALSE):FALSE
                        );
                echo form_checkbox($checkbox); 
            ?>
        </div>
        <div class="field">
            <label for="role_level">Rol:</label>
            <?php
                $options = array(
                        '0'  => 'Usuario',
                        '1'    => 'Administrador'
                        );
                $default = ($edit)?$user->getRoleLevel():array();

                echo form_dropdown('role_level', $options, $default, "id='role_level'"); 
                ?>
       </div>
       <input type="submit" name="register" value="Enviar" />
   </form>
<?php $this->load->view('common/footer_open'); ?>
    <?php echo load_jquery(); ?>
    <?php echo load_js('profile/editmember'); ?>
<?php $this->load->view('common/footer_close'); ?>
