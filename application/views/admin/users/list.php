<?php $this->load->view('common/head_open'); ?>
   <title>Administraci&oacute;n de usuarios</title>   
   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/user.css" type="text/css" media="screen, projection" />
<?php $this->load->view('common/head_close', array("context" => "privates")); ?>
   <h1>Administraci&oacute;n de usuarios</h1>
      
    <?php if (isSet($error)): ?>
	<div class="error"><?php echo $error; ?></div>
    <?php endif; ?>
        
    <?php if (isSet($message)): ?>
	<div class="info"><?php echo $message; ?></div>
    <?php endif; ?>
   
   <?php echo anchor(base_url('admin/users/new'), 'Crear nuevo usuario', 'Crear nuevo usuario'); ?>
   
   <?php foreach($users as $user): ?>
        <?php member_admin($user); ?>
   <?php endforeach; ?>
   
<?php $this->load->view('common/footer'); ?>
