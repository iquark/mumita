<?php $this->load->view('common/head_open'); ?>
   <title>Panel de administraci&oacute;n de Mumita</title>
<?php $this->load->view('common/head_close', array("context" => "privates")); ?>
   <h1>Panel de control de administrador</h1>
   
   <div class="section">
       <img src="<?php echo base_url('assets/images/users/normal.png'); ?>" />
       <div class="menu">
            <ul>
                <li><?php echo anchor(base_url('admin/users'),'Lista de usuarios',''); ?></li>
                <li><?php echo anchor(base_url('admin/users/new'),'Crear usuario',''); ?></li>
            </ul>
       </div>
   </div>
   
<?php $this->load->view('common/footer'); ?>
