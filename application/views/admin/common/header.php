<?php  
      $logged_in = $this->session->userdata('logged_in');
      $role_level = $this->session->userdata('role_level');
    
      if ($logged_in && $role_level>0): ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/admin.css" type="text/css" media="screen, projection" />
    <div id="admin">
        <ul class="menu">
            <li><a href="<?php echo base_url(); ?>admin">Panel</a></li>
            <li><a href="<?php echo base_url(); ?>admin/users">Usuarios</a></li>
            <li><a href="<?php echo base_url(); ?>admin/boxes">Cajas</a></li>
            <li>otras cosas...</li>
        </ul>
        
        <?php if ($this->uri->segment(1) == "admin"): ?>
            <div id="gotoweb">
                <?php echo anchor(base_url('/panel'),'Volver a la web &raquo;',''); ?>
            </div>
        <?php endif; ?>
        
        <div id="notices">
            (0) Avisos
        </div>
    </div>
<?php endif; ?>