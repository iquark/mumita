<?php $this->load->view('common/head_open'); ?>
   <title>Administraci&oacute;n de usuarios</title>   
   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/box.css" type="text/css" media="screen, projection" />
<?php $this->load->view('common/head_close', array("context" => "privates")); ?>
   <h1>Administraci&oacute;n de cajas</h1>

    <?php if (isSet($error)): ?>
	<div class="error"><?php echo $error; ?></div>
    <?php endif; ?>

    <?php if (isSet($message)): ?>
	<div class="info"><?php echo $message; ?></div>
    <?php endif; ?>

   <?php echo anchor(base_url('admin/boxes/new_cloth'), 'Crear nueva caja de ropa', 'Crear nueva caja de ropa'); ?>

   <?php foreach($boxes as $box): ?>
        <?php box_admin($box); ?>
   <?php endforeach; ?>

<?php $this->load->view('common/footer'); ?>
