<?php $this->load->view('common/head_open'); ?>
   <title>Administraci&oacute;n de usuarios</title>   
   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/box.css" type="text/css" media="screen, projection" />
<?php $this->load->view('common/head_close', array("context" => "privates")); ?>
   
   <?php 
        $edit = isSet($box);
        $uri_string = explode("/",uri_string());
        $edit_string = (count($uri_string)>=3 && $uri_string[2] == "edit");
        if ($edit || $edit_string) {
            $url_destiny = '/admin/boxes/edit/'.$id;
            $title = "Editando a ".$box->getAbstract();
        } else {
            $url_destiny = '/admin/boxes/new_cloth';
            $title = "Crear nueva caja";
        }
   ?>
   
   <h1><?php echo $title; ?></h1>
   
   <?php if (isSet($message)): ?>
       <div class="info"><?php echo $message; ?></div>
   <?php endif; ?>
   
   <?php if (isSet($error)): ?>
       <div class="error"><?php echo $error; ?></div>
   <?php endif; ?>
       
   <?php echo validation_errors(); ?>
   
   <?php echo form_open($url_destiny); ?>
       <?php if($edit): ?>
        <input type="hidden" name="id" id="id" value="<?php echo $box->getId(); ?>" />
       <?php endif; ?>
        <div class="field">
            <label for="age">Edad:</label>
            <?php
                $options = array(
                        '1'  => 'Recien nacido',
                        '2'  => '3 meses',
                        '3'  => '6 meses',
                        '4'  => '9 meses',
                        '5'  => '12 meses',
                        '6'  => '15 meses',
                        '7'  => '18 meses',
                        '8'  => '21 meses',
                        '9'  => '24 meses',
                        '10'  => '3 a&ntilde;os',
                        '11' => '4 a&ntilde;os',
                        '12' => '5 a&ntilde;os'
                        );
                $default = ($edit)?$box->getAge():array();

                echo form_dropdown('age', $options, $default, "id='age' tabindex=\"1\""); 
            ?>
        </div>
       
        <div class="field">
            <label for="sex">Sexo:</label>
            <?php
                $options = array(
                        '0'  => 'Unisex',
                        '1'  => 'Ni&ntilde;a',
                        '2'  => 'Ni&ntilde;o'
                        );
                $default = ($edit)?$box->getSex():array();

                echo form_dropdown('sex', $options, $default, "id='sex' tabindex=\"2\""); 
            ?>
        </div>
        
        <div class="field">
            <label for="abstract">Resumen:</label>
            <textarea id="abstract" name="abstract" maxlength="100"  tabindex="3"><?php echo set_value('abstract', ($edit)?$box->getAbstract():""); ?></textarea>
            <div class="little-message">M&aacute;ximo <strong>100</strong> caracteres</div>
        </div>
        
        <div class="field">
            <label for="description">Descripci&oacute;n:</label>
            <textarea id="description" name="description" maxlength="500" tabindex="4"><?php echo set_value('description', ($edit)?$box->getDescription():""); ?></textarea>
            <div class="little-message">M&aacute;ximo <strong>500</strong> caracteres</div>
        </div>
        
        <div class="field">
            <label for="size">Talla:</label>
            <?php
                $options = array(
                        '1'  => '56 cm',
                        '2'  => '62 cm',
                        '3'  => '68 cm',
                        '4'  => '74 cm',
                        '5'  => '80 cm',
                        '6'  => '86 cm',
                        '7'  => '92 cm',
                        '8'  => '104 cm',
                        '9'  => '116 cm',
                        '10'  => '128 cm'
                        );
                $default = ($edit)?$box->getIdSize():array();

                echo form_dropdown('size', $options, $default, "id='size' tabindex=\"5\""); 
            ?>
        </div>
        
        <div class="field">
            <label for="colour1">Colores:</label>
            <select name="colour1" tabindex="6">
                <option value="#0000FF" style="background-color: #0000FF;"></option>
                <option value="#FFFF00" style="background-color: #FFFF00;"></option>
                <option value="#FF0000" style="background-color: #FF0000;"></option>
                <option value="#FF3E96" style="background-color: #FF3E96;"></option>
                <option value="#03A89E" style="background-color: #03A89E;"></option>
            </select>
            <select name="colour2" tabindex="7">
                <option value="#0000FF" style="background-color: #0000FF;"></option>
                <option value="#FFFF00" style="background-color: #FFFF00;"></option>
                <option value="#FF0000" style="background-color: #FF0000;"></option>
                <option value="#FF3E96" style="background-color: #FF3E96;"></option>
                <option value="#03A89E" style="background-color: #03A89E;"></option>
            </select>
            <select name="colour3" tabindex="8">
                <option value="#0000FF" style="background-color: #0000FF;"></option>
                <option value="#FFFF00" style="background-color: #FFFF00;"></option>
                <option value="#FF0000" style="background-color: #FF0000;"></option>
                <option value="#FF3E96" style="background-color: #FF3E96;"></option>
                <option value="#03A89E" style="background-color: #03A89E;"></option>
            </select>
        </div>
      
        <div class="field">
            <label for="season">Talla:</label>
            <?php
                $options = array(
                        '0'  => 'Verano',
                        '1'  => 'Oto&ntilde;o',
                        '2'  => 'Invierno',
                        '3'  => 'Primavera'
                        );
                $default = ($edit)?$box->getSeason():array();

                echo form_dropdown('season', $options, $default, "id='season' tabindex=\"9\""); 
            ?>
        </div>
       <input type="submit" name="send" value="Enviar" />
   </form>
<?php $this->load->view('common/footer'); ?>
