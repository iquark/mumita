<?php $this->load->view('common/head_open'); ?>
   <title>Bienvenidos a Mumita!</title>
   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/register.css" type="text/css" media="screen, projection" />
<?php $this->load->view('common/head_close', array("context" => "publics")); ?>

   <div class="blue_panel">
        <h1>Reg&iacute;strate en Mumita</h1>
        <?php echo validation_errors(); ?>

        <?php if(isSet($register_error)): ?>
                    <?php echo "<div class=\"error\">$register_error</div>"; ?>
        <?php endif; ?>
            <?php echo form_open('registro'); ?>
                <div class="field">
                    <label for="name">Nombre:</label>
                    <input type="text" id="name" name="name" value="<?php echo set_value('name'); ?>" maxlength="50" />
                </div>
                <div class="field">
                    <label for="lastname">Apellidos:</label>
                    <input type="text" id="lastname" name="lastname" value="<?php echo set_value('lastname'); ?>" maxlength="75" />
                </div>
                <div class="field">
                    <label for="sex">Sexo:</label>
                    <?php
                        $options = array(
                                    '0'  => 'Hombre',
                                    '1'    => 'Mujer'
                                );
                        $default = set_value('sex', array());

                        echo form_dropdown('sex', $options, $default, "id='sex'"); 
                    ?>
                </div>
                <div class="field">
                    <label for="birthday">Fecha de nacimiento:</label>
                    <?php
                        $options = array();
                        for($day=1; $day<=31; $day++) {
                        $options["$day"] = $day;
                        }
                        $default = set_value('day', array());
                        echo form_dropdown('day', $options, $default, "id='day'"); 

                        $options = array();
                        for($month=1; $month<=12; $month++) {
                        $options["$month"] = $month;
                        }
                        $default = set_value('month', array());
                        echo form_dropdown('month', $options, $default, "id='month'"); 

                        $year_end = (int)date("Y", strtotime("now"));
                        $options = array();
                        for($year=1910; $year<=$year_end; $year++) {
                        $options[$year] = $year;
                        }
                        $default = set_value('year', $year_end);

                        echo form_dropdown('year', $options, $default, "id='year'"); 
                    ?>
                </div>
                <div class="field">
                    <label for="email">Correo electr&oacute;nico:</label>
                    <input type="text" id="email" name="email" value="<?php echo set_value('email'); ?>" maxlength="254" />
                </div>
                <div class="field">
                    <label for="password">Contrase&ntilde;a:</label>
                    <input type="password" id="password" name="password" maxlength="255" />
                </div>
                <div class="field">
                    <label for="repeat_password">Por favor, repite la contrase&ntilde;a:</label>
                    <input type="password" id="repeat_password" name="repeat_password" maxlength="255" />
                </div> 
            <input type="submit" name="submit" value="Enviar" />
        </form>
   </div>
<?php $this->load->view('common/footer'); ?>