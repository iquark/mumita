<?php $this->load->view('common/head_open'); ?>
   <title>Bienvenidos a Mumita!</title>
<?php $this->load->view('common/head_close', array("context" => "publics")); ?>

<?php if (isSet($message)): ?>
	<div class="info"><?php echo $message; ?></div>
<?php endif; ?>
	
	<?php $this->load->view('security/form/login_form'); ?>
	
<?php $this->load->view('common/footer'); ?>
