<?php $this->load->view('common/head_open'); ?>
   <title>Bienvenidos a Mumita!</title>
   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/index.css" type="text/css" media="screen, projection" />
<?php $this->load->view('common/head_close', array("context" => "publics")); ?>
   <div id="abstract">
       <div class="left">
           <h1>LA WEB DE INTERCAMBIO DE ROPA INFANTIL ENTRE MADRES Y PADRES</h1>
           <p>
               MUMITA nace para facilitar el intercambio de ropa infantil entre 
               padres y madres. El uso de MUMITA es muy sencillo, aquí te dejamos 
               un video para que lo entiendas muy facilmente.
           </p>
           <p>
               <?php 
                $button = load_image("index/bot_registration.png", $title="Registrate para acceder a Mumita"); 
                echo anchor(base_url('registro'), $button, "title='Registrate para acceder a Mumita!'"); 
                ?>
           </p>
       </div>
       <div class="right">
           
       </div>
   </div>

            </div>  <!-- .content -->
        </div>  <!-- #body -->
    </div>  <!-- #wrapper -->
   
           <div id="peaks_mosaic" class="push">
           </div>   
   <div id="faq">
       <div class="left">
           <h2>PREGUNTAS FRECUENTES</h2>
           <ul>
               <li>&iquest;C&oacute;mo consigo las cajas de MUMITA?</li>
               <li>&iquest;Cu&aacute;nto gano por cada venta?</li>
               <li>Si mi caja viene vacia &iquest;a qui&eacute;n reclamo?</li>
               <li>&iquest;Puedo introducir todo tipo de productos en la caja?</li>
               <li>&iquest;Por qu&eacute; no me deja realizar mas de 2 compras?</li>
               <li>&iquest;Por qu&eacute; no puedo poner mas de 2 cajas a la venta?</li>
           </ul>
       </div>
       <div class="right">
           <h2>AS&Iacute; FUNCIONA MUMITA</h2>
           <div>
               <?php echo load_image("index/boxes.png", "Como funciona Mumita"); ?>
           </div>
       </div>
   </div>
            
            
        </div>  <!-- #wrapper -->
        
         
           <div id="peaks" class="push">
           </div>
        <div id="footer">
            <div class="content">
                <div>
                <div id="sign-newsletter">
                    <form>
                        <div class="header-news"><h3>QUIERO RECIBIR LA NEWSLETTER DE MUMITA</h3></div>
                        <div class="field"><label for="news_name">Nombre y Apellidos</label>
                        <input type="text" name="news_name" id="news_name" /></div>
                        <div class="field"><label for="news_email">Email</label>
                        <input type="text" name="news_email" id="news_name" /></div>
                        <div class="footer-news"> Proteccion de datos</div>
                    </form>
                </div> 
                <div id="contact">
                    <div id="contact-company">
                        <p><span>MUMITA es un producto de FUTURA S.L.<br />
                        Ante cualquier duda o consulta tambi&eacute;n se pueden poner en contacto
                        con nosotros en:</span><br />
                        Avda. Rep. Argentina, 8 Gand&iacute;a (Valencia)</p>
                        <p><span>Tel&eacute;fonos de informaci&oacute;n:</span><br />
                        962.872.649<br />
                        962.832.395</p>
                        <p><span>Email de contacto:</span><br />
                        info@mumita.com</p>
                    </div>
                    <div id="socialmedia">

                    </div>
                </div>
                </div>
                <div id="copyright">
                Mumita &copy; 2012. Todos los derechos reservados
                <div style="float:right">
                    Contacto | Privacidad | Facebook | Twitter
                </div>
                </div>
            </div> <!-- .content -->
        </div> <!-- #footer -->
        <script type="text/javascript">
            function load() {

            }
        </script>
  </body>
 </html>