<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $title;?></title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/base.css" type="text/css" media="screen, projection" />
        <?php echo $stylesheets;?>
        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />
    </head>
    <body>
        <div id="wrapper">
            <?php echo $header;?>
            
            <div id="body">
                <div class="content">
                <?php echo $body;?>
                </div>  <!-- .content -->
            </div>  <!-- #body -->
            
        </div>  <!-- #wrapper -->
        
        <div id="peaks">
        </div>
         
        <div id="footer">
            <div class="content">
                <div>
            <!--    <div id="sign-newsletter">
                    <form>
                        <fieldset>
                            <legend>QUIERO RECIBIR LA NEWSLETTER DE MUMITA</legend>
                            <label for="news_name">Nombre y Apellidos</label>
                            <input type="text" name="news_name" id="news_name" />

                        </fieldset>
                    </form>
                </div> -->
                <div id="contact">
                    <div id="contact-company">
                        <p><span>MUMITA es un producto de FUTURA S.L.<br />
                        Ante cualquier duda o consulta tambi&eacute;n se pueden poner en contacto
                        con nosotros en:</span><br />
                        Avda. Rep. Argentina, 8 Gand&iacute;a (Valencia)</p>
                        <p><span>Tel&eacute;fonos de informaci&oacute;n:</span><br />
                        962.872.649<br />
                        962.832.395</p>
                        <p><span>Email de contacto:</span><br />
                        info@mumita.com</p>
                    </div>
                    <div id="socialmedia">

                    </div>
                </div>
                </div>
                <div id="copyright">
                Mumita &copy; 2012. Todos los derechos reservados
                <div style="float:right">
                    Contacto | Privacidad | Facebook | Twitter
                </div>
                </div>
            </div> <!-- .content -->
        </div> <!-- #footer -->
        <?php echo $javascripts;?>
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-29762148-1']);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();

        </script>
    </body>
</html>