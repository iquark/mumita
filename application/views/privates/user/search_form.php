<div id="search_panel" class="blue_panel">
    <?php echo form_open('comunidad/buscar'); ?>
            <?php if (isSet($message)): ?>
                <div class="info"><?php echo $message; ?></div>
            <?php endif; ?>

            <?php if (isSet($error)): ?>
                <div class="error"><?php echo $error; ?></div>
            <?php endif; ?>

            <?php $errors = validation_errors(); 
                  if(!empty($errors)): ?>
                <div class="error"><?php echo $errors; ?></div>
            <?php endif; ?>
       <div class="right">
           <input type="submit" name="submit" id="submit" value="Buscar" tabindex="5" />
       </div>
       <div>
            <div id="simple">
                <label for="search">Buscar: </label>
                <input type="text" name="search" id="search" value="<?php echo set_value('search'); ?>" tabindex="1" />
            </div>
       <div>
         En gente que venda:  
       </div>
            <div id="advanced">
                <label for="sex">Sexo</label>
                <?php
                    $options = array(
                            '-1' => 'Todos',
                            '0'  => 'Unisex',
                            '1'  => 'Ni&ntilde;a',
                            '2'  => 'Ni&ntilde;o'
                            );
                    echo form_dropdown('sex', $options, set_value('sex', -1),"id='sex' tabindex=\"2\""); 
                ?>

                <label for="size">Selecciona talla:</label>
                <?php
                    $options = array(
                            '-1' => 'Todas',
                            '1'  => '56 cm',
                            '2'  => '62 cm',
                            '3'  => '68 cm',
                            '4'  => '74 cm',
                            '5'  => '80 cm',
                            '6'  => '86 cm',
                            '7'  => '92 cm',
                            '8'  => '104 cm',
                            '9'  => '116 cm',
                            '10'  => '128 cm'
                            );

                    echo form_dropdown('size', $options, set_value('size', -1),"id='size' tabindex=\"3\""); 
                ?>
                <label for="season">Temporada:</label>
                <?php
                    $options = array(
                            '-1' => 'Todas',
                            '0'  => 'Verano',
                            '1'  => 'Oto&ntilde;o',
                            '2'  => 'Invierno',
                            '3'  => 'Primavera'
                            );

                    echo form_dropdown('season', $options, set_value('season', -1), "id='season' tabindex=\"4\""); 
                ?>
            </div>
        </div>
    </form>
</div>
