<?php $this->load->view('common/head_open'); ?>
   <title>Perfil de <?php echo $user->getFullName(); ?></title>
   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/profile.css" type="text/css" media="screen, projection" />
   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/user.css" type="text/css" media="screen, projection" />
<?php $this->load->view('common/head_close', array("context" => "privates")); ?>
   <div id="member_data">
    <?php echo form_open_multipart('mi_perfil/guardar/1');?>     
        <?php if (isSet($message)): ?>
            <div class="info"><?php echo $message; ?></div>
        <?php endif; ?>

        <?php if (isSet($error)): ?>
            <div class="error"><?php echo $error; ?></div>
        <?php endif; ?>

        <?php echo validation_errors(); ?>
            <div id="photo" class="sidebar">
                <?php echo photo_normal($user); ?><br />
                <input type="file" name="userfile" size="3" style="width:130px" />
            </div>
            <div class="left">
                <div class="field">
                    <label for="name">Nombre: </label>
                    <input type="text" name="name" id="name" value="<?php echo $user->getName(); ?>" tabindex="1"/> 
                </div>
                <div class="field">
                    <label for="lastname">Apellidos: </label>
                    <input type="text" name="lastname" id="lastname" value="<?php echo $user->getLastname(); ?>" tabindex="2" />
                </div>
                <div class="field">
                    <label for="sex">Sexo: </label>
                    <?php
                        $options = array(
                            '0'  => 'Hombre',
                            '1'    => 'Mujer'
                            );
                        $default = $user->getSex();
                        echo form_dropdown('sex', $options, $default, "id='sex' tabindex=\"3\""); 
                    ?>
                </div>
                <div class="field">
                    <label for="email">Correo electr&oacute;nico:</label>
                    <input type="text" id="email" name="email" value="<?php echo set_value('email', $user->getEmail()); ?>" maxlength="254" tabindex="4" />
                </div>
                <div class="field">
                    <input type="button" name="change_password" id="change_password" value="Cambiar contrase&ntilde;a" tabindex="5" />
                </div>
                <h2>Direcci&oacute;n de recogida y env&iacute;o</h2>
                
                <div class="field">
                    <label for="street">Calle:</label>
                    <input type="text" id="street" name="street" value="<?php echo set_value('street', $user->getShippingAddress()->getStreet()); ?>" maxlength="200" tabindex="7" />
                </div>
                <div class="field">
                    <label for="city">Ciudad:</label>
                    <input type="text" id="city" name="city" value="<?php echo set_value('city', $user->getShippingAddress()->getCity()); ?>" maxlength="200" tabindex="8" />
                </div>
                <div class="field">
                    <label for="province">Provincia:</label>
                    <input type="text" id="province" name="province" value="<?php echo set_value('province', $user->getShippingAddress()->getProvince()); ?>" maxlength="200" tabindex="9" />
                </div>
                <div class="field">
                    <label for="postal_code">C&oacute;digo Postal:</label>
                    <input type="text" id="postal_code" name="postal_code" value="<?php echo set_value('postal_code', $user->getShippingAddress()->getPostalCode()); ?>" maxlength="10" tabindex="10" />
                </div>
                <input type="submit" value="Guardar" id="submit" name="submit" style="float:right;clear: left;margin-bottom: 10px" tabindex="7" />
            </div>
       </form>
   </div>

<?php $this->load->view('common/footer_open'); ?>
    <?php echo load_jquery(); ?>
    <?php echo load_js('profile/editmember'); ?>
<?php $this->load->view('common/footer_close'); ?>