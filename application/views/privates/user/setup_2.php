<?php $this->load->view('common/head_open'); ?>
   <title>Perfil de <?php echo $user->getFullName(); ?></title>
   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/profile.css" type="text/css" media="screen, projection" />
   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/user.css" type="text/css" media="screen, projection" />
<?php $this->load->view('common/head_close', array("context" => "privates")); ?>
   pagina2
   <div id="member_data">
       <div id="photo">
           <?php photo_normal($user); ?>
           <?php echo anchor(current_url().'#','Cambiar foto',''); ?>
       </div>
       <h1><?php echo $user->getFullName(); ?></h1>

       <div id="address"><?php echo $user->getShippingAddress()->getBrief(); ?></div>
   </div>
   
<?php $this->load->view('common/footer'); ?> 