<?php $this->load->view('common/head_open'); ?>
   <title>Perfil de <?php echo $user->getFullName(); ?></title>
   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/profile.css" type="text/css" media="screen, projection" />
   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/user.css" type="text/css" media="screen, projection" />
<?php $this->load->view('common/head_close', array("context" => "privates")); ?>
   <div id="member_data">    
        <?php if (isSet($message)): ?>
            <div class="info"><?php echo $message; ?></div>
        <?php endif; ?>

        <?php if (isSet($error)): ?>
            <div class="error"><?php echo $error; ?></div>
        <?php endif; ?>
        <div id="user_data" class="left">
            <div id="photo" class="left">
                <?php echo photo_normal($user); ?>
            </div>  
            <div class="left">
                <h1><?php echo $user->getName(); ?> </h1>
            </div>
            <div id="valoration">
                <?php show_valoration($user->getValoration()); ?>
            </div>
            <div id="balance">
                <label>Balance: </label>
                <?php echo $user->getBalance(); ?>
            </div>
            <div id="box-panel">
                <div class="right">
                    <?php $address = $user->getShippingAddress(); ?>
                    <?php if(!empty($address)): ?>
                        Actualmente tus datos de env&iacute;o son:<br />
                        <?php echo $address->getStreet(); ?><br />
                        <?php echo $address->getPostalCode(); ?> <?php echo $address->getCity(); ?><br />
                        <?php echo $address->getProvince(); ?> (<?php echo $address->getCountry(); ?>)<br />
                    <?php else: ?>
                        &iexcl;No tienes una direcci&oacute;n de env&iacute;o y recepci&ocute;n!
                    <?php endif; ?>
                    <div class="right">
                        <a href="<?php echo base_url('mi_perfil/editar/1'); ?>" id="edit_btn">
                            <input type="button" id="edit" name="edit" value="Editar" />
                        </a>    
                    </div>
                </div>
                <?php echo $user->getFullName(); ?><br />
                <label>Usuario desde:</label>
                <?php echo $user->getCreatedAt(); ?><br />
                <label>N&uacute;mero de cajas enviadas:</label>
                <?php echo count($user->getDeliveredOrders()); ?><br />
                <label>N&uacute;mero de pedidos realizados:</label>
                <?php echo count($user->getReceivedOrders()); ?>
                <div id="box-list">
                    <h2>CAJAS DISPONIBLES DE ESTE USUARIO</h2>
                <?php
                     $boxes = $user->getBoxes();
                     if (count($boxes)==0): ?>
                        No hay cajas todavía, <?php echo anchor("/caja/nueva", "¡crea una ahora!"); ?>.
               <?php else: 
                        for($idx = 0; $idx < 3 && $idx < count($boxes); $idx++){
                            box_slim($boxes[$idx]);
                        }
                        if (count($boxes)>3) {
                            echo anchor("/cajas", 
                                        "Ver las ".count($boxes)." cajas.",
                                        "Ver las ".count($boxes)." cajas.");
                        }
                     endif; ?>
                </div>
            </div>
        </div><!-- #user_data -->
        
        <div id="user_comments" class="right">
            <div class="comment">
                <div class="metadata">
                    <?php show_valoration(3); ?>
                    Name Lastname
                </div>
                <div class="comment_content">
                    Lorem ipsum dolor sit amet, consectetur adipisicing 
                    elit, sed do eiusmod tempor incididunt ut labore et 
                    dolore magna aliqua.
                </div>
            </div>
            <div class="comment">
                <div class="metadata">
                    <?php show_valoration(2); ?>
                    Name Lastname
                </div>
                <div class="comment_content">
                    Lorem ipsum dolor sit amet, consectetur adipisicing 
                    elit, sed do eiusmod tempor incididunt ut labore et 
                    dolore magna aliqua.
                </div>
            </div>
            <div class="comment">
                <div class="metadata">
                    <?php show_valoration(1); ?>
                    Name Lastname
                </div>
                <div class="comment_content">
                    Lorem ipsum dolor sit amet, consectetur adipisicing 
                    elit, sed do eiusmod tempor incididunt ut labore et 
                    dolore magna aliqua.
                </div>
            </div>
            <div class="comment">
                <div class="metadata">
                    <?php show_valoration(5); ?>
                    Name Lastname
                </div>
                <div class="comment_content">
                    Lorem ipsum dolor sit amet, consectetur adipisicing 
                    elit, sed do eiusmod tempor incididunt ut labore et 
                    dolore magna aliqua.
                </div>
            </div>
        </div><!-- #user_comments -->
    </div>
   
<?php $this->load->view('common/footer_open'); ?>
    <?php echo load_jquery(); ?>
    <?php echo load_js('profile/editmember'); ?>
<?php $this->load->view('common/footer_close'); ?>