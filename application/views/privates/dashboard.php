<?php $this->load->view('common/head_open'); ?>
   <title>Panel</title>
<?php $this->load->view('common/head_close', array("context" => "privates")); ?>
   <div id="your_info" class="dashboard">
   <h1>T&uacute;</h1>
   <?php foreach($your_info as $info): ?>
    <?php echo your_info($info); ?>
   <?php endforeach; ?>
   </div>
   <div id="your_friends" class="dashboard">
    <h1>Gente a la que sigues</h1>
    <?php foreach($friends_info as $info): ?>
        <?php echo friends_info($info); ?>
    <?php endforeach; ?>
    
   </div>
   <div id="boxes_you_like" class="dashboard">
    <h1>Cajas que te podr&iacute;an interesar</h1>
    
   </div>
   <div id="people_you_can_follow" class="dashboard">
    <h1>Gente a la que podr&iacute;as seguir</h1>
    
   </div>
<?php $this->load->view('common/footer'); ?>