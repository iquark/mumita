<?php $this->load->view('common/head_open'); ?>
   <title>Panel</title>
   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/community.css" type="text/css" media="screen, projection" />
   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/user.css" type="text/css" media="screen, projection" />
<?php $this->load->view('common/head_close', array("context" => "privates")); ?>

    <h1>Comunidad</h1>

    <?php $this->load->view('privates/user/search_form'); ?>

    <div id="results">
    Se han encontrado <strong><?php echo $total_rows; ?></strong> miembros
    </div>

    <div id="members">
    <?php $user_count = count($users); ?>
       
    <?php for($idx=0; $idx<$user_count; $idx++): ?>
        <?php if (($idx)%3==0): ?>
        <div class="row-users">
        <?php endif; ?>
        
        <?php echo member_tiny($users[$idx]); ?>
        
        <?php if (($idx+1)%3==0 || ($idx+1)==$user_count): ?>
            </div>
        <?php endif; ?>
   <?php endfor; ?>
   	</div>

   <?php 
   if ($total_rows>0):
        $links = $this->pagination->create_links(); 
        if (!empty($links)): ?>
    <div id="pagination">
        <?php echo $links; ?>
    </div>
   <?php    
        endif; 
    endif; ?>
<?php $this->load->view('common/footer'); ?> 