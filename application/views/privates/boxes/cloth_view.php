<?php $this->load->view('common/head_open'); ?>
   <title><?php $box->getAbstract(); ?></title>
   <?php echo load_css("box"); ?>
<?php $this->load->view('common/head_close', array("context" => "privates")); ?>
   
   <?php if (isSet($message)): ?>
       <div class="info"><?php echo $message; ?></div>
   <?php endif; ?>
   
   <?php if (isSet($error)): ?>
       <div class="error"><?php echo $error; ?></div>
   <?php endif; ?>
       
    <?php 
        $box_id = $box->getId();
        if (!empty($box_id)): 
    ?>
       <div id="view_box">
           <div class="right">
               <div id="attributes">
                    <div class="attribute">
                        <label>TALLA:</label>
                            <div class="value"><?php 
                            $sizes = array(
                                '1'  => '56 cm',
                                '2'  => '62 cm',
                                '3'  => '68 cm',
                                '4'  => '74 cm',
                                '5'  => '80 cm',
                                '6'  => '86 cm',
                                '7'  => '92 cm',
                                '8'  => '104 cm',
                                '9'  => '116 cm',
                                '10'  => '128 cm'
                                );
                            $size = $box->getIdSize();
                            echo $sizes[(!empty($size))?$size:1]; ?></div>
                    </div>
                    <div class="attribute">
                        <label>TEMPORADA:</label>
                            <div class="value"><?php echo box_season($box->getSeason()); ?></div>
                    </div>
                    <div class="attribute">
                        <label>SEXO:</label>
                            <div class="value"><?php echo box_sex($box->getSex()) ?></div>
                    </div>
                    <div class="attribute">
                        <label>COLORES:</label>
                        <div class="value">
                                <?php $colour = $box->getColours(); ?>
                                <div class="colour" style="background-color:#<?php echo $colour[0]; ?>"></div>
                                <div class="colour" style="background-color:#<?php echo $colour[1]; ?>"></div>
                                <div class="colour" style="background-color:#<?php echo $colour[2]; ?>"></div>
                        </div>
                    </div>
                </div>
               <div id="payment">
                <h2>Precio Caja: <span id="box_price">6.5</span>&euro;</h2>
                5&euro; gastos de env&iacute;o
                <div id="select_payment_type">
                    <?php echo form_open('pedido/crear'); ?>
                        <input type="hidden" value="<?php echo $box_id; ?>" name="box_id" />
                        <input type="radio" name="payment_type" id="credit_card" value="credit_card" /> 
                        <label for="credit_card">
                        <?php echo load_image("icons/visa.png", "Pago con tarjeta de credito");?>
                        <?php echo load_image("icons/mastercard.png", "Pago con tarjeta de credito");?>
                        </label>
                        <input type="radio" name="payment_type" id="paypal" value="paypal" />
                        <label for="paypal">
                        <?php echo load_image("icons/paypal.png", "Pago con PayPal");?>
                        </label>
                        <input type="submit" value="Solicitar Caja" name="submit" />
                    </form>
                </div>
               </div>
           </div>
           <div class="left">
                <div id="box_data">
                    <div id="photo_gallery">
                        <div id="big_photo">
                                    <?php echo photo_box_big($box);?>
                        </div>
                   <?php 
                   /*     <div id="tiny_photos">
                            <div class="tiny_photo"></div>
                            <div class="tiny_photo"></div>
                            <div class="tiny_photo"></div>
                            <div class="tiny_photo last"></div>

                        </div>
                    
                    */ ?>
                        <?php echo show_valoration($box->getValoration()); ?>
                        <?php echo form_open('caja/valorar/'.$box->getId()); ?>
                            <label for="valoration1">1</label>
                            <input type="radio" name="valoration" id="valoration1" value="1" />
                            <label for="valoration2">2</label>
                            <input type="radio" name="valoration" id="valoration2" value="2" />
                            <label for="valoration2">3</label>
                            <input type="radio" name="valoration" id="valoration3" value="3" />
                            <label for="valoration2">4</label>
                            <input type="radio" name="valoration" id="valoration4" value="4" />
                            <label for="valoration2">5</label>
                            <input type="radio" name="valoration" id="valoration5" value="5" />
                            <input type="submit" name="valorate" id="valorate" value="Valorar" />
                        </form>
                    </div>

                    <div id="data_box">
                        <h1><?php echo $box->getAbstract(); ?></h1>
                        <?php if($box->getUser()->getId()==$user_id): ?>
                        <div style="float:right">
                            <?php echo anchor("/caja/editar/".$box->getId(), "Editar Caja", "title='Editar esta caja'"); ?>
                        </div>
                        <?php endif; ?>
                        <p>
                            <?php echo $box->getDescription(); ?>
                        </p>
                        <div id="metadata">
                            <div id="box">
                                <h2>Caja montada por</h2>
                                <?php
                                 $user = $box->getUser();
                                 echo (!empty($user))?$user->getFullName():""; ?><br />
                                <?php show_valoration($box->getValoration()); ?>
                            </div>
                            <div id="other">
                                Denunciar venta
                            </div>
                        </div>
                    </div>
                </div> <!-- #box_data -->
                <?php $this->load->view("privates/boxes/comments", array("box"=>$box)); ?>
           </div>
           
       </div>
        <?php endif; ?>

<?php $this->load->view('common/footer_open'); ?>
<?php $this->load->view('common/footer_close'); ?>