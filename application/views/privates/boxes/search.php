<?php $this->load->view('common/head_open'); ?>
   <title>Tus cajas</title>   
   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/box.css" type="text/css" media="screen, projection" />
<?php $this->load->view('common/head_close', array("context" => "privates")); ?>
    
    <?php $this->load->view('privates/boxes/search_form'); ?>
    
    <?php if (isSet($error)): ?>
	<div class="error"><?php echo $error; ?></div>
    <?php endif; ?>

    <?php if (isSet($message)): ?>
	<div class="info"><?php echo $message; ?></div>
    <?php endif; ?>
    
    <?php if(isSet($boxes)): ?>
        <?php $this->load->view('privates/boxes/list_boxes', array('boxes'=>$boxes)); ?>
    <?php endif; ?>

<?php $this->load->view('common/footer'); ?>
