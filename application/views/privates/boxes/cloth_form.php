<?php $this->load->view('common/head_open'); ?>
   <title>Administraci&oacute;n de usuarios</title>
   <?php echo load_css("box"); ?>
   <?php echo load_css_third_party("colourPicker/colourPicker"); ?>
<?php $this->load->view('common/head_close', array("context" => "privates")); ?>
   
   <?php 
        $edit = isSet($box);
        $uri_string = explode("/",uri_string());
        $edit_string = (count($uri_string)>=3 && $uri_string[2] == "edit");
        if ($edit || $edit_string) {
            $url_destiny = '/caja/editar/'.$id;
            $title = "Editando ".$box->getAbstract();
        } else {
            $url_destiny = '/caja/nueva';
            $title = "Publicar nueva caja de ropa";
        }
   ?>
   
    <div id="create_box">
        <?php echo form_open_multipart($url_destiny); ?>
            <h1><?php echo $title; ?></h1>
        <?php if (isSet($message)): ?>
            <div class="info"><?php echo $message; ?></div>
        <?php endif; ?>

        <?php if (isSet($error)): ?>
            <div class="error"><?php echo $error; ?></div>
        <?php endif; ?>

        <?php $errors = validation_errors(); 
                if(!empty($errors)): ?>
            <div class="error"><?php echo $errors; ?></div>
        <?php endif; ?>
        <?php if($edit): ?>
            <input type="hidden" name="id" id="id" value="<?php echo $box->getId(); ?>" />
        <?php endif; ?>
        <?php 
            if($edit) {
                $box_id = $box->getId();
            } 
        ?>
        <?php if ($edit && !empty($box_id) || !$edit): ?>
            <div class="form-left">
                <div class="field">
                    <label for="abstract">Resumen del contenido:</label>
                    <input type="text" id="abstract" name="abstract" maxlength="100" tabindex="1" value="<?php echo set_value('abstract', ($edit)?$box->getAbstract():""); ?>" />
                </div>

                <div class="field">
                    <label for="description">Descripci&oacute;n:</label><br />
                    <textarea id="description" name="description" maxlength="500" tabindex="2"><?php echo set_value('description', ($edit)?$box->getDescription():""); ?></textarea>
                    <div class="little-message">M&aacute;ximo <strong>500</strong> caracteres</div>
                </div>
                <div class="field file">
                    <label for="description">Elige una foto (maximo 1024x768 y 500kb):</label>
                    <input type="file" id="photo1_btn" name="userfile" value="Agregar" />
                </div> 
            </div>  
            <div class="form-right">
                <div class="field">
                    <label for="size">Selecciona talla:</label>
                    <?php
                        $options = array(
                                '1'  => '56 cm',
                                '2'  => '62 cm',
                                '3'  => '68 cm',
                                '4'  => '74 cm',
                                '5'  => '80 cm',
                                '6'  => '86 cm',
                                '7'  => '92 cm',
                                '8'  => '104 cm',
                                '9'  => '116 cm',
                                '10'  => '128 cm'
                                );
                        $default = ($edit)?$box->getIdSize():array();

                        echo form_dropdown('size', $options, $default, "id='size' tabindex=\"5\""); 
                    ?>
                </div>

                <div class="field">
                    <label for="season">Temporada:</label>
                    <?php
                        $options = array(
                                '0'  => 'Verano',
                                '1'  => 'Oto&ntilde;o',
                                '2'  => 'Invierno',
                                '3'  => 'Primavera'
                                );
                        $default = ($edit)?$box->getSeason():array();

                        echo form_dropdown('season', $options, $default, "id='season' tabindex=\"9\""); 
                    ?>
                </div>
                <div class="field">
                    <label for="sex">Sexo:</label>
                    <?php
                        $options = array(
                                '0'  => 'Unisex',
                                '1'  => 'Ni&ntilde;a',
                                '2'  => 'Ni&ntilde;o'
                                );
                        $default = ($edit)?$box->getSex():array();

                        echo form_dropdown('sex', $options, $default, "id='sex' tabindex=\"2\""); 
                    ?>
                </div>
<?php
    $color = array("ffffff", "ffccc9", "ffce93", "fffc9e", "ffffc7", "9aff99", "96fffb",
                   "cdffff", "cbcefb", "cfcfcf", "fd6864", "fe996b", "fffe65", "fcff2f",
                   "67fd9a", "38fff8", "68fdff", "9698ed", "c0c0c0", "fe0000", "f8a102",
                   "ffcc67", "f8ff00", "34ff34", "68cbd0", "34cdf9", "6665cd", "9b9b9b",
                   "cb0000", "f56b00", "ffcb2f", "ffc702", "32cb00", "00d2cb", "3166ff",
                   "6434fc", "656565", "9a0000", "ce6301", "cd9934", "999903", "009901",
                   "329a9d", "3531ff", "6200c9", "343434", "680100", "963400", "986536",
                   "646809", "036400", "34696d", "00009b", "303498", "000000", "330001",
                   "643403", "663234", "343300", "013300", "003532", "010066", "340096");
?>
                <?php $colours = ($edit)?$box->getColours():array("ffffff","ffffff","ffffff"); ?>
                <div class="field colour" id="colour1">
                    <?php $colour1 = (isSet($colours[0]))?$colours[0]:"ffffff"; ?>
                    <label for="colour1">Colores:</label>
                    <select name="colour1" tabindex="6">
                        <?php foreach($color as $c): ?>
                            <option value="<?php echo $c ?>" <?php echo ($c == $colour1)? 'selected="selected"':''; ?> 
                                    style="background-color:#<?php echo $c ?>"></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                    
                <div class="field colour" id="colour2">
                    <?php $colour2 = (isSet($colours[1]))?$colours[1]:"ffffff"; ?>
                    <label for="colour2">Colores:</label>
                    <select name="colour2" tabindex="6">
                        
                        <?php foreach($color as $c): ?>
                            <option value="<?php echo $c ?>" <?php echo ($c == $colour2)? 'selected="selected"':''; ?> 
                                    style="background-color:#<?php echo $c ?>"></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="field colour" id="colour3">
                    <?php $colour3 = (isSet($colours[2]))?$colours[2]:"ffffff"; ?>
                    <label for="colour3">Colores:</label>
                    <select name="colour3" tabindex="6">
                        
                        <?php foreach($color as $c): ?>
                            <option value="<?php echo $c ?>" <?php echo ($c == $colour3)? 'selected="selected"':''; ?> 
                                    style="background-color:#<?php echo $c ?>"></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="buttons">
                <input type="submit" name="send" value="Guardar Caja" />
            </div>
        </form>
    <?php endif; ?>
   </div>
<?php $this->load->view('common/footer_open'); ?>
    <?php echo load_jquery(); ?>
    <?php echo load_js_third_party('colourPicker'); ?>
    <script type="text/javascript">
        // Array of opened colour pickers
        var opened = new Array();
        
       jQuery('#colour1 select').colourPicker({
            ico:    '<?php echo asset_url("images/third_party/colourPicker/colourPicker.gif"); ?>', 
            title: false,
            inputBG:    true,
            id: 'jquery-colour-picker-1'              
        });
       jQuery('#colour2 select').colourPicker({
            ico:    '<?php echo asset_url("images/third_party/colourPicker/colourPicker.gif"); ?>', 
            title: false,
            inputBG:    true,
            id: 'jquery-colour-picker-2'              
        });
       jQuery('#colour3 select').colourPicker({
            ico:    '<?php echo asset_url("images/third_party/colourPicker/colourPicker.gif"); ?>', 
            title: false,
            inputBG:    true,
            id: 'jquery-colour-picker-3'              
        });
   </script>
<?php $this->load->view('common/footer_close'); ?>