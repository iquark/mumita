    <div id="comments">
        <?php $comment = $box->getComments(); ?>
        <?php for($idx=0; $idx<count($comment); $idx++): ?>
        <div class="comment">
            <a name="<?php echo $comment[$idx]->getId(); ?>"></a>
            <div class="date"><?php echo $comment[$idx]->getCreatedAt(); ?></div>
            <div class="commenter">
                <?php echo photo_tiny($comment[$idx]->getUser()); ?>
                <div><?php echo $comment[$idx]->getUser()->getFullName(); ?></div>
                <div><?php echo show_valoration($comment[$idx]->getUser()->getValoration()); ?></div>
            </div>
            <?php echo $comment[$idx]->getComment(); ?>
        </div>
        <?php endfor; ?>
        
        <div class="comment form">
            <div class="commenter">
                <?php echo photo_tiny($box->getUser());?>
            </div>
            <div style="float:right">
            <?php echo form_open("caja/comentar/".$box->getId()); ?>
            <?php if (isSet($message)): ?>
                <div class="info"><?php echo $message; ?></div>
            <?php endif; ?>

            <?php if (isSet($error)): ?>
                <div class="error"><?php echo $error; ?></div>
            <?php endif; ?>

            <?php $errors = validation_errors(); 
                  if(!empty($errors)): ?>
                <div class="error"><?php echo $errors; ?></div>
            <?php endif; ?>
                <div><textarea name="comment" id="comment" style="width: 570px; height: 100px;clear: right;"></textarea></div>
                <input type="submit" name="send_comment" id="send_comment" value="Enviar comentario" />
            </form>
            </div>
        </div>
    </div>