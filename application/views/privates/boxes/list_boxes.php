
    <?php $box_count = count($boxes); ?>
    <div class="box long" style="background-color:transparent;clear:both;">
        <?php for($idx=0; $idx<$box_count; $idx++): ?>
            <?php if (($idx)%4==0): ?>
            <div class="row-boxes">
            <?php endif; ?>
                
            <?php box_user($boxes[$idx], (($idx+1)%4==0)?true:false); ?>
                
            <?php if (($idx+1)%4==0 || ($idx+1)==$box_count): ?>
                </div>
            <?php endif; ?>
        <?php endfor; ?>
           
        <?php 
        if ($box_count>0):
                $links = $this->pagination->create_links(); 
                if (!empty($links)): ?>
            <div id="pagination">
                <?php echo $links; ?>
            </div>
        <?php    
                endif; 
            endif; ?>
    </div>