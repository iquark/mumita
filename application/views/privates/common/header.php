<?php
    require_once(dirname(__FILE__) . '/../../../libraries/entities/User_entity.php');
?>
<div id="header">
    <?php
      $logged_in = $this->session->userdata('logged_in');
      $role_level = $this->session->userdata('role_level');
      $user = unserialize($this->session->userdata('user_data'));
    ?>
    <div class="content">    
        <a href="/home" title="Logo de Mumita" id="main_logo"><img src="<?php echo base_url(); ?>assets/images/logos/logoheader.png" title="Intercambia la ropa usada de tu bebe!" /></a>

    <?php if ($logged_in): ?>
        <div id="profile">
            <div class="info_profile">
                <div class="icon">
                    <img src="/assets/images/icons/note.png" />
                </div>
                <div class="value">
                    36€
                </div>
            </div>
            <div class="info_profile">
                <div class="icon">
                    <img src="/assets/images/icons/box.png" />
                </div>
                <div class="value">
                    +2
                </div>
            </div>
            <div class="info_profile">
                <div class="icon">
                    <img src="/assets/images/icons/envelope.png" />
                </div>
                <div class="value">
                    3
                </div>
            </div>
            
            <div id="detail">
                
                <div>
                    <span class="profile_name" id="profile_name" title='Ir a tu perfil'>
                        <?php echo $user->getFullName(); ?> <br />                 
                        <?php echo anchor('/salir', '(salir)', "title='Salir de tu perfil' id='close_session'"); ?>
                    </span>
                    
                    <a href="/mi_perfil" title='Ir a tu perfil'><?php echo photo_tiny($user); ?></a>
                </div>  
            </div>
                
            <img src="<?php echo base_url(); ?>assets/images/icons/twitter.png" title="Twitter de Mumita" alt="Twitter" />
            <img src="<?php echo base_url(); ?>assets/images/icons/facebook.png" title="Facebook de Mumita" alt="Facebook" />
            <img src="<?php echo base_url(); ?>assets/images/icons/gplus.png" title="Google Plus de Mumita" alt="Google Plus" />
        </div>
             
    <?php endif; ?>
    </div>  <!-- .content -->
    <div id="menu">
        <div id="socialmedia_top">
            <ul class="menu">
                <li>
                <a href="<?php echo base_url('home'); ?>">
                    <?php echo load_image("icons/twitter_blue.png", $title="Twitter"); ?>
                </a>
                </li>
                <li>
                <a href="<?php echo base_url('home'); ?>">
                    <?php echo load_image("icons/facebook_blue.png", $title="Facebook"); ?>
                </a>
                </li>
                <li>
                <a href="<?php echo base_url('home'); ?>">
                    <?php echo load_image("icons/gplus_blue.png", $title="Google plus"); ?>
                </a>
                </li>
            </ul>
        </div>
        <?php  
            $logged_in = $this->session->userdata('logged_in');

            if (!$logged_in)
            {
                    $level = "publics"; 
            }
            else
            {
                    $level = "privates";
            }
            $this->load->view("$level/common/menu");
        ?>
    </div>
</div>  <!-- #header -->
