<?php $this->load->view('common/head_open'); ?>
   <title>Bienvenidos a Mumita!</title>
<?php $this->load->view('common/head_close', array("context" => "publics")); ?>

   No has podido activar el perfil para la direcci&oacute;n de correo <strong><?php echo $email; ?></strong>,
   el c&oacute;digo que has puesto no es correcto.

<?php $this->load->view('common/footer'); ?>
<?php $this->load->view('common/end_page'); ?>