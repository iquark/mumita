<?php $this->load->view('common/head_open'); ?>
   <title>Bienvenidos a Mumita!</title>
<?php $this->load->view('common/head_close', array("context" => "publics")); ?>

   No has podido activar el perfil, la direcci&oacute;n de correo <strong><?php echo $email; ?></strong>
   no la tenemos registrada, <?php echo anchor('registro', "puedes hacerlo si quieres", "title='&iexcl;registrate!'"); ?>.

<?php $this->load->view('common/footer'); ?>