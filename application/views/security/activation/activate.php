<?php $this->load->view('common/head_open'); ?>
   <title>Bienvenidos a Mumita!</title>
<?php $this->load->view('common/head_close', array("context" => "publics")); ?>

<?php echo $name; ?> acabas de activar tu perfil!<br />

<?php echo anchor('/acceder', "Accede a tu cuenta", "title='Accede a tu cuenta'"); ?> 
utilizando tu correo electr&oacute;nico: <?php echo $email; ?>.

<?php $this->load->view('common/footer'); ?>