<div class="blue_panel forgot">
    <h1>Escribe tu correo electr&oacute;nico</h1>
    
    <?php $errors = validation_errors(); 
            if(!empty($errors)): ?>
        <div class="error"><?php echo $errors; ?></div>
    <?php endif; ?>
        
   	<?php if(isSet($forgot_error)): ?>
         <?php echo "<div class=\"error\">$forgot_error</div>"; ?>
      <?php endif; ?>

    <?php echo form_open('contrasena_olvidada'); ?>
        <div class="field">
            <label for="email">Correo electr&oacute;nico:</label>
            <input type="text" id="email" name="email" tabindex="1" value="<?php echo set_value('email'); ?>" />
        </div>
       <input type="submit" name="login" value="Enviar" />
    </form>
</div>