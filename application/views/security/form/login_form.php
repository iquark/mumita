<div class="blue_panel">
    <h1>Accede a Mumita</h1>
    
    <?php $errors = validation_errors(); 
            if(!empty($errors)): ?>
        <div class="error"><?php echo $errors; ?></div>
    <?php endif; ?>
        
    <?php if(isSet($login_error)): ?>
        <?php echo "<div class=\"error\">$login_error</div>"; ?>
    <?php endif; ?>
     
    <?php echo form_open('comprobar'); ?>
        <div>
            <label for="username">Usuario:</label>
            <input type="text" id="username" name="_username" tabindex="1" value="<?php echo set_value('_username'); ?>" />
        </div>
        <div>
            <label for="password">Contrase&ntilde;a:</label>
            <input type="password" id="password" tabindex="2" name="_password" />
        </div>
       <?php if(isSet($target_path)): ?>
           <input type="hidden" name="_target_path" value="<?php echo $target_path; ?>" />
       <?php endif; ?>
   
        <div>
            <input type="checkbox" id="remember_me" tabindex="3" name="_remember_me" checked />
            <label for="remember_me">Mantener sesi&oacute;n</label>
        </div>
        <div>
            <input type="submit" name="login" value="Enviar" />
        </div>
        <div>
            <?php echo anchor("/contrasena_olvidada", "&iquest;Has olvidado la contrase&ntilde;a?"); ?>
        </div>
    </form>
</div>