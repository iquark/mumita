<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <title>Renovar el vestuario de tus hijos ahora será fácil y barato.</title>
      <meta name="title" content="Política de privacidad de Mumita" />
      <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/splash/css/privacy.css" type="text/css" media="screen, projection" />
      <meta http-equiv="Content-Type" content="text/html" />
      <meta name="keywords" content="mumita, privacidad" />
      <meta name="description" content="politica de privacidad de mumita" />
      <META NAME="Language" CONTENT="spanish" />
      <META HTTP-EQUIV="Content-Language" CONTENT="es-ES">
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-29809902-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

   </head>
   <body>
      <div id="header">
          <div id="head_content"><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/splash/img/logo_blue.png"/></a></div>
      </div>
         <div id="separator_bm"></div>
      <div id="wrapper">
         <div id="content">
             <h1>Pol&iacute;tica de Privacidad</h1>
            <p>
            En Mumita.com somos conscientes de la importancia de los datos personales, por eso, te explicamos a continuaci&oacute;n el tratamiento que reciben.
            </p>
            <h2>1. Titularidad</h2>
            <p>
            Mumita.com y los ficheros donde se almacenan los datos de los usuarios son propiedad de Omix Desarrollos SL.
            </p>
            <h2>2. Derechos de Acceso, Rectificaci&oacute;n, Cancelaci&oacute;n y Oposici&oacute;n:</h2>
            <p>Ante todo, recordarte que puedes ejercer en cualquier momento, los derechos de acceso, rectificaci&oacute;n, cancelaci&oacute;n y oposici&oacute;n de sus datos de car&aacute;cter personal mediante correo electronico a info@mumita.com o bien por correo, acompa&ntilde;ando siempre una fotocopia de su D.N.I. a:<br />
            Omix Desarrollos SL <br />
            Avda. Rep&uacute;blica Argentina, 12, 1&ordm; <br />
            46702 - Gandia, Valencia<br />
            </p>
            <h2>3. Medidas de seguridad:</h2>
            <p>
            En Mumita.com los ficheros que contienen informaci&oacute;n de caracter personal est&aacute;n securizados seg&uacute;n el Real Decreto 1720/2007, de 21 de diciembre, por el que se aprueba el Reglamento de desarrollo de la Ley Org&aacute;nica 15/1999, de 13 de diciembre, de protecci&oacute;n de datos de car&aacute;cter personal.
            </p>
            <h2>4. Para que utilizamos la informaci&oacute;n recogida:</h2>
            <p>
            Gestionar los productos o servicios solicitados, pudiendo utilizar sus datos de contacto para solucionar o poner en su conocimiento posibles incidencias o informaci&oacute;n relevante para realizar dicha gesti&oacute;n.<br />
            Remitir informaci&oacute;n sobre otros productos o servicios propios o de terceros que pudieran ser de su inter&eacute;s, del sector del mobiliario, decoraci&oacute;n y complementos, por cualquier medio, incluido los electr&oacute;nicos, hasta el momento en que revoque su consentimiento, ejerza su derecho de cancelaci&oacute;n o de oposici&oacute;n, o manifieste su negativa a recibir dicha informaci&oacute;n.<br />
            Tramitar y gestionar el alta como usuario registrado y la publicaci&oacute;n de los comentarios en el blog.<br />
            Realizar informes estad&iacute;sticos an&oacute;nimos respecto a los h&aacute;bitos de acceso y la actividad desarrollada por los usuarios en la Web.<br />
            En el caso de que nos autorices, para el envio de comunicaciones comerciales.<br />
            </p>
            <h2>5. Envio de comunicaciones comerciales:</h2>
            <p>
            &Uacute;nicamente enviamos comunicaciones comerciales a aquellos usuarios que han manifestado expresamente su deseo de recibirlos. En cualquier caso, en todo momento puedes solicitar que ya no deseas recibir m&aacute;s mensajes. As&iacute; mismo, si no los recibes, en cualquier momento puedes solicitar que te los enviemos.
            </p>
            <h2>6. Uso de Cookies y Web Beacons:</h2>
            <p>En Mumita.com necesitamos utilizar las cookies para poder identificarte y saber los productos que has a&ntilde;adido al carrito, recordar quien eres cuando te identificas y mostrarte los &uacute;ltimos productos que has visualizado. Adem&aacute;s utilizamos las cookies para realizar perfiles de navegaci&oacute;n an&oacute;nimos, bien mediante medios propios o mediante los de terceros como Google Analytics. Los Web Beacons pueden ser utilizados ocasionalmente para la recogida de datos de navegaci&oacute;n de forma an&oacute;nima.
            </p>
            <h2>7. Y ante todo, ten la tranquilidad de que tus datos no ser&aacute;n comunicados a ning&uacute;n tercero, salvo que:</h2>
            <p>
            La cesi&oacute;n est&eacute; autorizada en una ley.<br />
            El tratamiento responda a la libre y leg&iacute;tima aceptaci&oacute;n de una relaci&oacute;n jur&iacute;dica cuyo desarrollo, cumplimiento y control implique necesariamente la conexi&oacute;n de dicho tratamiento con ficheros de terceros, como por ejemplo, entidades bancarias para la facturaci&oacute;n de los productos o servicios contratados o empresas de mensajer&iacute;a para el env&iacute;o de los productos contratados.<br />
            Los datos sean solicitados por el Defensor del Pueblo, las fuerzas y cuerpos policiales, el Ministerio Fiscal, un juez o un tribunal, en el ejercicio de las funciones que tienen atribuidas.
            </p>
            <h2>8. Para m&aacute;s informaci&oacute;n:</h2>
            <p>
            M&aacute;s informaci&oacute;n sobre las Cookies en: <a href="http://es.wikipedia.org/wiki/Cookie" target="_blank">http://es.wikipedia.org/wiki/Cookie</a><br />
            M&aacute;s informaci&oacute;n sobre los web beacons en: <a href="http://es.wikipedia.org/wiki/Web_bug" target="_self">http://es.wikipedia.org/wiki/Web_bug</a><br />
            Pol&iacute;tica de privacidad de Google Analytics: <a href="http://www.google.com/intl/es_ALL/privacypolicy.html" target="_self">http://www.google.com/intl/es_ALL/privacypolicy.html</a>
            </p>
         </div>
      </div>
         <div id="separator"></div>
      <div id="footer">
      Mumita &copy; 2012. Todos los derechos reservados
         <div class="right">
             <a href="mailto:info@mumita.com" title="Contacta con nosotros">info@mumita.com</a> | 
            <?php echo anchor (base_url()."privacidad","Privacidad",""); ?>
             | <a target="_blank" href="http://www.facebook.es/mumitamegusta">Facebook</a>
|
<a target="_blank" href="https://twitter.com/#!/mumita_es">Twitter</a>
         </div>
      </div>
       

   </body>
</html>    
