<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <title>Renovar el vestuario de tus hijos ahora será fácil y barato.</title>
      <meta name="title" content="Renovar el vestuario de tus hijos ahora será fácil y barato." />
      <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/splash/css/styles.css" type="text/css" media="screen, projection" />
      <!--[if lt IE 9]>
         <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/splash/css/ie.css" />
      <![endif]-->
      <meta http-equiv="Content-Type" content="text/html" />
      <meta name="keywords" content="ropa de bebe, ropa de niños, ropa infantil, intercambio, renovar vestuario,ropa bebe, segunda mano, compra venta, ropa, bebes, infantil, niños, ropa online" />
      <meta name="description" content="Compra y venta de ropa infantil y de bebé de segunda mano." />
      <META NAME="Language" CONTENT="spanish" />
      <META HTTP-EQUIV="Content-Language" CONTENT="es-ES">
   </head>
   <body>
      <div id="header">
         <div id="separator"><h1>&iquest;Vas a ser mam&aacute; o tienes<br /> hijos peque&ntilde;os?<br /><br />&iquest;Quieres ahorrar renovando<br />la ropa que les ha quedado peque&ntilde;a?</h1></div>
      </div>
      <div id="wrapper">
         <div id="content">
        <?php if ( !isSet($register_success) ): ?>
            <p>Introduce tu nombre y correo electr&oacute;nico y te avisaremos en cuanto nos pongamos en marcha &iexcl;Falta muy poco!</p>
            <?php echo form_open('enviar'); ?>
                <?php echo validation_errors(); ?>
               <label for="name">Nombre: </label>
               <input type="text" id="name" name="name" tabindex="1" /><br />
               
               <label for="email">Correo electr&oacute;nico: </label>
               <input type="text" id="email" name="email" tabindex="2" />
               
               <input type="submit" id="submit" name="submit" tabindex="3" value="&iexcl;Ap&uacute;ntate!" />
            </form>
        <?php else: ?>
            <p>Much&iacute;simas gracias por registrarte, te avisaremos de novedades ;).</p>
        <?php endif; ?>
         </div>
      </div>
      <div id="footer">
      Mumita &copy; 2012. Todos los derechos reservados
         <div class="right">
            <a href="mailto:info@mumita.com" title="Contacta con nosotros">info@mumita.com</a> | 
            <?php echo anchor (base_url()."privacidad","Privacidad",""); ?>
            | <a target="_blank" href="http://www.facebook.es/mumitamegusta">Facebook</a>
            |
            <a target="_blank" href="https://twitter.com/#!/mumita_es">Twitter</a>
         </div>
      </div>
       
       <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-29762148-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

       </script>
   </body>
</html>    
