<?php $this->load->view('common/head_open'); ?>
   <title>&iexcl;Te has perdido por Mumita!</title>
   <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/error.css" type="text/css" media="screen, projection" />
<?php $this->load->view('common/head_close', array("context" => "publics")); ?>
	<div class="blue_panel">
            <h1>&iexcl;Ups! &iexcl;HA HABIDO UN ERROR!</h1>
            <p>
                La página que buscas no existe.
            </p>
            <p>
                Pincha <?php echo anchor('panel',"aquí","title='Vuelve a la pagina inicial'"); ?>
                y te llevaremos a la página principal de MUMITA.
            </p>
	</div>
<?php $this->load->view('publics/common/footer'); ?>