<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(dirname(__FILE__) . '/../libraries/entities/Box_entity.php');
require_once(dirname(__FILE__) . '/../libraries/entities/Cloth_entity.php');

class Box_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function get_all($num=null, $offset=null)
    {
        $boxes = array();
       
        $this->db->join("box_clothes as clothes", "clothes.id_box=boxes.id", "left");
        $query = $this->db->get('boxes',$num,$offset);
        
        if(!empty($query))
        {
            $boxes = $this->to_boxes($query->result());
            $query->free_result();
        }
        
        return $boxes;
    }
    
    public function get_all_clothes($num=null, $offset=null)
    {
        $boxes = array();
       
        $this->db->join("box_clothes as clothes", "clothes.id_box=boxes.id", "inner");
        $query = $this->db->get('boxes',$num,$offset);
        
        if(!empty($query))
        {
            $boxes = $this->to_boxes($query->result());
            $query->free_result();
        }
        
        return $boxes;
    }

    public function find_by($field, $value) 
    {
        $boxes = array();
       
        $this->db->join("box_clothes as clothes", "clothes.id_box=boxes.id", "left");
        $query = $this->db->get_where('boxes', array($field=>$value));
        
        if(!empty($query))
        {
            $boxes = $this->to_boxes($query->result());
            $query->free_result();
        }
        
        return $boxes;
    }
    
    public function create($box)
    {
        // Insert the box data
        $res = $this->db->insert('boxes', $box);
        $box_id = $this->db->insert_id();
        
        return $box_id;
    }
    
    private function to_boxes($result)
    {
        $boxes = array();
        
        foreach ($result as $box)
        {
            array_push($boxes, $this->to_box($box));
        }
        
        return $boxes;
    }
    
    public function to_box($row) 
    {
        $id = $row->id;
        $abstract = $row->abstract;
        $category = $row->category;
        $description = $row->description;
        $id_age = $row->id_age;
        $sex = $row->sex;
        $user_id = $row->id_user;
        $created_at = $row->created_at;
        $status = $row->status;
        $valoration = $row->valoration;
        
        switch($category)
        {
            case 0:
                $id_size = $row->id_size;
                $colour1 = $row->colour1;
                $colour2 = $row->colour2;
                $colour3 = $row->colour3;
                $season = $row->season;

                $box = new Cloth_entity($id, $abstract, $category, $description, 
                                        $id_age, $sex, $user_id, $created_at, 
                                        $status, $valoration, $id_size, 
                                        $colour1, $colour2, $colour3, $season);
                break;
            default:
                $box = new Box_entity($id, $abstract, $category, $description, 
                                      $id_age, $sex, $user_id, $created_at, $status, 
                                      $valoration);
                break;
        }
        
        return $box;
    }
    
    public function delete($box_id) 
    {
        $this->db->delete('boxes', array('id'=>$box_id));
        
        $error_number = $this->db->_error_number();
        $deleted = empty($error_number);
        
        return $deleted;
    }

    function update($box)
    {
        $updated = false;
        
        if (!empty($box))
        {
            $box_info = $box->getToUpdate();
            $this->db->update('boxes', $box_info, array('id'=>$box->getId()) );
            $error_number = $this->db->_error_number();
            $updated = empty($error_number);
        }
        
        return $updated;
    }
    
    public function comment($id_box, $id_user, $comment)
    {
        $this->db->insert('box_comments', array(
                    'id_box' => $id_box,
                    'id_user' => $id_user,
                    'comment' => $comment
        ));
        
        $id_comment = $this->db->insert_id();
        
        return $id_comment;
    }
    
    public function add_valoration($box_id, $user_id, $valoration)
    {
        $query = $this->db->get_where('box_valoration', array('id_box'=>$box_id, 'id_user'=>$user_id));
        $result = $query->result();
        
        if (!empty($result))
        {
            $this->db->update('box_valoration', 
                                    array(  'valoration'=>$valoration), 
                                    array(  'id_box' =>$box_id,
                                            'id_user' => $user_id));
        }
        else
        {
            $this->db->insert('box_valoration', 
                                    array(
                                        'id_box' => $box_id,
                                        'id_user' => $user_id,
                                        'valoration' => $valoration));
        }
        
        $query = $this->db->get_where('box_valoration', array('id_box'=>$box_id));
        
        $values = array();
        foreach($query->result() as $valoration)
        {
            array_push($values, $valoration->valoration);
        }
        
        $median = 1;
        
        if(!empty($values))
        {
            $this->load->library("common");
            $median = $this->common->median($values);
        }
        
        $this->db->update('boxes', array('valoration'=>$median), array('id' =>$box_id));
        
        return $median;
    }
    
}