<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(dirname(__FILE__) . '/../libraries/entities/BoxComment_entity.php');
require_once(dirname(__FILE__) . '/../libraries/entities/User_entity.php');

class Boxcomment_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function get_all()
    {
        $comments = array();
       
        $query = $this->db->get('box_comments');
        
        if(!empty($query))
        {
            $comments = $this->to_comments($query->result());
            $query->free_result();
        }
        
        return $comments;
    }

    public function find_by($field, $value) 
    {
        $query = $this->db->get_where('box_comments', array($field=>$value));
        $comments = $this->to_comments($query->result());
        
        return $comments;
    }
    
    public function create($id_box, $id_user, $comment)
    {
        $this->db->insert('box_comments', array(
                    'id_box' => $id_box,
                    'id_user' => $id_user,
                    'comment' => $comment
        ));
        
        $num_comment = $this->db->insert_id();
        
        return $num_comment;
    }
    
    private function to_comments($result)
    {
        $comments = array();
        
        foreach ($result as $comment)
        {
            array_push($comments, $this->to_comment($comment));
        }
        
        return $comments;
    }
    
    public function to_comment($row) 
    {
        $id = $row->id;
        $id_box = $row->id_box;
        $id_user = $row->id_user;
        $comment = $row->comment;
        $created_at = $row->date_creation;

        $comment = new BoxComment_entity($id, $id_box, $id_user, $comment, $created_at);

        return $comment;
    }
    
    public function delete($id) 
    {
        $this->db->delete('box_comments', array('id'=>$id));
        
        $error_number = $this->db->_error_number();
        $deleted = empty($error_number);
        
        return $deleted;
    }

    function update($comment)
    {
        $updated = false;
        
        if (!empty($box))
        {
            $comment_info = $comment->getToUpdate();
            $this->db->update('box_comments', $comment_info, array('id'=>$comment->getId()) );
            $error_number = $this->db->_error_number();
            $updated = empty($error_number);
        }
        
        return $updated;
    }

}