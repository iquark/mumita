<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(dirname(__FILE__) . '/../libraries/entities/Timeline_entity.php');

class Timeline_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function getAll()
    {
        $query = $this->db->get('timelines');

        $timelines = $this->toActions($query->result());
        $query->free_result();

        return $timelines;
    }

    public function findBy($field, $value) 
    {
        $query = $this->db->get_where('timelines', array($field=>$value));

        $timelines = $this->toTimelines($query->result());
        $query->free_result();

        return $timelines;
    }

    /**
     *  toTimelines: converts a query result from "timelines" in an array of
     *   entities of Timeline
     * @param type $result
     * @param type $users must be an array wich indices are the users' id
     * @param type $box must be an array wich indices are the box' id
     * @return array 
     */
    private function toTimelines($result, $users = null, $boxes = null)
    {
        $timelines = array();
        
        foreach ($result as $timeline)
        {
            $id = $timeline->id;
            $id_action = $timeline->id_action;
            $id_user1 = $timeline->id_user1;
            $id_box = $timeline->id_box;
            $id_user2 = $timeline->id_user2;
            $date = $timeline->date;
            $value = $timeline->value;
            $id_comment = $timeline->id_comment;

            $t = new Timeline_entity($id, $id_action, $id_user1, $id_user2, 
                                     $id_box, $value, $date, $id_comment);
            
            if (isSet($users[$id_user1]))
            {
                $t->setUser1($users[$id_user1]);
            }
            if (isSet($users[$id_user2]))
            {
                $t->setUser2($users[$id_user2]);
            }
            if (isSet($boxes[$id_box]))
            {
                $t->setBox($boxes[$id_box]);
            }
            
            array_push($timelines, $t);
        }
        
        return $timelines;
    }
    
    public function delete($timeline) 
    {
        $deleted = false;
        $timeline_id = $timeline->getId();
        
        if (!empty($timeline_id))
        {
            $this->db->delete('timeline', array('id'=>$timeline_id));
            $deleted = true;
        }
        
        return $deleted;
    }
    
    
    public function update($timeline)
    {
        $updated = false;
        
        if (!empty($timeline))
        {
            $this->db->update("timelines", $timeline->getToUpdate(), array("id"=>$timeline->getId()));
            $error_number = $this->db->_error_number();
            $updated = empty($error_number);
        }
        
        return $updated;
    }
    
    public function insert($timeline)
    {
        if (!empty($timeline))
        {
            $this->db->insert("timelines", $timeline);
            $timeline_id = $this->db->insert_id();
        }
        else
        {
            $timeline_id = null;
        }

        return $timeline_id;
    }
    
    public function get_your_info($user_id)
    {        
        $this->db->where("id_user1", $user_id);
        $this->db->or_where("id_user2", $user_id);
        $this->db->order_by("id", "desc"); 
        $query = $this->db->get('timelines');
        $actions = $this->toTimelines($query->result());
        $query->free_result();
       
        return $actions;
    }
    
    public function get_friends_info($user_id)
    {
        $people = $this->user->get_following($user_id);
        $people_id = array();
        $people_by_id = array();
        $actions = array();
        
        foreach($people as $person)
        {
            array_push($people_id, $person->getId());
            $people_by_id[$person->getId()] = $person;
        }
        
        if (!empty($people_id))
        {
            $this->db->where_in('id_user1', $people_id);
            $this->db->order_by("id", "desc"); 
            $query = $this->db->get('timelines');

            $actions = $this->toTimelines($query->result(), $people_by_id);
            $query->free_result();
        }
        
        return $actions;
    }
}