<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(dirname(__FILE__) . '/../libraries/entities/User_entity.php');

class User_model extends CI_Model {

    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function get_all($also_admins = FALSE, $num=null, $offset=null)
    {
        $our_id = $this->session->userdata('id');
        $users = array();
        
        if (!empty($our_id))
        {
            $original_reserved = $this->db->_reserved_identifiers;
            $this->db->_reserved_identifiers[] = $our_id;
            $this->db->select("users.*, follow.id_followed as following");
            $this->db->join('user_follow follow', "follow.id_follower=$our_id and users.id=follow.id_followed", 'left');
            $this->db->_reserved_identifiers = $original_reserved;
        }
        
        if (!$also_admins)
        {
            $this->db->where('role_level', 0);
        }

        $query = $this->db->get('users',$num,$offset);
        
        if(!empty($query))
        {
            $users = $this->to_users($query->result());
            $query->free_result();
        }
        
        return $users;
    }

    public function find_by($field, $value, $also_admins = FALSE) 
    {
        $our_id = $this->session->userdata('id');
        
        if (!empty($our_id))
        {
            $original_reserved = $this->db->_reserved_identifiers;
            $this->db->_reserved_identifiers[] = $our_id;
            $this->db->select("users.*, follow.id_followed as following");
            $this->db->join('user_follow follow', "follow.id_follower=$our_id and users.id=follow.id_followed", 'left');
            $this->db->_reserved_identifiers = $original_reserved;
        }
        
        if (!$also_admins)
        {
            $this->db->where('role_level', 0);
        }
        
        $query = $this->db->get_where('users', array($field=>$value));
        $users = $this->to_users($query->result());
        
        return $users;
    }

    public function check_user($username, $password)
    {
        $return = false;
        $query = $this->db->get_where('users', array('email'=>$username, 'password'=>$password));
        
        if($query->num_rows() == 1) 
        {
            $return = $this->to_user($query->row());
        }
        $query->free_result();
        
        return $return;
    }

    public function register($user)
    {
        // search if there's an user with this email
        $user_result = $this->find_by("email", $user['email']);

        if (empty($user_result))
        {
            $user["salt"] = sha1($user['name'].$user['lastname']);
            $user["activation_code"] = sha1($user['name'].$user['lastname'].$user["salt"]);
            $this->db->insert("users", $user);
            $user_id = $this->db->insert_id();
        }
        else
        {
            $user_id = null;
        }

        return $user_id;
    }

    public function search($string, $sex, $size, $season) 
    {
        $our_id = $this->session->userdata('id');
        if (!empty($our_id))
        {
            $original_reserved = $this->db->_reserved_identifiers;
            $this->db->_reserved_identifiers[] = $our_id;
            $this->db->select("users.*, follow.id_followed as following");
            $this->db->join('user_follow follow', "follow.id_follower=$our_id and users.id=follow.id_followed", 'left');
            $this->db->_reserved_identifiers = $original_reserved;
        }
        if (!empty($string))
        {
            $this->db->like('users.name', $string);
            $this->db->or_like('users.lastname', $string); 
        }
        
        if (!empty($sex) || !empty($size) || !empty($season))
        {
            $params_boxes = "";
            $params_clothes = "";
            
            if (!empty($sex) && is_numeric($sex) && $sex >-1)
            {
                $params_boxes = "AND boxes.sex=$sex";
            }
            if (!empty($size) && is_numeric($size) && $size >0)
            {
                $params_clothes = "AND boxes.id_size=$size";
            }
            if (!empty($season) && is_numeric($season) && $season >-1)
            {
                $params_clothes .= "AND boxes.season=$season";
            }
            
            $this->db->join('boxes', "boxes.id_user=users.id $params_boxes", 'inner');
            $this->db->join('box_clothes', "box_clothes.id_box=boxes.id $params_clothes", 'inner');
        }
        
        $this->db->where('role_level', '0');
        
        $this->db->from('users');
        $this->db->distinct();
        $query = $this->db->get();
        $users = $this->to_users($query->result());
        
        $query->free_result();

        return $users;
    }
    
    private function to_users($result)
    {
        $users = array();
        
        foreach ($result as $user)
        {
            array_push($users, $this->to_user($user));
        }
        
        return $users;
    }
    
    public function to_user($row) 
    {
        $id = $row->id;
        $name = $row->name;
        $lastname = $row->lastname;
        $email = $row->email;
        $password = $row->password;
        $sex = $row->sex;
        $role_level = $row->role_level;
        $bio = $row->bio;
        $salt = $row->salt;
        $birthday = $row->birthday;
        $active = $row->active;
        $shipping_address_id = $row->shipping_address_id;
        $billing_address_id = $row->billing_address_id;
        $pickup_address_id = $row->pickup_address_id;
        $created_at = $row->created_at;
        $activation_code = $row->activation_code;
        $following = (!empty($row->following))?$row->following:null;

        $user = new User_entity($id, $name, $lastname, $email, $password, $sex, 
                                $role_level, $bio, $salt, $active, $birthday, $shipping_address_id, 
                                $billing_address_id, $pickup_address_id, $created_at, $activation_code, $following);
        
        return $user;
    }
    
    public function delete($user) 
    {
        $this->load->model("address_model");
        $deleted = false;
        $our_id = $this->session->userdata('id');
        $user_id = $user->getId();
        $shipping_address_id = $user->getShippingAddressId();
        $billing_address_id = $user->getShippingAddressId();
        $pickup_address_id = $user->getShippingAddressId();
        
        if (!empty($user_id) && $user_id!=$our_id)
        {
            // we have to delete addresses if nobody is using them
            $addresses = array();
            
            if (!empty($shipping_address_id)) 
            {
                array_push($addresses, $user->getShippingAddress());
            }
            if (!empty($billing_address_id)) 
            {
                array_push($addresses, $user->getBillingAddress());
            }
            if (!empty($pickup_address_id)) 
            {
                array_push($addresses, $user->getPickupAddress());
            }
            
            $this->db->delete('users', array('id'=>$user_id));
            
            foreach($addresses as $address)
            {
                $this->db->where('shipping_address_id', $address->getId());
                $this->db->or_where('billing_address_id', $address->getId());
                $this->db->or_where('pickup_address_id', $address->getId());
                $query = $this->db->get('users');
                if($query->num_rows()==0)
                {
                    $this->address_model->delete($address);
                }
                $query->free_result();
            }
            
            
            $deleted = true;
        }

        return $deleted;
    }

    public function update($user=null)
    {
        $updated = false;
        
        if (!empty($user))
        {
            $this->db->update("users", $user->getToUpdate(), array("id"=>$user->getId()));
            $error_number = $this->db->_error_number();
            $updated = empty($error_number);
        }
        
        return $updated;
    }
    
    public function follow($user_follower_id=null, $user_followed_id=null)
    {
        $following = false; 
        
        if (!empty($user_follower_id) && !empty($user_followed_id))
        {
            $this->db->insert("user_follow", array("id_follower"=>$user_follower_id,
                                                   "id_followed"=>$user_followed_id));
            $following = true;
        }
        
        return $following;
    }
   
    public function check_following($user_follower_id=null, $user_followed_id=null)
    {
        $following = false; 
        
        if (!empty($user_follower_id) && !empty($user_followed_id))
        {
            $query = $this->db->get_where("user_follow", array("id_follower"=>$user_follower_id,
                                                               "id_followed"=>$user_followed_id));
            if($query->num_rows()!=0)
            {
                $following = true;
            }
            $query->free_result();
        }
        
        return $following;
    }
    
    public function unfollow($user_follower_id=null, $user_followed_id=null)
    {
        $following = false; 
        
        if (!empty($user_follower_id) && !empty($user_followed_id))
        {
            $this->db->delete("user_follow", array("id_follower"=>$user_follower_id,
                                                   "id_followed"=>$user_followed_id));
            $following = true;
        }
        
        return $following;
    }
    
    public function activate($email, $code)
    {
        $return = 0;
        
        $users = $this->find_by("email", $email,true);
        if(empty($users))
        {
            $return = 2;
        }
        else 
        {
            $user = $users[0];
            
            if ($user->getActivationCode()==$code)
            {
                $this->db->update("users", array("active"=>1), array("id"=>$user->getId()));
                $return = 1;
            }
        }
        
        return $return;
    }
    
    public function get_following($user_id)
    {
        $this->db->join("users", "users.id=user_follow.id_followed", "inner");
        $query = $this->db->get_where('user_follow', array('id_follower'=>$user_id));
        $users = $this->to_users($query->result());
        
        $query->free_result();
        
        return $users;
        
    }
}