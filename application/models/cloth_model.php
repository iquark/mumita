<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(dirname(__FILE__) . '/../libraries/entities/Cloth_entity.php');

class Cloth_model extends Box_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function get_all($num=null, $offset=null)
    {
        $boxes = array();
       
        $this->db->join("box_clothes as clothes", "clothes.id_box=boxes.id", "left");
        $query = $this->db->get('boxes',$num,$offset);
        
        if(!empty($query))
        {
            $boxes = $this->to_boxes($query->result());
            $query->free_result();
        }
        
        return $boxes;
    }
    
    public function get_all_clothes($num=null, $offset=null)
    {
        $boxes = array();
       
        $this->db->join("box_clothes as clothes", "clothes.id_box=boxes.id", "inner");
        $query = $this->db->get('boxes',$num,$offset);
        
        if(!empty($query))
        {
            $boxes = $this->to_boxes($query->result());
            $query->free_result();
        }
        
        return $boxes;
    }

    public function find_by($field, $value) 
    {
        $this->db->join("box_clothes as clothes", "clothes.id_box=boxes.id", "left");
        $query = $this->db->get_where('boxes', array($field=>$value));
        $boxes = $this->to_boxes($query->result());
        
        return $boxes;
    }

    public function create($box_info)
    {
        $box = $box_info['box'];
        
        $this->db->trans_start();
            $box_id = parent::create($box);

            unset($box_info['box']);
            $box_info['id_box'] = $box_id;
            $this->db->insert('box_clothes', $box_info);
        $this->db->trans_complete();
        
        if ($this->db->trans_status() === FALSE)
        {
            $box_id = null;
        }
        
        return $box_id;
    }

    public function search($string = null, $sex = -1, $size = -1, $season = -1, $user_id = null)
    {
        $boxes = array();
        
        $params_join = "";
        
        if (!empty($size) && $size!=-1 && is_numeric($size))
        {
            $params_join = "AND cloth.id_size=$size ";
        }
        
        if (!empty($season) && $season!=-1 && is_numeric($season))
        {
            $params_join .= "AND cloth.season=$season ";
        }
        
        $this->db->join('box_clothes cloth', "cloth.id_box=boxes.id $params_join", 'inner');

        if (!empty($string))
        {
            $this->db->like('boxes.abstract', $string);
            $this->db->or_like('boxes.description', $string); 
        }
        
        if (!empty($sex) && $sex != -1 && is_numeric($sex))
        {
            $this->db->where("boxes.sex", $sex);
        }
        
        if (!empty($user_id))
        {
            $this->db->where("boxes.id_user", $user_id);
        }
        
        $this->db->from('boxes');
        $query = $this->db->get();

        $boxes = $this->to_boxes($query->result());
        
        $query->free_result();
        
        return $boxes;
    }
    
    private function to_boxes($result)
    {
        $boxes = array();
        
        foreach ($result as $box)
        {
            array_push($boxes, $this->to_box($box));
        }
        
        return $boxes;
    }
    
    public function to_box($row) 
    {
        $id = $row->id;
        $abstract = $row->abstract;
        $category = $row->category;
        $description = $row->description;
        $id_age = $row->id_age;
        $sex = $row->sex;
        $user_id = $row->id_user;
        $created_at = $row->created_at;
        $status = $row->status;
        $valoration = $row->valoration;
        
        $id_size = $row->id_size;
        $colour1 = $row->colour1;
        $colour2 = $row->colour2;
        $colour3 = $row->colour3;
        $season = $row->season;

        $box = new Cloth_entity($id, $abstract, $category, $description, 
                                $id_age, $sex, $user_id, $created_at, $status, 
                                $valoration,  $id_size, 
                                $colour1, $colour2, $colour3, $season);
        
        return $box;
    }
    
    public function delete($box_id) 
    {
        $this->db->trans_start();
            $this->db->delete('box_clothes', array('id_box'=>$box_id));
            $this->db->delete('boxes', array('id'=>$box_id));
            $error_number = $this->db->_error_number();
            $deleted = empty($error_number);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $deleted = null;
        }
        
       return $deleted; 
    }

    function update($box)
    {
        $box_info = $box->getToUpdate();
        $data = $box_info['box'];
        $updated = false;
        
        if (!empty($box))
        {
            $this->db->trans_start();
                if(parent::update($box->getParentCopy()))
                {
                    unset($box_info['box']);
                    $this->db->update('box_clothes', $box_info, array('id_box'=>$box->getId()) );
                    $error_number = $this->db->_error_number();
                    $updated = empty($error_number);
                }
            $this->db->trans_complete();
            
            if ($this->db->trans_status() === FALSE)
            {
                $updated = null;
            }
        }
        
        return $updated;
    }
    
}