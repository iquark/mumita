<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(dirname(__FILE__) . '/../libraries/entities/Order_entity.php');

class Order_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function get_all($num=null, $offset=null)
    {
        $orders = array();
       
        $query = $this->db->get('order',$num,$offset);
        
        if(!empty($query))
        {
            $orders = $this->to_orders($query->result());
            $query->free_result();
        }
        
        return $orders;
    }
    
    public function find_by($field, $value) 
    {
        $orders = array();
       
        $query = $this->db->get_where('orders', array($field=>$value));
        
        if(!empty($query))
        {
            $orders = $this->to_orders($query->result());
            $query->free_result();
        }
        
        return $orders;
    }
    
    public function create($order)
    {
        // Insert the box data
        $res = $this->db->insert('orders', $order);
        $order_id = $this->db->insert_id();
        
        return $order_id;
    }
    
    private function to_orders($result)
    {
        $orders = array();
        
        foreach ($result as $order)
        {
            array_push($orders, $this->to_order($order));
        }
        
        return $orders;
    }
    
    public function to_order($row) 
    {
        $id = $row->id;
        $id_box = $row->id_box;
        $id_user = $row->id_user;
        $date = $row->date;
        $request_number = $row->request_number;
        $delivery_number = $row->delivery_number;
        $URL = $row->URL;
        
        $order = new Order_entity($id, $id_box, $id_user, $date, $request_number,
                                  $delivery_number, $URL);
        
        return $order;
    }
    
    public function delete($order_id) 
    {
        $this->db->delete('orders', array('id'=>$order_id));
        
        $error_number = $this->db->_error_number();
        $deleted = empty($error_number);
        
        return $deleted;
    }

    function update($order)
    {
        $updated = false;
        
        if (!empty($order))
        {
            $order_info = $box->getToUpdate();
            $this->db->update('orders', $order_info, array('id'=>$order->getId()) );
            $error_number = $this->db->_error_number();
            $updated = empty($error_number);
        }
        
        return $updated;
    }
    
}