<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    public function get_all()
    {
        $customers = array();
        $query = $this->db->get('customers');
        
        if(!empty($query))
        {
            $customers = $this->to_customers($query->result());
            $query->free_result();
        }
        
        return $customers;
    }
    
    public function find_by($field, $value) 
    {
        $query = $this->db->get_where('customers', array($field=>$value));
        $customers = $this->to_customers($query->result());
        
        $query->free_result();
        
        return $customers;
    }
    
    public function store($customer)
    {
        // search if there's an customer with this email
        $customer_result = $this->find_by("email", $customer['email']);

        if (empty($customer_result))
        {
            $res = $this->db->insert("customers", $customer);
            $customer_id = $this->db->insert_id();
        }
        else
        {
            $customer_id = null;
        }

        return $customer_id;
    }
    
    
       
    private function to_customers($result)
    {
        $customers = array();
        
        foreach ($result as $customer)
        {
            array_push($customers, $this->to_customer($customer));
        }
        
        return $customers;
    }
    
    public function to_customer($row) 
    {
        $id = $row->id;
        $name = $row->name;
        $email = $row->email;
        $ip = $row->ip;
        $created_at = $row->created_at;

        $customer = new Customer_entity($id, $name, $email, $ip, $created_at);
        
        return $customer;
    }
    
}
