<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(dirname(__FILE__) . '/../libraries/entities/Address_entity.php');

class Address_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function getAll()
    {
        $query = $this->db->get('address');

        $adresses = $this->toAddresses($query->result());
        $query->free_result();

        return $adresses;
    }

    public function findBy($field, $value) 
    {
        $query = $this->db->get_where('address', array($field=>$value));

        $adresses = $this->toAddresses($query->result());
        $query->free_result();

        return $adresses;
    }

    private function toAddresses($result)
    {
        $adresses = array();
        
        foreach ($result as $address)
        {
            $id = $address->id;
            $street = $address->street;
            $city = $address->city;
            $province = $address->province;
            $country = $address->country;
            $postal_code = $address->postal_code;

            array_push($adresses, new Address_entity($id, $street, $city, $province, $country, $postal_code));
        }
        
        return $adresses;
    }
    
    public function delete($address) 
    {
        $deleted = false;
        $address_id = $address->getId();
        
        if (!empty($address_id))
        {
            $this->db->delete('address', array('id'=>$address_id));
            $deleted = true;
        }
        
        return $deleted;
    }
    
    
    function update($address)
    {
        $updated = false;
        
        if (!empty($address))
        {
            $this->db->update("address", $address->getToUpdate(), array("id"=>$address->getId()));
            $error_number = $this->db->_error_number();
            $updated = empty($error_number);
        }
        
        return $updated;
    }
    
    function insert($address)
    {
        if (!empty($address))
        {
            $res = $this->db->insert("address", $address->getToInsert());
            $address_id = $this->db->insert_id();
        }
        else
        {
            $address_id = null;
        }

        return $address_id;
    }
}