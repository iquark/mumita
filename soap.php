<?php
error_reporting(E_ALL);

function envio() 
{
    echo "conectando...<br />";

    $wsdl = "http://sagec-test.mrw.es/MRWEnvio.asmx?wsdl";
    ini_set($wsdl, "0"); // disabling WSDL cache

    $client = new SoapClient($wsdl,array('encoding'=>'utf-8',
                                        'soap_version'=> SOAP_1_2, 
                                        'SOAPAction'=> "http://www.mrw.es/TransmEnvio"));

    if(empty($client)) {
        echo "<br />No ha ido bien :(";
    } else {  
        $functions = $client->__getFunctions ();
        var_dump ($functions);

        $headerbody = array('CodigoFranquicia' => "04318",
                            'CodigoAbonado' => "005521",
                            'UserName'=>"04318SAGECFUT",
                            'Password'=>"04318SAGECFUT",
                            'CodigoDepartamento' => "");

        $namespace = 'http://www.mrw.es/'; 
        $header = new SoapHeader($namespace, 'AuthInfo', $headerbody);  

        $client->__setSoapHeaders($header); 
        echo "<br /><strong>client:</strong> ";
        print "<pre>";
        var_dump($client);
        print "</pre>";

        // datos del ejemplo del pdf
        $parameters = array(

            'request' => array (
                'DatosEntrega' => array (
                    'Direccion' => array (
                    'CodigoTipoVia' => "CL",
                    'Via' => "PRUEBA",
                    'Numero' => "1",
                    'Resto' => "PRUEBA",
                    'CodigoPostal' =>"28004",
                    'Poblacion' =>"MADRID"
                    ),
                    'Nif' => "12345678Z",
                    'Nombre' => "PRUEBA INTEGRACION",
                    'Telefono' => "947001001",
                    'Contacto' => "Prueba",
                    'ALaAtencionDe' => "SERGIO PRUEBA INTEGRACION",
                    'Observaciones' => "PRUEBA DPTO FIELD SUPPORT"
                ),
                'DatosServicio' => array (
                    'Fecha' =>"28/10/2012",
                    'Referencia' => "PRUEBA INTEGRACION",
                    'CodigoServicio' =>"0800",
                    'NumeroSobre' => 1,
                    'Bultos' => array(
                        'Bulto' => array(
                            'Alto' =>10,
                            'Largo' =>20,
                            'Ancho' =>30,
                            'Dimension' =>40,
                            'Referencia' =>"AAA",
                            'Peso' =>500
                        ),
                    ),
                    'NumeroBultos' =>1,
                    'Peso' =>500,
                    'EntregaPartirDe' =>"",
                    'Gestion' =>"",
                    'Retorno'=>"",
                    'ConfirmacionInmediata' =>"",
                    'EntregaSabado' =>"",
                    'Entrega830' =>"",
                    'Reembolso' =>"O",
                    'ImporteReembolso' =>"13,40",
                    'TramoHorario' =>2
                ),
            )
        );

        try{ 
            // $return = $client->__soapCall('TransmEnvio', $parameters, array('uri'=> $namespace), $header);
            echo "llamada.. ";
            $return = $client->TransmEnvio($parameters);
            echo "llamado.. ";
            echo "<br /><strong>BODY:</strong> ";
            print "<pre>";
            var_dump($client);
            print "</pre>";
        } catch(SoapFault $fault) {
            // <xmp> tag displays xml output in html
            echo 'SOMETHING FAILED: Request : <br/><xmp>',
            $client->__getLastRequest(),
            '</xmp><br/><br/> Error Message : <br/>',
            $fault->getMessage();
        } 
        
        echo "<br /><strong>return:</strong> ";

        print "<pre>";
        var_dump($return);
        print "</pre>";
        echo "<br /><strong>estado: </strong> ".$return->TransmEnvioResult->Estado;

        echo "REQUEST:\n" . $client->__getLastRequest() . "\n";
    }
}
   
function etiqueta() 
{
    echo "conectando...<br />";

    $wsdl = "http://sagec-test.mrw.es/MRWEnvio.asmx?wsdl";
    ini_set($wsdl, "0"); // disabling WSDL cache

    $client = new SoapClient($wsdl,array('encoding'=>'utf-8',
                                        'soap_version'=> SOAP_1_2, 
                                        'SOAPAction'=> "http://www.mrw.es/GetEtiquetaEnvio"));

    if(empty($client)) {
        echo "<br />No ha ido bien :(";
    } else {  
        $functions = $client->__getFunctions ();
        var_dump ($functions);

        $headerbody = array('CodigoFranquicia' => "04318",
                            'CodigoAbonado' => "005521",
                            'UserName'=>"04318SAGECFUT",
                            'Password'=>"04318SAGECFUT",
                            'CodigoDepartamento' => "");

        $namespace = 'http://www.mrw.es/'; 
        $header = new SoapHeader($namespace, 'AuthInfo', $headerbody);  

        $client->__setSoapHeaders($header); 
        echo "<br /><strong>client:</strong> ";
        print "<pre>";
        var_dump($client);
        print "</pre>";
/*
 * SOAPAction: "http://www.mrw.es/GetEtiquetaEnvio"

<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Header>
    <AuthInfo xmlns="http://www.mrw.es/">
      <CodigoFranquicia>string</CodigoFranquicia>
      <CodigoAbonado>string</CodigoAbonado>
      <CodigoDepartamento>string</CodigoDepartamento>
      <UserName>string</UserName>
      <Password>string</Password>
    </AuthInfo>
  </soap:Header>
  <soap:Body>
    <GetEtiquetaEnvio xmlns="http://www.mrw.es/">
      <request>
        <NumeroEnvio>string</NumeroEnvio>
        <SeparadorNumerosEnvio>string</SeparadorNumerosEnvio>
        <FechaInicioEnvio>string</FechaInicioEnvio>
        <FechaFinEnvio>string</FechaFinEnvio>
        <TipoEtiquetaEnvio>string</TipoEtiquetaEnvio>
        <ReportTopMargin>int</ReportTopMargin>
        <ReportLeftMargin>int</ReportLeftMargin>
      </request>
    </GetEtiquetaEnvio>
  </soap:Body>
</soap:Envelope>
 */
        // datos del ejemplo del pdf
        $parameters = array(

            'request' => array (
                'NumeroEnvio' => "CL",
                'SeparadorNumerosEnvio' => "-",
                'FechaInicioEnvio' => "28/09/2012",
                'FechaFinEnvio' => "28/09/2012",
                'TipoEtiquetaEnvio' => "document",
                'ReportTopMargin' => 20,
                'ReportLeftMargin' => 10,
            )
        );

        try{ 
        // $return = $client->__soapCall('TransmEnvio', $parameters, array('uri'=> $namespace), $header);
            $return = $client->GetEtiquetaEnvio($parameters);
            echo "<br /><strong>BODY:</strong> ";
            print "<pre>";
            var_dump($client);
            print "</pre>";
            echo "<br /><strong>return:</strong> ";

            print "<pre>";
            var_dump($return);
            print "</pre>";
        }catch(SoapFault $fault){
            // <xmp> tag displays xml output in html
            echo 'SOMETHING FAILED: Request : <br/><xmp>',
            $client->__getLastRequest(),
            '</xmp><br/><br/> Error Message : <br/>',
            $fault->getMessage();
        } 
   // echo "<br /><strong>estado: </strong> ".$return->GetEtiquetaEnvioResult->Estado;
    
        echo "REQUEST:\n" . $client->__getLastRequest() . "\n";
    }
}
echo "Envio: <br />";
envio();
//echo "Etiqueta: <br />";
//etiqueta();
/*
echo " <br />PROBANDO DIRECTAMENTE CON LA LLAMADA POR XML: ";

$envelope = '<?xml version="1.0" encoding="utf-8"?>
<soap12:Envelope xmlns:soap12="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns8447="http://tempuri.org">
    <soap12:Header>
        <AuthInfo xmlns="http://www.mrw.es/">
            <CodigoFranquicia>04318</CodigoFranquicia>
            <CodigoAbonado>005521</CodigoAbonado>
         <CodigoDepartamento></CodigoDepartamento>
            <UserName>04318SAGECFUT</UserName>
            <Password>04318SAGECFUT</Password>
        </AuthInfo>
    </soap12:Header>
    <soap12:Body>
        <TransmEnvio xmlns="http://www.mrw.es/">
            <request>
                <DatosEntrega>
                    <Direccion>
                        <CodigoTipoVia>CL</CodigoTipoVia>
                        <Via>PRUEBA</Via>
                        <Numero>1</Numero>
                        <Resto>PRUEBA</Resto>
                        <CodigoPostal>28004</CodigoPostal>
                        <Poblacion>MADRID</Poblacion>
                    </Direccion>
                    <Nif>12345678Z</Nif>
                    <Nombre>PRUEBA INTEGRACION</Nombre>
                    <Telefono>123456789</Telefono>
                    <Contacto>PRUEBA </Contacto>
                    <ALaAtencionDe>SERGIO PRUEBA INTEGRACION</ALaAtencionDe>
                    <Observaciones>PRUEBA DPTO FIELD SUPPORT</Observaciones>
                </DatosEntrega> 
                <DatosRecogida>
                   <Direccion>
                     <CodigoTipoVia>CL</CodigoTipoVia>
                     <Via>PRUEBA</Via>
                     <Numero>1</Numero>
                     <Resto>PRUEBA</Resto>
                     <CodigoPostal>28004</CodigoPostal>
                     <Poblacion>MADRID</Poblacion>
                   </Direccion>
                     <Nif>12345678Z</Nif>
                     <Nombre>PRUEBA INTEGRACION</Nombre>
                     <Telefono>123456789</Telefono>
                     <Contacto>PRUEBA </Contacto>
                    <Observaciones>PRUEBA DPTO FIELD SUPPORT</Observaciones>
                 </DatosRecogida>
                <DatosServicio>
                    <Fecha>01/09/2012</Fecha>
                    <Referencia>PRUEBA INTEGRACION</Referencia>
                    <CodigoServicio>0800</CodigoServicio>
                    <NumeroSobre></NumeroSobre>
                    <Bultos>
                        <Bulto>
                            <Alto></Alto>
                            <Largo></Largo>
                            <Ancho></Ancho>
                            <Dimension></Dimension>
                            <Referencia></Referencia>
                            <Peso></Peso>
                        </Bulto>
                    </Bultos>
                    <NumeroBultos>1</NumeroBultos>
                    <Peso>3</Peso>
                   <EntregaPartirDe></EntregaPartirDe>
                    <Gestion></Gestion>
                    <Retorno></Retorno>
                    <ConfirmacionInmediata></ConfirmacionInmediata>
                    <EntregaSabado></EntregaSabado>
                    <Entrega830></Entrega830>
                    <Reembolso>O</Reembolso>
                    <ImporteReembolso>13,40</ImporteReembolso>
                <TramoHorario>2</TramoHorario>
                </DatosServicio>
            </request>
        </TransmEnvio>
    </soap12:Body>
</soap12:Envelope>';
$header = array(
   'POST /MRWEnvio.asmx HTTP/1.1',
   'Host: sagec-test.mrw.es',
   'Content-Type: text/xml; charset=utf-8',
   'SOAPAction: "http://www.mrw.es/TransmEnvio"',
   "Content-length: ".strlen($envelope)
  );

$soap_do = curl_init(); 
curl_setopt($soap_do, CURLOPT_URL,  "http://sagec-test.mrw.es/MRWEnvio.asmx?wsdl");   
curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10); 
curl_setopt($soap_do, CURLOPT_TIMEOUT,        10); 
curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true );
curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);  
curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false); 
curl_setopt($soap_do, CURLOPT_POST,           true ); 
curl_setopt($soap_do, CURLOPT_POSTFIELDS,   $envelope); 
curl_setopt($soap_do, CURLOPT_HTTPHEADER,    $header); 

$result = curl_exec($soap_do);
$err = curl_error($soap_do);  

var_dump($result);
echo "<br />RESULT: <br />";
echo "<br />ERR: $err<br />";
*/
die();

/*
// otros parametros

 $parameters  = array (
      'request' => array (
            'DatosRecogida' => array (
               'Direccion' => array (
                           'CodigoDireccion' => "",
                           'CodigoTipoVia' => "",
                           'Via' => "",
                           'Numero' => "",
                           'Resto' => "",
                           'CodigoPostal' => "",
                           'Poblacion' => "",
                           'Provincia' => "",
                           'Estado' => "",
                           'CodigoPais' => "",
                           'TipoPuntoEntrega' => "",
                           'CodigoPuntoEntrega' => "",
                           'CodigoFranquiciaAsociadaPuntoEntrega' => ""
                        ),
               'Nif' => "",
               'Nombre' => "",
               'Telefono' => "",
               'Contacto' => "",
               'Horario' => false,
               'Observaciones' => ""
            ),
            'DatosEntrega' => array (
               'Direccion' => array (
                           'CodigoDireccion' => "",
                           'CodigoTipoVia' => "",
                           'Via' => "",
                           'Numero' => "",
                           'Resto' => "",
                           'CodigoPostal' => "",
                           'Poblacion' => "",
                           'Provincia' => "",
                           'Estado' => "",
                           'CodigoPais' => "",
                           'TipoPuntoEntrega' => "",
                           'CodigoPuntoEntrega' => "",
                           'CodigoFranquiciaAsociadaPuntoEntrega' => ""
                        ),
               'Nif' => "",
               'Nombre' => "",
               'Telefono' => "",
               'Contacto' => "",
               'ALaAtencionDe' => "",
               'Horario' => false,
               'Observaciones' => ""
            ),
            'DatosServicio' => array (
               'Fecha' => "",
               'NumeroAlbaran' => "",
               'Referencia' => "",
               'EnFranquicia' => "",
               'CodigoServicio' => "",
               'DescripcionServicio' => "",
               'Frecuencia' => "",
               'CodigoPromocion' => "",
               'NumeroSobre' => "",
               'Bultos' => array(false, false),
               'NumeroBultos' => "",
               'Peso' => "",
               'NumeroPuentes' => "",
               'EntregaSabado' => "",
               'Entrega830' => "",
               'EntregaPartirDe' => "",
               'Gestion' => "",
               'Retorno' => "",
               'ConfirmacionInmediata' => "",
               'Reembolso' => "",
               'ImporteReembolso' => "",
               'TipoMercancia' => "",
               'ValorDeclarado' => "",
               'ServicioEspecial' => "",
               'CodigoMoneda' => "",
               'ValorEstadistico' => "",
               'ValorEstadisticoEuros' => "",
               'Notificaciones' => array(false, false),
               'SeguroOpcional' => array(
                     'CodigoNaturaleza' => "", 
                     'ValorAsegurado' => ""
               ),
               'TramoHorario' => "",
               'PortesDebidos' => ""
            )
      ),
   );

*/
?>