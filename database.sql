SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `mumita_bbdd` DEFAULT CHARACTER SET utf8 ;
USE `mumita_bbdd` ;

-- -----------------------------------------------------
-- Table `mumita_bbdd`.`address`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mumita_bbdd`.`address` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `street` VARCHAR(200) NULL DEFAULT NULL ,
  `city` VARCHAR(200) NULL DEFAULT NULL ,
  `province` VARCHAR(200) NULL DEFAULT NULL ,
  `country` VARCHAR(50) NULL DEFAULT NULL ,
  `postal_code` VARCHAR(10) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mumita_bbdd`.`ages`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mumita_bbdd`.`ages` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(15) NOT NULL ,
  `order` TINYINT(2) NOT NULL DEFAULT '0' ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mumita_bbdd`.`boxes`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mumita_bbdd`.`boxes` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `abstract` VARCHAR(100) NOT NULL ,
  `category` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0: clothes' ,
  `description` VARCHAR(500) NOT NULL ,
  `id_age` INT(11) NOT NULL DEFAULT '0' ,
  `sex` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0: unisex\n1: girl\n2: boy' ,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_age` (`id_age` ASC) ,
  CONSTRAINT `fk_age`
    FOREIGN KEY (`id_age` )
    REFERENCES `mumita_bbdd`.`ages` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mumita_bbdd`.`users`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mumita_bbdd`.`users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `email` VARCHAR(254) NOT NULL ,
  `password` VARCHAR(255) NOT NULL ,
  `name` VARCHAR(50) NOT NULL ,
  `lastname` VARCHAR(75) NOT NULL ,
  `salt` VARCHAR(255) NOT NULL ,
  `sex` TINYINT(1) NOT NULL ,
  `active` TINYINT(1) NOT NULL ,
  `role_level` TINYINT(2) NOT NULL DEFAULT '0' ,
  `bio` TEXT NULL DEFAULT NULL ,
  `shipping_address_id` INT(11) NULL DEFAULT NULL ,
  `billing_address_id` INT(11) NULL DEFAULT NULL ,
  `pickup_address_id` INT(11) NULL DEFAULT NULL ,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ,
  `activation_code` VARCHAR(40) NULL DEFAULT NULL ,
  `birthday` DATE NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `UNIQ_2DA17977F85E0677` (`email` ASC) ,
  INDEX `IDX_2DA17977D60322AC` (`role_level` ASC) ,
  INDEX `billing_address` (`billing_address_id` ASC) ,
  INDEX `pickup_address` (`pickup_address_id` ASC) ,
  INDEX `shipping_address` (`shipping_address_id` ASC) ,
  CONSTRAINT `billing_address`
    FOREIGN KEY (`billing_address_id` )
    REFERENCES `mumita_bbdd`.`address` (`id` )
    ON UPDATE CASCADE,
  CONSTRAINT `pickup_address`
    FOREIGN KEY (`pickup_address_id` )
    REFERENCES `mumita_bbdd`.`address` (`id` )
    ON UPDATE CASCADE,
  CONSTRAINT `shipping_address`
    FOREIGN KEY (`shipping_address_id` )
    REFERENCES `mumita_bbdd`.`address` (`id` )
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mumita_bbdd`.`box_changes`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mumita_bbdd`.`box_changes` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `id_box` INT(11) NOT NULL ,
  `id_user` INT(11) NULL DEFAULT NULL ,
  `details` VARCHAR(500) NULL DEFAULT NULL ,
  `ip_address` VARCHAR(10) NULL DEFAULT NULL ,
  `type` TINYINT(1) NULL DEFAULT NULL COMMENT '0: creation\n1: update' ,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`id`, `id_box`) ,
  INDEX `fk_box` (`id_box` ASC) ,
  INDEX `fk_user` (`id_user` ASC) ,
  CONSTRAINT `fk_box`
    FOREIGN KEY (`id_box` )
    REFERENCES `mumita_bbdd`.`boxes` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_user`
    FOREIGN KEY (`id_user` )
    REFERENCES `mumita_bbdd`.`users` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mumita_bbdd`.`sizes`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mumita_bbdd`.`sizes` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mumita_bbdd`.`box_clothes`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mumita_bbdd`.`box_clothes` (
  `id_box` INT(11) NOT NULL ,
  `id_size` INT(11) NULL DEFAULT NULL ,
  `colour1` VARCHAR(6) NULL DEFAULT NULL ,
  `colour2` VARCHAR(6) NULL DEFAULT NULL ,
  `colour3` VARCHAR(6) NULL DEFAULT NULL ,
  `season` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0:summer\n1:autumn\n2:winter\n3:spring' ,
  PRIMARY KEY (`id_box`) ,
  INDEX `fk_cloth_box` (`id_box` ASC) ,
  INDEX `fk_cloth_size` (`id_size` ASC) ,
  CONSTRAINT `fk_cloth_box`
    FOREIGN KEY (`id_box` )
    REFERENCES `mumita_bbdd`.`boxes` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_cloth_size`
    FOREIGN KEY (`id_size` )
    REFERENCES `mumita_bbdd`.`sizes` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mumita_bbdd`.`box_valoration`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mumita_bbdd`.`box_valoration` (
  `id_box` INT(11) NOT NULL ,
  `id_user` INT(11) NOT NULL ,
  `valoration` TINYINT(1) NOT NULL DEFAULT '1' COMMENT '1-5' ,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`id_box`, `id_user`) ,
  INDEX `fk_box_valoration` (`id_box` ASC) ,
  INDEX `fk_user_valoration` (`id_user` ASC) ,
  CONSTRAINT `fk_box_valoration`
    FOREIGN KEY (`id_box` )
    REFERENCES `mumita_bbdd`.`boxes` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_user_valoration`
    FOREIGN KEY (`id_user` )
    REFERENCES `mumita_bbdd`.`users` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mumita_bbdd`.`customers`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mumita_bbdd`.`customers` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(128) NULL DEFAULT NULL ,
  `email` VARCHAR(255) NULL DEFAULT NULL ,
  `ip` VARCHAR(15) NULL DEFAULT NULL ,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mumita_bbdd`.`mu_sessions`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mumita_bbdd`.`mu_sessions` (
  `session_id` VARCHAR(40) NOT NULL DEFAULT '0' ,
  `ip_address` VARCHAR(16) NOT NULL DEFAULT '0' ,
  `user_agent` VARCHAR(120) NOT NULL ,
  `last_activity` INT(10) UNSIGNED NOT NULL DEFAULT '0' ,
  `user_data` TEXT NOT NULL ,
  PRIMARY KEY (`session_id`) ,
  INDEX `last_activity_idx` (`last_activity` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mumita_bbdd`.`role`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mumita_bbdd`.`role` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(25) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mumita_bbdd`.`user_follow`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mumita_bbdd`.`user_follow` (
  `id_follower` INT(11) NOT NULL ,
  `id_followed` INT(11) NOT NULL ,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`id_follower`, `id_followed`) ,
  INDEX `fk_follower` (`id_follower` ASC) ,
  INDEX `fk_followed` (`id_followed` ASC) ,
  CONSTRAINT `fk_followed`
    FOREIGN KEY (`id_followed` )
    REFERENCES `mumita_bbdd`.`users` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_follower`
    FOREIGN KEY (`id_follower` )
    REFERENCES `mumita_bbdd`.`users` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


INSERT INTO `mumita_bbdd`.`user`
(
`email`,
`password`,
`name`,
`lastname`,
`salt`,
`sex`,
`active`,
`role_level`)
VALUES
(
'admin@admin.com',
'21232f297a57a5a743894a0e4a801fc3',
'Administrador',
'Admin',
'fe991c780ed5c850972efb8e2b1e07ea64bf60af',
0,
1,
1
);